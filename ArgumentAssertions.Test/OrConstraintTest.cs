﻿using System;
using System.Diagnostics.CodeAnalysis;
using ArgumentAssertions.Generic;
using ArgumentAssertions.String;
using FluentAssertions;
using NUnit.Framework;

namespace ArgumentAssertions.Test
{
    [TestFixture]
    [Category("Unit")]
    [ExcludeFromCodeCoverage]
    public class OrConstraintTest
    {
        #region Or If

        [TestCase]
        [SuppressMessage("ReSharper", "ConvertToConstant.Local")]
        public void ThrowIfNullOrIfEmpty_TextIsNotNullOrEmpty_DoNotThrowArgumentException()
        {
            // Arrange
            var text = "just a string";

            // Act
            Action act = () => text.ThrowIf(() => text).Null().OrIf.Empty();

            // Result
            act.ShouldNotThrow();
        }

        [TestCase]
        [SuppressMessage("ReSharper", "ConvertToConstant.Local")]
        [SuppressMessage("ReSharper", "ExpressionIsAlwaysNull")]
        public void ThrowIfNullOrIfEmpty_TextIsNull_ThrowArgumentNullException()
        {
            // Arrange
            var text = (string) null;

            // Act
            Action act = () => text.ThrowIf(() => text).Null().OrIf.Empty();

            // Result
            act.ShouldThrow<ArgumentNullException>()
                .Where(exception => exception.Message.Contains("Parameter 'text' should not be null."));
        }

        [TestCase]
        [SuppressMessage("ReSharper", "ConvertToConstant.Local")]
        [SuppressMessage("ReSharper", "ExpressionIsAlwaysNull")]
        public void ThrowIfNullOrIfEmpty_TextIsEmpty_ThrowArgumentNullException()
        {
            // Arrange
            var text = "";

            // Act
            Action act = () => text.ThrowIf(() => text).Null().OrIf.Empty();

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(exception => exception.Message.Contains("Parameter 'text' should not be empty."));
        }

        #endregion

        #region Or If Not

        [TestCase]
        [SuppressMessage("ReSharper", "ConvertToConstant.Local")]
        public void ThrowIfNullOrIfNotEmpty_TextIsNotNullAndIsEmpty_DoNotThrowArgumentException()
        {
            // Arrange
            var text = "";

            // Act
            Action act = () => text.ThrowIf(() => text).Null().OrIfNot.Empty();

            // Result
            act.ShouldNotThrow();
        }

        [TestCase]
        [SuppressMessage("ReSharper", "ConvertToConstant.Local")]
        [SuppressMessage("ReSharper", "ExpressionIsAlwaysNull")]
        public void ThrowIfNullOrIfNotEmpty_TextIsNull_ThrowArgumentNullException()
        {
            // Arrange
            var text = (string)null;

            // Act
            Action act = () => text.ThrowIf(() => text).Null().OrIfNot.Empty();

            // Result
            act.ShouldThrow<ArgumentNullException>()
                .Where(exception => exception.Message.Contains("Parameter 'text' should not be null."));
        }

        [TestCase]
        [SuppressMessage("ReSharper", "ConvertToConstant.Local")]
        [SuppressMessage("ReSharper", "ExpressionIsAlwaysNull")]
        public void ThrowIfNullOrIfNotEmpty_TextIsNotEmpty_ThrowArgumentException()
        {
            // Arrange
            var text = "not empty";

            // Act
            Action act = () => text.ThrowIf(() => text).Null().OrIfNot.Empty();

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(exception => exception.Message.Contains("Parameter 'text' should be empty."));
        }

        #endregion
    }
}