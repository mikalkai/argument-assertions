﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using ArgumentAssertions.Enumerable;
using ArgumentAssertions.Generic;
using FluentAssertions;
using NUnit.Framework;

namespace ArgumentAssertions.Test.Enumerable
{
    [TestFixture]
    [Category("Unit")]
    [ExcludeFromCodeCoverage]
    public class EnumerableContainsTest
    {
        #region Test Data

        public class CustomException : Exception {}

        #endregion

        [TestCase]
        public void ThrowIfContains_EnumerableDoesNotContainZero_DoNotThrowArgumentException()
        {
            // Arrange
            var ints = new List<int> {1, 2, 3};

            // Act
            Action act1 = () => ints.ThrowIf(() => ints).Contains(i => i == 0);
            Action act2 = () => ints.ThrowIf(() => ints).Contains(0);

            // Result
            act1.ShouldNotThrow<ArgumentException>();
            act2.ShouldNotThrow<ArgumentException>();
        }

        [TestCase]
        public void ThrowIfContains_EnumerableContainsZero_ThrowArgumentException()
        {
            // Arrange
            var ints = new List<int> { 0, 1, 2, 3 };

            // Act
            Action act1 = () => ints.ThrowIf(() => ints).Contains(i => i == 0);
            Action act2 = () => ints.ThrowIf(() => ints).Contains(0);

            // Result
            act1.ShouldThrow<ArgumentException>()
                .Where(exception => exception.Message.Contains("Parameter 'ints' should not contain item '0'."));

            act2.ShouldThrow<ArgumentException>()
                .Where(exception => exception.Message.Contains("Parameter 'ints' should not contain item '0'."));
        }

        [TestCase]
        [SuppressMessage("ReSharper", "ExpressionIsAlwaysNull")]
        public void ThrowIfContains_EnumerableIsNull_ThrowArgumentNullException()
        {
            // Arrange
            List<int> ints = null;

            // Act
            Action act1 = () => ints.ThrowIf(() => ints).Contains(i => i == 0);
            Action act2 = () => ints.ThrowIf(() => ints).Contains(0);

            // Result
            act1.ShouldThrow<ArgumentException>()
                .Where(exception => exception.Message.Contains("Parameter 'ints' should not be null."));

            act2.ShouldThrow<ArgumentException>()
                .Where(exception => exception.Message.Contains("Parameter 'ints' should not be null."));
        }

        [TestCase]
        public void ThrowIfContains_EnumerableContainsZero_ThrowCustomException()
        {
            // Arrange
            var ints = new List<int> { 0, 1, 2, 3 };

            // Act
            Action act1 = () => ints.ThrowIf(() => new CustomException()).Contains(i => i == 0);
            Action act2 = () => ints.ThrowIf(() => new CustomException()).Contains(0);

            // Result
            act1.ShouldThrow<CustomException>();
            act2.ShouldThrow<CustomException>();
        }
    }
}