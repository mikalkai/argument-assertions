﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using ArgumentAssertions.Enumerable;
using ArgumentAssertions.Generic;
using FluentAssertions;
using NUnit.Framework;

namespace ArgumentAssertions.Test.Enumerable
{
    [TestFixture]
    [Category("Unit")]
    [ExcludeFromCodeCoverage]
    public class EnumerableEmptyTest
    {
        #region Test Data

        public class A
        {
            public List<int> Numbers { get; set; }
        }

        public class CustomException : Exception {}

        #endregion

        [TestCase]
        public void ThrowIfEmpty_EnumerableIsNotEmpty_DoNotThrowArgumentException()
        {
            // Arrange
            var a = new A
            {
                Numbers = new List<int> {1, 2, 3}
            };

            // Act
            Action act = () => a.Numbers.ThrowIf(() => a.Numbers).Empty();

            // Result
            act.ShouldNotThrow<ArgumentException>();
        }

        [TestCase]
        public void ThrowIfEmpty_EnumerableIsEmpty_ThrowArgumentException()
        {
            // Arrange
            var a = new A
            {
                Numbers = new List<int>()
            };

            // Act
            Action act = () => a.Numbers.ThrowIf(() => a.Numbers).Empty();

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(exception => exception.Message.Contains("Parameter 'a.Numbers' should not be empty."));
        }

        [TestCase]
        public void ThrowIfEmpty_EnumerableIsNull_ThrowArgumentNullException()
        {
            // Arrange
            var a = new A
            {
                Numbers = null
            };

            // Act
            Action act = () => a.Numbers.ThrowIf(() => a.Numbers).Empty();

            // Result
            act.ShouldThrow<ArgumentNullException>()
                .Where(exception => exception.Message.Contains("Parameter 'a.Numbers' should not be null."));
        }

        [TestCase]
        public void ThrowIfEmpty_EnumerableIsEmpty_ThrowCustomException()
        {
            // Arrange
            var a = new A
            {
                Numbers = new List<int>()
            };

            // Act
            Action act = () => a.Numbers.ThrowIf(() => new CustomException()).Empty();

            // Result
            act.ShouldThrow<CustomException>();
        }
    }
}