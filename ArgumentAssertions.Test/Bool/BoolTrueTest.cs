﻿using System;
using System.Diagnostics.CodeAnalysis;
using ArgumentAssertions.Bool;
using ArgumentAssertions.Generic;
using FluentAssertions;
using NUnit.Framework;

namespace ArgumentAssertions.Test.Bool
{
    [TestFixture]
    [Category("Unit")]
    [ExcludeFromCodeCoverage]
    [SuppressMessage("ReSharper", "ConvertToConstant.Local")]
    [SuppressMessage("ReSharper", "ConditionIsAlwaysTrueOrFalse")]
    public class BoolTrueTest
    {
        #region Test Data

        public class CustomException : Exception {}

        #endregion

        [TestCase]
        public void ThrowIfNotTrue_BoolIsTrue_DoNotThrowArgumentException()
        {
            // Arrange
            var b = true;

            // Act
            Action act = () => b.ThrowIfNot(() => b).True();

            // Result
            act.ShouldNotThrow<ArgumentException>();
        }

        [TestCase]
        public void ThrowIfNotTrue_BoolIsFalse_ThrowArgumentException()
        {
            // Arrange
            var b = false;

            // Act
            Action act = () => b.ThrowIfNot(() => b).True();

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(exception => exception.Message.Contains("Parameter 'b' should be 'true'."));
        }

        [TestCase]
        public void ThrowIfNotTrue_BoolIsFalse_ThrowCustomException()
        {
            // Arrange
            var b = false;

            // Act
            Action act = () => b.ThrowIfNot(() => new CustomException()).True();

            // Result
            act.ShouldThrow<CustomException>();
        }

        [TestCase]
        public void ThrowIfNotTrue_NullableBoolIsTrue_DoNotThrowArgumentException()
        {
            // Arrange
            bool? b = true;

            // Act
            Action act = () => b.ThrowIfNot(() => b).True();

            // Result
            act.ShouldNotThrow<ArgumentException>();
        }

        [TestCase]
        public void ThrowIfNotTrue_NullableBoolIsFalse_ThrowArgumentException()
        {
            // Arrange
            bool? b = false;

            // Act
            Action act = () => b.ThrowIfNot(() => b).True();

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(exception => exception.Message.Contains("Parameter 'b' should be 'true'."));
        }

        [TestCase]
        [SuppressMessage("ReSharper", "ExpressionIsAlwaysNull")]
        public void ThrowIfNotTrue_NullableBoolIsNull_ThrowArgumentNullException()
        {
            // Arrange
            bool? b = null;

            // Act
            Action act = () => b.ThrowIfNot(() => b).True();

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(exception => exception.Message.Contains("Parameter 'b' should not be null."));
        }

        [TestCase]
        public void ThrowIfNotTrue_NullableBoolIsFalse_ThrowCustomException()
        {
            // Arrange
            bool? b = false;

            // Act
            Action act = () => b.ThrowIfNot(() => new CustomException()).True();

            // Result
            act.ShouldThrow<CustomException>();
        }

        [TestCase]
        [SuppressMessage("ReSharper", "ExpressionIsAlwaysNull")]
        public void ThrowIfNotTrue_NullableBoolIsNull_ThrowCustomException()
        {
            // Arrange
            bool? b = null;

            // Act
            Action act = () => b.ThrowIfNot(() => new CustomException()).True();

            // Result
            act.ShouldThrow<CustomException>();
        }
    }
}