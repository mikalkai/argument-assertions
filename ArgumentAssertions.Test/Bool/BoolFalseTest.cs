﻿using System;
using System.Diagnostics.CodeAnalysis;
using ArgumentAssertions.Bool;
using ArgumentAssertions.Generic;
using FluentAssertions;
using NUnit.Framework;

namespace ArgumentAssertions.Test.Bool
{
    [TestFixture]
    [Category("Unit")]
    [ExcludeFromCodeCoverage]
    [SuppressMessage("ReSharper", "ConvertToConstant.Local")]
    [SuppressMessage("ReSharper", "ConditionIsAlwaysTrueOrFalse")]
    public class BoolFalseTest
    {
        #region Test Data

        public class CustomException : Exception {}

        #endregion

        [TestCase]
        public void ThrowIfFalse_BoolIsTrue_DoNotThrowArgumentException()
        {
            // Arrange
            var b = true;

            // Act
            Action act = () => b.ThrowIf(() => b).False();

            // Result
            act.ShouldNotThrow<ArgumentException>();
        }

        [TestCase]
        public void ThrowIfFalse_BoolIsFalse_ThrowArgumentException()
        {
            // Arrange
            var b = false;

            // Act
            Action act = () => b.ThrowIf(() => b).False();

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(exception => exception.Message.Contains("Parameter 'b' should not be 'false'."));
        }

        [TestCase]
        public void ThrowIfFalse_BoolIsFalse_ThrowCustomException()
        {
            // Arrange
            var b = false;

            // Act
            Action act = () => b.ThrowIf(() => new CustomException()).False();

            // Result
            act.ShouldThrow<CustomException>();
        }

        [TestCase]
        public void ThrowIfFalse_NullableBoolIsTrue_DoNotThrowArgumentException()
        {
            // Arrange
            bool? b = true;

            // Act
            Action act = () => b.ThrowIf(() => b).False();

            // Result
            act.ShouldNotThrow<ArgumentException>();
        }

        [TestCase]
        public void ThrowIfFalse_NullableBoolIsFalse_ThrowArgumentException()
        {
            // Arrange
            bool? b = false;

            // Act
            Action act = () => b.ThrowIf(() => b).False();

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(exception => exception.Message.Contains("Parameter 'b' should not be 'false'."));
        }

        [TestCase]
        [SuppressMessage("ReSharper", "ExpressionIsAlwaysNull")]
        public void ThrowIfFalse_NullableBoolIsNull_ThrowArgumentNullException()
        {
            // Arrange
            bool? b = null;

            // Act
            Action act = () => b.ThrowIf(() => b).False();

            // Result
            act.ShouldThrow<ArgumentNullException>()
                .Where(exception => exception.Message.Contains("Parameter 'b' should not be null."));
        }

        [TestCase]
        public void ThrowIfFalse_NullableBoolIsFalse_ThrowCustomException()
        {
            // Arrange
            bool? b = false;

            // Act
            Action act = () => b.ThrowIf(() => new CustomException()).False();

            // Result
            act.ShouldThrow<CustomException>();
        }

        [TestCase]
        [SuppressMessage("ReSharper", "ExpressionIsAlwaysNull")]
        public void ThrowIfFalse_NullableBoolIsNull_ThrowCustomException()
        {
            // Arrange
            bool? b = null;

            // Act
            Action act = () => b.ThrowIf(() => new CustomException()).False();

            // Result
            act.ShouldThrow<CustomException>();
        }
    }
}