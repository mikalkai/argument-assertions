﻿using System;
using System.Diagnostics.CodeAnalysis;
using ArgumentAssertions.Generic;
using ArgumentAssertions.Numeric;
using FluentAssertions;
using NUnit.Framework;

namespace ArgumentAssertions.Test
{
    [TestFixture]
    [Category("Unit")]
    [ExcludeFromCodeCoverage]
    public class NullableOrConstraintTest
    {
        #region Or If

        [TestCase]
        [SuppressMessage("ReSharper", "ConvertToConstant.Local")]
        public void ThrowIfNullOrIfZero_NumberIsNotNullOrZero_DoNotThrowArgumentException()
        {
            // Arrange
            int? number = 1;

            // Act
            Action act = () => number.ThrowIf(() => number).Null().OrIf.Zero();

            // Result
            act.ShouldNotThrow();
        }

        [TestCase]
        [SuppressMessage("ReSharper", "ConvertToConstant.Local")]
        [SuppressMessage("ReSharper", "ExpressionIsAlwaysNull")]
        public void ThrowIfNullOrIfZero_NumberIsNull_ThrowArgumentNullException()
        {
            // Arrange
            int? number = null;

            // Act
            Action act = () => number.ThrowIf(() => number).Null().OrIf.Zero();

            // Result
            act.ShouldThrow<ArgumentNullException>()
                .Where(exception => exception.Message.Contains("Parameter 'number' should not be null."));
        }

        [TestCase]
        [SuppressMessage("ReSharper", "ConvertToConstant.Local")]
        public void ThrowIfNullOrIfZero_NumberIsZero_ThrowArgumentNullException()
        {
            // Arrange
            int? number = 0;

            // Act
            Action act = () => number.ThrowIf(() => number).Null().OrIf.Zero();

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(exception => exception.Message.Contains("Parameter 'number' should not be zero."));
        }

        #endregion

        [TestCase]
        [SuppressMessage("ReSharper", "ConvertToConstant.Local")]
        public void ThrowIfNullOrIfNotZero_NumberIsNotNullOrZero_DoNotThrowArgumentException()
        {
            // Arrange
            int? number = 0;

            // Act
            Action act = () => number.ThrowIf(() => number).Null().OrIfNot.Zero();

            // Result
            act.ShouldNotThrow();
        }

        [TestCase]
        [SuppressMessage("ReSharper", "ConvertToConstant.Local")]
        [SuppressMessage("ReSharper", "ExpressionIsAlwaysNull")]
        public void ThrowIfNullOrIfNotZero_NumberIsNull_ThrowArgumentNullException()
        {
            // Arrange
            int? number = null;

            // Act
            Action act = () => number.ThrowIf(() => number).Null().OrIfNot.Zero();

            // Result
            act.ShouldThrow<ArgumentNullException>()
                .Where(exception => exception.Message.Contains("Parameter 'number' should not be null."));
        }

        [TestCase]
        [SuppressMessage("ReSharper", "ConvertToConstant.Local")]
        public void ThrowIfNullOrIfNotZero_NumberIsZero_ThrowArgumentNullException()
        {
            // Arrange
            int? number = 1;

            // Act
            Action act = () => number.ThrowIf(() => number).Null().OrIfNot.Zero();

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(exception => exception.Message.Contains("Parameter 'number' should be zero."));
        }
    }
}