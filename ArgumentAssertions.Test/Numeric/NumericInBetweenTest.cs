﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using ArgumentAssertions.Generic;
using ArgumentAssertions.Numeric;
using FluentAssertions;
using NUnit.Framework;

namespace ArgumentAssertions.Test.Numeric
{
    [TestFixture]
    [Category("Unit")]
    [ExcludeFromCodeCoverage]
    [SuppressMessage("ReSharper", "ConvertToConstant.Local")]
    public class NumericInBetweenTest
    {
        #region Test Data

        public class CustomException : Exception { }

        #endregion

        [TestCase]
        public void ThrowIfNotInBetween_NumberIsNotInBetween_DoNotThrowArgumentException()
        {
            // Arrange
            var ints = new List<int> { 0, 1, 2 };
            var number = 0;

            // Act
            Action act = () => number.ThrowIfNot(() => number).InBetween(0, ints.Count - 1);

            // Result
            act.ShouldNotThrow<ArgumentException>();
        }

        [TestCase]
        public void ThrowIfNotInBetween_NumberIsInBetween_ThrowArgumentException()
        {
            // Arrange
            var ints = new List<int> { 0, 1, 2 };
            var number = ints.Count;

            // Act
            Action act = () => number.ThrowIfNot(() => number).InBetween(0, ints.Count - 1);

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(exception => exception.Message.Contains("Parameter 'number' should be in between '0' and '2'."));
        }

        [TestCase]
        public void ThrowIfNotInBetween_NumberIsInBetween_ThrowCustomException()
        {
            // Arrange
            var ints = new List<int> { 0, 1, 2 };
            var number = -1;

            // Act
            Action act = () => number.ThrowIfNot(() => new CustomException()).InBetween(0, ints.Count - 1);

            // Result
            act.ShouldThrow<CustomException>();
        }

        [TestCase]
        public void ThrowIfNotInBetween_NullableNumberIsNotInBetween_DoNotThrowArgumentException()
        {
            // Arrange
            var ints = new List<int> { 0, 1, 2 };
            int? number = 0;

            // Act
            Action act = () => number.ThrowIfNot(() => number).InBetween(0, ints.Count - 1);

            // Result
            act.ShouldNotThrow<ArgumentException>();
        }

        [TestCase(-1,   "Parameter 'number' should be in between '0' and '2'.")]
        [TestCase(3,    "Parameter 'number' should be in between '0' and '2'.")]
        [TestCase(null, "Parameter 'number' should not be null.")]
        public void ThrowIfNotInBetween_NullableNumberIsInBetween_ThrowArgumentException(int? number, string message)
        {
            // Arrange

            // Act
            Action act = () => number.ThrowIfNot(() => number).InBetween(0, 2);

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(exception => exception.Message.Contains(message));
        }

        [TestCase(-1)]
        [TestCase(3)]
        [TestCase(null)]
        public void ThrowIfNotInBetween_NullableNumberIsInBetween_ThrowCustomException(int? number)
        {
            // Arrange

            // Act
            Action act = () => number.ThrowIfNot(() => new CustomException()).InBetween(0, 2);

            // Result
            act.ShouldThrow<CustomException>();
        }
    }
}