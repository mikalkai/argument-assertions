﻿using System;
using System.Diagnostics.CodeAnalysis;
using ArgumentAssertions.Generic;
using ArgumentAssertions.Numeric;
using FluentAssertions;
using NUnit.Framework;

namespace ArgumentAssertions.Test.Numeric
{
    [TestFixture]
    [Category("Unit")]
    [ExcludeFromCodeCoverage]
    [SuppressMessage("ReSharper", "ConvertToConstant.Local")]
    public class NumericNegativeTest
    {
        #region Test Data

        public class CustomException : Exception {}

        #endregion

        [TestCase]
        public void ThrowIfNegative_NumberIsNotNegative_DoNotThrowArgumentException()
        {
            // Arrange
            var number = 1;

            // Act
            Action act = () => number.ThrowIf(() => number).Negative();

            // Result
            act.ShouldNotThrow<ArgumentException>();
        }

        [TestCase]
        public void ThrowIfNegative_NumberIsNegative_ThrowArgumentException()
        {
            // Arrange
            var number = -1;

            // Act
            Action act = () => number.ThrowIf(() => number).Negative();

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(exception => exception.Message.Contains("Parameter 'number' should not be negative."));
        }

        [TestCase]
        public void ThrowIfNegative_NumberIsNegative_ThrowCustomException()
        {
            // Arrange
            var number = -1;

            // Act
            Action act = () => number.ThrowIf(() => new CustomException()).Negative();

            // Result
            act.ShouldThrow<CustomException>();
        }

        [TestCase]
        public void ThrowIfNegative_NullableNumberIsNotNegative_DoNotThrowArgumentException()
        {
            // Arrange
            int? number = 1;

            // Act
            Action act = () => number.ThrowIf(() => number).Negative();

            // Result
            act.ShouldNotThrow<ArgumentException>();
        }

        [TestCase(-1,   "Parameter 'number' should not be negative.")]
        [TestCase(null, "Parameter 'number' should not be null.")]
        public void ThrowIfNegative_NullableNumberIsNegative_ThrowArgumentException(int? number, string message)
        {
            // Arrange

            // Act
            Action act = () => number.ThrowIf(() => number).Negative();

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(exception => exception.Message.Contains(message));
        }

        [TestCase(-1)]
        [TestCase(null)]
        public void ThrowIfNegative_NullableNumberIsNegative_ThrowCustomException(int? number)
        {
            // Arrange

            // Act
            Action act = () => number.ThrowIf(() => new CustomException()).Negative();

            // Result
            act.ShouldThrow<CustomException>();
        }
    }
}