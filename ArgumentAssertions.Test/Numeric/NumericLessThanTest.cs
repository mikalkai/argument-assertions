﻿using System;
using System.Diagnostics.CodeAnalysis;
using ArgumentAssertions.Generic;
using ArgumentAssertions.Numeric;
using FluentAssertions;
using NUnit.Framework;

namespace ArgumentAssertions.Test.Numeric
{
    [TestFixture]
    [Category("Unit")]
    [ExcludeFromCodeCoverage]
    [SuppressMessage("ReSharper", "ConvertToConstant.Local")]
    public class NumericLessThanTest
    {
        #region Test Data

        public class CustomException : Exception { }

        #endregion

        [TestCase]
        public void ThrowIfLessThan_NumberIsNotLessThan_DoNotThrowArgumentException()
        {
            // Arrange
            var number = 10;

            // Act
            Action act = () => number.ThrowIf(() => number).LessThan(5);

            // Result
            act.ShouldNotThrow<ArgumentException>();
        }

        [TestCase]
        public void ThrowIfLessThan_NumberIsLessThan_ThrowArgumentException()
        {
            // Arrange
            var number = 2;

            // Act
            Action act = () => number.ThrowIf(() => number).LessThan(5);

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(exception => exception.Message.Contains("Parameter 'number' should not be less than '5'."));
        }

        [TestCase]
        public void ThrowIfLessThan_NumberIsLessThan_ThrowCustomException()
        {
            // Arrange
            var number = 2;

            // Act
            Action act = () => number.ThrowIf(() => new CustomException()).LessThan(5);

            // Result
            act.ShouldThrow<CustomException>();
        }

        [TestCase]
        public void ThrowIfLessThan_NullableNumberIsNotLessThan_DoNotThrowArgumentException()
        {
            // Arrange
            int? number = 10;

            // Act
            Action act = () => number.ThrowIf(() => number).LessThan(5);

            // Result
            act.ShouldNotThrow<ArgumentException>();
        }

        [TestCase(2,    "Parameter 'number' should not be less than '5'.")]
        [TestCase(null, "Parameter 'number' should not be null.")]
        public void ThrowIfLessThan_NullableNumberIsLessThan_ThrowArgumentException(int? number, string message)
        {
            // Arrange

            // Act
            Action act = () => number.ThrowIf(() => number).LessThan(5);

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(exception => exception.Message.Contains(message));
        }

        [TestCase(2)]
        [TestCase(null)]
        public void ThrowIfLessThan_NullableNumberIsLessThan_ThrowCustomException(int? number)
        {
            // Arrange

            // Act
            Action act = () => number.ThrowIf(() => new CustomException()).LessThan(5);

            // Result
            act.ShouldThrow<CustomException>();
        }
    }
}