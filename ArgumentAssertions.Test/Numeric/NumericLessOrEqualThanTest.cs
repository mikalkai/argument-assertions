﻿using System;
using System.Diagnostics.CodeAnalysis;
using ArgumentAssertions.Generic;
using ArgumentAssertions.Numeric;
using FluentAssertions;
using NUnit.Framework;

namespace ArgumentAssertions.Test.Numeric
{
    [TestFixture]
    [Category("Unit")]
    [ExcludeFromCodeCoverage]
    [SuppressMessage("ReSharper", "ConvertToConstant.Local")]
    public class NumericLessOrEqualThanTest
    {
        #region Test Data

        public class CustomException : Exception { }

        #endregion

        [TestCase(2)]
        public void ThrowIfLessOrEqualThan_NumberIsNotLessThan_DoNotThrowArgumentException(int number)
        {
            // Arrange

            // Act
            Action act = () => number.ThrowIf(() => number).LessOrEqualThan(1);

            // Result
            act.ShouldNotThrow<ArgumentException>();
        }

        [TestCase(1)]
        [TestCase(0)]
        public void ThrowIfLessOrEqualThan_NumberIsLessThan_ThrowArgumentException(int number)
        {
            // Arrange

            // Act
            Action act = () => number.ThrowIf(() => number).LessOrEqualThan(1);

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(
                    exception => exception.Message.Contains("Parameter 'number' should not be less or equal than '1'."));
        }

        [TestCase(1)]
        [TestCase(0)]
        public void ThrowIfLessOrEqualThan_NumberIsLessThan_ThrowCustomException(int number)
        {
            // Arrange

            // Act
            Action act = () => number.ThrowIf(() => new CustomException()).LessOrEqualThan(1);

            // Result
            act.ShouldThrow<CustomException>();
        }

        [TestCase(2)]
        public void ThrowIfLessOrEqualThan_NullableNumberIsNotLessThan_DoNotThrowArgumentException(int? number)
        {
            // Arrange

            // Act
            Action act = () => number.ThrowIf(() => number).LessOrEqualThan(1);

            // Result
            act.ShouldNotThrow<ArgumentException>();
        }

        [TestCase(1,    "Parameter 'number' should not be less or equal than '1'.")]
        [TestCase(0,    "Parameter 'number' should not be less or equal than '1'.")]
        [TestCase(null, "Parameter 'number' should not be null.")]
        public void ThrowIfLessOrEqualThan_NullableNumberIsLessThan_ThrowArgumentException(int? number, string message)
        {
            // Arrange

            // Act
            Action act = () => number.ThrowIf(() => number).LessOrEqualThan(1);

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(exception => exception.Message.Contains(message));
        }

        [TestCase(1)]
        [TestCase(0)]
        [TestCase(null)]
        public void ThrowIfLessOrEqualThan_NullableNumberIsLessThan_ThrowCustomException(int? number)
        {
            // Arrange

            // Act
            Action act = () => number.ThrowIf(() => new CustomException()).LessOrEqualThan(1);

            // Result
            act.ShouldThrow<CustomException>();
        }
    }
}