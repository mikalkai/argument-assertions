﻿using System;
using System.Diagnostics.CodeAnalysis;
using ArgumentAssertions.Generic;
using ArgumentAssertions.Numeric;
using FluentAssertions;
using NUnit.Framework;

namespace ArgumentAssertions.Test.Numeric
{
    [TestFixture]
    [Category("Unit")]
    [ExcludeFromCodeCoverage]
    [SuppressMessage("ReSharper", "ConvertToConstant.Local")]
    public class NumericMoreOrEqualThanTest
    {
        #region Test Data

        public class CustomException : Exception {}

        #endregion

        [TestCase(0)]
        public void ThrowIfMoreOrEqualThan_NumberIsNotGreaterOrEqualThan_DoNotThrowArgumentException(int number)
        {
            // Arrange

            // Act
            Action act = () => number.ThrowIf(() => number).MoreOrEqualThan(1);

            // Result
            act.ShouldNotThrow<ArgumentException>();
        }

        [TestCase(1)]
        [TestCase(2)]
        public void ThrowIfMoreOrEqualThan_NumberIsGreaterOrEqualThan_ThrowArgumentException(int number)
        {
            // Arrange

            // Act
            Action act = () => number.ThrowIf(() => number).MoreOrEqualThan(1);

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(exception => exception.Message.Contains("Parameter 'number' should not be more or equal than '1'."));
        }

        [TestCase(1)]
        [TestCase(2)]
        public void ThrowIfMoreOrEqualThan_NumberIsGreaterOrEqualThan_ThrowCustomException(int number)
        {
            // Arrange

            // Act
            Action act = () => number.ThrowIf(() => new CustomException()).MoreOrEqualThan(1);

            // Result
            act.ShouldThrow<CustomException>();
        }

        [TestCase(0)]
        public void ThrowIfMoreOrEqualThan_NullableNumberIsNotGreaterOrEqualThan_DoNotThrowArgumentException(int? number)
        {
            // Arrange

            // Act
            Action act = () => number.ThrowIf(() => number).MoreOrEqualThan(1);

            // Result
            act.ShouldNotThrow<ArgumentException>();
        }

        [TestCase(1,    "Parameter 'number' should not be more or equal than '1'.")]
        [TestCase(2,    "Parameter 'number' should not be more or equal than '1'.")]
        [TestCase(null, "Parameter 'number' should not be null.")]
        public void ThrowIfMoreOrEqualThan_NullableNumberIsGreaterOrEqualThan_ThrowArgumentException(int? number, string message)
        {
            // Arrange

            // Act
            Action act = () => number.ThrowIf(() => number).MoreOrEqualThan(1);

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(exception => exception.Message.Contains(message));
        }

        [TestCase(1)]
        [TestCase(2)]
        [TestCase(null)]
        public void ThrowIfMoreOrEqualThan_NullableNumberIsGreaterOrEqualThan_ThrowCustomException(int? number)
        {
            // Arrange

            // Act
            Action act = () => number.ThrowIf(() => new CustomException()).MoreOrEqualThan(1);

            // Result
            act.ShouldThrow<CustomException>();
        }
    }
}