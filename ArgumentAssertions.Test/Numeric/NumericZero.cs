﻿using System;
using System.Diagnostics.CodeAnalysis;
using ArgumentAssertions.Generic;
using ArgumentAssertions.Numeric;
using FluentAssertions;
using NUnit.Framework;

namespace ArgumentAssertions.Test.Numeric
{
    [TestFixture]
    [Category("Unit")]
    [ExcludeFromCodeCoverage]
    [SuppressMessage("ReSharper", "ConvertToConstant.Local")]
    public class NumericZeroTest
    {
        #region Test Data

        public class CustomException : Exception {}

        #endregion

        [TestCase]
        public void ThrowIfZero_NumberIsNotZero_DoNotThrowArgumentException()
        {
            // Arrange
            var number = 1;

            // Act
            Action act = () => number.ThrowIf(() => number).Zero();

            // Result
            act.ShouldNotThrow<ArgumentException>();
        }

        [TestCase]
        public void ThrowIfZero_NumberIsZero_ThrowArgumentException()
        {
            // Arrange
            var number = 0;

            // Act
            Action act = () => number.ThrowIf(() => number).Zero();

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(exception => exception.Message.Contains("Parameter 'number' should not be zero."));
        }

        [TestCase]
        public void ThrowIfZero_NumberIsZero_ThrowCustomException()
        {
            // Arrange
            var number = 0;

            // Act
            Action act = () => number.ThrowIf(() => new CustomException()).Zero();

            // Result
            act.ShouldThrow<CustomException>();
        }

        [TestCase]
        public void ThrowIfZero_NullableNumberIsNotZero_DoNotThrowArgumentException()
        {
            // Arrange
            int? number = 1;

            // Act
            Action act = () => number.ThrowIf(() => number).Zero();

            // Result
            act.ShouldNotThrow<ArgumentException>();
        }

        [TestCase(0,    "Parameter 'number' should not be zero.")]
        [TestCase(null, "Parameter 'number' should not be null.")]
        public void ThrowIfZero_NullableNumberIsZero_ThrowArgumentException(int? number, string message)
        {
            // Arrange

            // Act
            Action act = () => number.ThrowIf(() => number).Zero();

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(exception => exception.Message.Contains(message));
        }

        [TestCase(0)]
        [TestCase(null)]
        public void ThrowIfZero_NullableNumberIsZero_ThrowCustomException(int? number)
        {
            // Arrange

            // Act
            Action act = () => number.ThrowIf(() => new CustomException()).Zero();

            // Result
            act.ShouldThrow<CustomException>();
        }
    }
}