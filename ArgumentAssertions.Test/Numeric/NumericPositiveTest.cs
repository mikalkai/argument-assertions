﻿using System;
using System.Diagnostics.CodeAnalysis;
using ArgumentAssertions.Generic;
using ArgumentAssertions.Numeric;
using FluentAssertions;
using NUnit.Framework;

namespace ArgumentAssertions.Test.Numeric
{
    [TestFixture]
    [Category("Unit")]
    [ExcludeFromCodeCoverage]
    [SuppressMessage("ReSharper", "ConvertToConstant.Local")]
    public class NumericPositiveTest
    {
        #region Test Data

        public class CustomException : Exception {}

        #endregion

        [TestCase]
        public void ThrowIfPositive_NumberIsNotPositive_DoNotThrowArgumentException()
        {
            // Arrange
            var number = -1;

            // Act
            Action act = () => number.ThrowIf(() => number).Positive();

            // Result
            act.ShouldNotThrow<ArgumentException>();
        }

        [TestCase]
        public void ThrowIfPositive_NumberIsPositive_ThrowArgumentException()
        {
            // Arrange
            var number = 1;

            // Act
            Action act = () => number.ThrowIf(() => number).Positive();

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(exception => exception.Message.Contains("Parameter 'number' should not be positive."));
        }

        [TestCase]
        public void ThrowIfPositive_NumberIsPositive_ThrowCustomException()
        {
            // Arrange
            var number = 1;

            // Act
            Action act = () => number.ThrowIf(() => new CustomException()).Positive();

            // Result
            act.ShouldThrow<CustomException>();
        }

        [TestCase]
        public void ThrowIfPositive_NullableNumberIsNotPositive_DoNotThrowArgumentException()
        {
            // Arrange
            int? number = -1;

            // Act
            Action act = () => number.ThrowIf(() => number).Positive();

            // Result
            act.ShouldNotThrow<ArgumentException>();
        }

        [TestCase(1,    "Parameter 'number' should not be positive.")]
        [TestCase(null, "Parameter 'number' should not be null.")]
        public void ThrowIfPositive_NullableNumberIsPositive_ThrowArgumentException(int? number, string message)
        {
            // Arrange

            // Act
            Action act = () => number.ThrowIf(() => number).Positive();

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(exception => exception.Message.Contains(message));
        }

        [TestCase(1)]
        [TestCase(null)]
        public void ThrowIfPositive_NullableNumberIsPositive_ThrowCustomException(int? number)
        {
            // Arrange

            // Act
            Action act = () => number.ThrowIf(() => new CustomException()).Positive();

            // Result
            act.ShouldThrow<CustomException>();
        }
    }
}