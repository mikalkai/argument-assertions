﻿using System;
using System.Diagnostics.CodeAnalysis;
using ArgumentAssertions.Generic;
using ArgumentAssertions.Numeric;
using FluentAssertions;
using NUnit.Framework;

namespace ArgumentAssertions.Test.Numeric
{
    [TestFixture]
    [Category("Unit")]
    [ExcludeFromCodeCoverage]
    [SuppressMessage("ReSharper", "ConvertToConstant.Local")]
    public class NumericMoreThanTest
    {
        #region Test Data

        public class CustomException : Exception { }

        #endregion

        [TestCase]
        public void ThrowIfMoreThan_NumberIsNotGreaterThan_DoNotThrowArgumentException()
        {
            // Arrange
            var number = 0;

            // Act
            Action act = () => number.ThrowIf(() => number).MoreThan(0);

            // Result
            act.ShouldNotThrow<ArgumentException>();
        }

        [TestCase]
        public void ThrowIfMoreThan_NumberIsGreaterThan_ThrowArgumentException()
        {
            // Arrange
            var number = 1;

            // Act
            Action act = () => number.ThrowIf(() => number).MoreThan(0);

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(exception => exception.Message.Contains("Parameter 'number' should not be more than '0'."));
        }

        [TestCase]
        public void ThrowIfMoreThan_NumberIsGreaterThan_ThrowCustomException()
        {
            // Arrange
            var number = 1;

            // Act
            Action act = () => number.ThrowIf(() => new CustomException()).MoreThan(0);

            // Result
            act.ShouldThrow<CustomException>();
        }

        [TestCase]
        public void ThrowIfMoreThan_NullableNumberIsNotGreaterThan_DoNotThrowArgumentException()
        {
            // Arrange
            int? number = 0;

            // Act
            Action act = () => number.ThrowIf(() => number).MoreThan(0);

            // Result
            act.ShouldNotThrow<ArgumentException>();
        }

        [TestCase(1,    "Parameter 'number' should not be more than '0'.")]
        [TestCase(null, "Parameter 'number' should not be null.")]
        public void ThrowIfMoreThan_NullableNumberIsGreaterThan_ThrowArgumentException(int? number, string message)
        {
            // Arrange

            // Act
            Action act = () => number.ThrowIf(() => number).MoreThan(0);

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(exception => exception.Message.Contains(message));
        }

        [TestCase(1)]
        [TestCase(null)]
        public void ThrowIfMoreThan_NullableNumberIsGreaterThan_ThrowCustomException(int? number)
        {
            // Arrange

            // Act
            Action act = () => number.ThrowIf(() => new CustomException()).MoreThan(0);

            // Result
            act.ShouldThrow<CustomException>();
        }
    }
}