﻿using System;
using System.Diagnostics.CodeAnalysis;
using ArgumentAssertions.Bool;
using ArgumentAssertions.DateTime;
using ArgumentAssertions.Generic;
using ArgumentAssertions.Numeric;
using ArgumentAssertions.String;
using FluentAssertions;
using NUnit.Framework;

namespace ArgumentAssertions.Test.Generic
{
    [TestFixture]
    [Category("Unit")]
    [ExcludeFromCodeCoverage]
    public class GenericPropertyIsTest
    {
        #region Test Data

        public class A
        {
            public string Text { get; set; }

            public B Bb { get; set; }
        }

        public class B
        {
            public int Number { get; set; }

            public C? Cc { get; set; }
        }

        public struct C
        {
            public System.DateTime Time { get; set; }

            public bool? Flag { get; set; }
        }

        #endregion

        [TestCase]
        public void ThrowIfPropertyIs_TextIsNotEmptyAndLessThan6_DoNotThrowArgumentException()
        {
            // Arrange
            var a = new A
            {
                Text = "text",
                Bb = new B
                {
                    Cc = new C()
                }
            };

            // Act
            Action act = () => a.ThrowIf(() => a).PropertyIs(arg => arg.Text).Empty()
                .OrIf.PropertyIs(s => s.Length).MoreThan(6);

            // Result
            act.ShouldNotThrow<ArgumentException>();
        }

        [TestCase]
        public void ThrowIfPropertyIs_TextIsNotEmptyButMoreThan6_ThrowArgumentException()
        {
            // Arrange
            var a = new A
            {
                Text = "text is longer",
                Bb = new B
                {
                    Cc = new C()
                }
            };

            // Act
            Action act = () => a.ThrowIf(() => a).PropertyIs(arg => arg.Text).Empty()
                .OrIf.PropertyIs(s => s.Length).MoreThan(6);

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(exception => exception.Message.Contains("Parameter 'a.Text.Length' should not be more than '6'."));
        }

        [TestCase]
        public void ThrowIfPropertyIs_TextIsEmpty_ThrowArgumentException()
        {
            // Arrange
            var a = new A
            {
                Text = "",
                Bb = new B
                {
                    Cc = new C()
                }
            };

            // Act
            Action act = () => a.ThrowIf(() => a).PropertyIs(arg => arg.Text).Empty()
                .OrIf.PropertyIs(s => s.Length).MoreThan(6);

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(exception => exception.Message.Contains("Parameter 'a.Text' should not be empty."));
        }

        [TestCase]
        public void ThrowIfPropertyIs_CcIsNull_ThrowArgumentException()
        {
            // Arrange
            var a = new A
            {
                Text = "",
                Bb = new B()
            };

            // Act
            Action act = () => a.ThrowIf(() => a).PropertyIs(arg => arg.Bb.Cc).Null();

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(exception => exception.Message.Contains("Parameter 'a.Bb.Cc' should not be null."));
        }

        [TestCase]
        public void ThrowIfPropertyIs_TimeIsAfterNow_ThrowArgumentException()
        {
            // Arrange
            var a = new A
            {
                Text = "",
                Bb = new B
                {
                    Cc = new C
                    {
                        Time = System.DateTime.Now.AddDays(1)
                    }
                }
            };

            // Act
            Action act = () => a.ThrowIf(() => a)
                .PropertyIs(arg => arg.Bb)
                .PropertyIs(b => b.Cc).Null()
                .OrIf.PropertyIs(c => c.Value.Time).AfterThanNow();

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(exception => exception.Message.Contains($"Parameter 'a.Bb.Cc.Value.Time' should not be after"));
        }

        [TestCase]
        public void ThrowIfPropertyIs_FlagIsFalse_ThrowArgumentException()
        {
            // Arrange
            var a = new A
            {
                Text = "",
                Bb = new B
                {
                    Cc = new C
                    {
                        Flag = false
                    }
                }
            };

            // Act
            Action act = () => a.ThrowIf(() => a)
                .PropertyIs(arg => arg.Bb)
                .PropertyIs(b => b.Cc).Null()
                .OrIf.PropertyIs(c => c.Value.Flag).False();

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(exception => exception.Message.Contains($"Parameter 'a.Bb.Cc.Value.Flag' should not be 'false'."));
        }
    }
}