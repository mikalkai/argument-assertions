﻿using System;
using System.Diagnostics.CodeAnalysis;
using ArgumentAssertions.Generic;
using FluentAssertions;
using NUnit.Framework;

namespace ArgumentAssertions.Test.Generic
{
    [TestFixture]
    [Category("Unit")]
    [ExcludeFromCodeCoverage]
    [SuppressMessage("ReSharper", "PossibleInvalidOperationException")]
    public class GenericValidTest
    {
        #region Test Data

        public class A
        {
            public bool IsValid { get; set; }
        }

        public class CustomException : Exception {}

        #endregion

        [TestCase]
        public void ThrowIfNotValid_AIsValid_DoNotThrowArgumentException()
        {
            // Arrange
            var a = new A
            {
                IsValid = true
            };

            // Act
            Action act = () => a.ThrowIfNot(() => a).Valid(arg => arg.IsValid);

            // Result
            act.ShouldNotThrow<ArgumentException>();
        }

        [TestCase(true, "This is custom message.")]
        [TestCase(false, "Parameter 'a' should be valid.")]
        public void ThrowIfNotValid_AIsNotValid_ThrowArgumentException(bool useCustomMessage, string message)
        {
            // Arrange
            var a = new A
            {
                IsValid = false
            };

            // Act
            var actMessage = message;
            Action act = () =>
            {
                if (useCustomMessage)
                {
                    a.ThrowIfNot(() => a).Valid(arg => arg.IsValid, actMessage);
                    return;
                }
                a.ThrowIfNot(() => a).Valid(arg => arg.IsValid);
            };

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(exception => exception.Message.Contains(message));
        }

        [TestCase]
        public void ThrowIfNotValid_AIsNotValid_ThrowCustomException()
        {
            // Arrange
            var a = new A
            {
                IsValid = false
            };

            // Act
            Action act = () => a.ThrowIfNot(() => new CustomException()).Valid(arg => arg.IsValid);

            // Result
            act.ShouldThrow<CustomException>();
        }

        [TestCase]
        public void ThrowIfNotValid_NumberIsValid_DoNotThrowArgumentException()
        {
            // Arrange
            int? number = 1;

            // Act
            Action act = () => number.ThrowIfNot(() => number).Valid(i => i.Value == 1);

            // Result
            act.ShouldNotThrow<ArgumentException>();
        }

        [TestCase(true, "This is custom message.")]
        [TestCase(false, "Parameter 'number' should be valid.")]
        public void ThrowIfNotValid_NumberIsNotValid_ThrowArgumentException(bool useCustomMessage, string message)
        {
            // Arrange
            int? number = 0;

            // Act
            var actMessage = message;
            Action act = () =>
            {
                if (useCustomMessage)
                {
                    number.ThrowIfNot(() => number).Valid(i => i.Value == 1, actMessage);
                    return;
                }
                number.ThrowIfNot(() => number).Valid(i => i.Value == 1);
            };

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(exception => exception.Message.Contains(message));
        }

        [TestCase]
        public void ThrowIfNotValid_NumberIsNotValid_ThrowCustomException()
        {
            // Arrange
            int? number = 0;

            // Act
            Action act = () => number.ThrowIfNot(() => new CustomException()).Valid(i => i.Value == 1);

            // Result
            act.ShouldThrow<CustomException>();
        }
    }
}