﻿using System;
using System.Diagnostics.CodeAnalysis;
using ArgumentAssertions.Generic;
using FluentAssertions;
using NUnit.Framework;

namespace ArgumentAssertions.Test.Generic
{
    [TestFixture]
    [Category("Unit")]
    [ExcludeFromCodeCoverage]
    public class GenericEqualToTest
    {
        #region Test Data

        public class CustomException : Exception {}

        #endregion

        [TestCase]
        public void ThrowIfEqualTo_ObjIsNotEqualToOtherObj_DoNotThrowArgumentException()
        {
            // Arrange
            var obj = new object();
            var otherObj = new object();

            // Act
            Action act = () => obj.ThrowIf(() => obj).EqualTo(otherObj);

            // Result
            act.ShouldNotThrow<ArgumentException>();
        }

        [TestCase]
        public void ThrowIfEqualTo_ObjIsEqualToObj_ThrowArgumentException()
        {
            // Arrange
            var obj = new object();

            // Act
            Action act = () => obj.ThrowIf(() => obj).EqualTo(obj);

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(exception => exception.Message.Contains($"Parameter 'obj' should not be equal to '{obj}'."));
        }

        [TestCase]
        public void ThrowIfEqualTo_ObjIsEqualToObj_ThrowCustomException()
        {
            // Arrange
            var obj = new object();

            // Act
            Action act = () => obj.ThrowIf(() => new CustomException()).EqualTo(obj);

            // Result
            act.ShouldThrow<CustomException>();
        }

        [TestCase]
        public void ThrowIfEqualTo_NumberIsNotEqualToOtherNumber_DoNotThrowArgumentException()
        {
            // Arrange
            int? number = 1;

            // Act
            Action act = () => number.ThrowIf(() => number).EqualTo(2);

            // Result
            act.ShouldNotThrow<ArgumentException>();
        }

        [TestCase]
        public void ThrowIfEqualTo_NumberIsNotEqualToNumber_ThrowArgumentException()
        {
            // Arrange
            int? number = 1;

            // Act
            Action act = () => number.ThrowIf(() => number).EqualTo(1);

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(exception => exception.Message.Contains($"Parameter 'number' should not be equal to '1'."));
        }

        [TestCase]
        public void ThrowIfEqualTo_NumberIsNotEqualToNumber_ThrowCustomException()
        {
            // Arrange
            int? number = 1;

            // Act
            Action act = () => number.ThrowIf(() => new CustomException()).EqualTo(1);

            // Result
            act.ShouldThrow<CustomException>();
        }
    }
}