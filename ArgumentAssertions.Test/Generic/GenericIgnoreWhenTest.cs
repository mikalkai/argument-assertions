﻿using System;
using System.Diagnostics.CodeAnalysis;
using ArgumentAssertions.Generic;
using FluentAssertions;
using NUnit.Framework;

namespace ArgumentAssertions.Test.Generic
{
    [TestFixture]
    [Category("Unit")]
    [ExcludeFromCodeCoverage]
    public class GenericIgnoreWhenTest
    {
        public class A
        {
            public string Text { get; set; }

            public bool IsTemporary { get; set; }
        }

        [TestCase(true)]
        [TestCase(false)]
        public void IgnoreWhen_TextIsNotNull_DoNotThrowArgumentNullException(bool isTemporary)
        {
            // Arrange
            var a = new A
            {
                Text = "not-null",
                IsTemporary = isTemporary
            };

            // Act
            Action act = () => a.Text.ThrowIf(() => a.Text).IgnoreWhen(() => a.IsTemporary).Null();

            // Result
            act.ShouldNotThrow<ArgumentException>();
        }

        [TestCase(true)]
        [TestCase(false)]
        public void IgnoreWhen_TextIsNull_ThrowArgumentNullExceptionWhenIsTemporaryIsFalse(bool isTemporary)
        {
            // Arrange
            var a = new A
            {
                Text = null,
                IsTemporary = isTemporary
            };

            // Act
            Action act = () => a.Text.ThrowIf(() => a.Text).IgnoreWhen(() => a.IsTemporary).Null();

            // Result
            if (isTemporary)
            {
                act.ShouldNotThrow<ArgumentNullException>();
            }
            else
            {
                act.ShouldThrow<ArgumentNullException>()
                    .Where(exception => exception.Message.Contains("Parameter 'a.Text' should not be null."));
            }
        }
    }
}