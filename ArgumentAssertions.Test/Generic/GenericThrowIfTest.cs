﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq.Expressions;
using ArgumentAssertions.Generic;
using ArgumentAssertions.Generic.Internal;
using FluentAssertions;
using NUnit.Framework;

namespace ArgumentAssertions.Test.Generic
{
    [TestFixture]
    [Category("Unit")]
    [ExcludeFromCodeCoverage]
    public class GenericThrowIfTest
    {
        #region Test Data

        public class A {}

        public class CustomException : Exception {}

        #endregion

        #region Property Expression

        [TestCase]
        public void ThrowIf_PropertyExpressionIsValid_ReturnIfAssertion()
        {
            // Arrange
            var a = new A();

            // Act
            var throwIf = a.ThrowIf(() => a);

            // Result
            throwIf.Should().NotBeNull();
            throwIf.Should().BeOfType<IfAssertion<A>>();
            throwIf.Value.Should().Be(a);

            var context = throwIf.Context();
            context.Condition.Should().Be(Condition.If);
            context.ParameterName.Should().Be("a");
            context.ExceptionExpression.Should().BeNull();
        }

        [TestCase]
        public void ThrowIf_NullablePropertyExpressionIsValid_ReturnIfAssertion()
        {
            // Arrange
            int? number = 1;

            // Act
            var throwIf = number.ThrowIf(() => number);

            // Result
            throwIf.Should().NotBeNull();
            throwIf.Should().BeOfType<NullableIfAssertion<int>>();
            throwIf.Value.Should().Be(number);

            var context = throwIf.Context();
            context.Condition.Should().Be(Condition.If);
            context.ParameterName.Should().Be("number");
            context.ExceptionExpression.Should().BeNull();
        }

        [TestCase]
        public void ThrowIf_PropertyExpressionIsNull_ReturnIfAssertion()
        {
            // Arrange
            var a = new A();

            // Act
            var throwIf = a.ThrowIf((Expression<Func<A>>)null);

            // Result
            throwIf.Should().NotBeNull();
            throwIf.Should().BeOfType<IfAssertion<A>>();
            throwIf.Value.Should().Be(a);

            var context = throwIf.Context();
            context.Condition.Should().Be(Condition.If);
            context.ParameterName.Should().Be("undefined");
            context.ExceptionExpression.Should().BeNull();
        }

        [TestCase]
        public void ThrowIf_NullablePropertyExpressionIsNull_ReturnIfAssertion()
        {
            // Arrange
            int? number = 1;

            // Act
            var throwIf = number.ThrowIf((Expression<Func<int?>>)null);

            // Result
            throwIf.Should().NotBeNull();
            throwIf.Should().BeOfType<NullableIfAssertion<int>>();
            throwIf.Value.Should().Be(number);

            var context = throwIf.Context();
            context.Condition.Should().Be(Condition.If);
            context.ParameterName.Should().Be("undefined");
            context.ExceptionExpression.Should().BeNull();
        }

        [TestCase]
        public void ThrowIf_PropertyInExpressionIsNull_ReturnIfAssertion()
        {
            // Arrange
            var a = new A();

            // Act
            var throwIf = a.ThrowIf(() => (A)null);

            // Result
            throwIf.Should().NotBeNull();
            throwIf.Should().BeOfType<IfAssertion<A>>();
            throwIf.Value.Should().Be(a);

            var context = throwIf.Context();
            context.Condition.Should().Be(Condition.If);
            context.ParameterName.Should().Be("undefined");
            context.ExceptionExpression.Should().BeNull();
        }

        [TestCase]
        public void ThrowIf_NullablePropertyInExpressionIsNull_ReturnIfAssertion()
        {
            // Arrange
            int? number = 1;

            // Act
            var throwIf = number.ThrowIf(() => (int?)null);

            // Result
            throwIf.Should().NotBeNull();
            throwIf.Should().BeOfType<NullableIfAssertion<int>>();
            throwIf.Value.Should().Be(number);

            var context = throwIf.Context();
            context.Condition.Should().Be(Condition.If);
            context.ParameterName.Should().Be("undefined");
            context.ExceptionExpression.Should().BeNull();
        }

        #endregion

        #region Property Name

        [TestCase]
        public void ThrowIf_ParameterNameIsNotNull_ReturnIfAssertion()
        {
            // Arrange
            var a = new A();

            // Act
            var throwIf = a.ThrowIf("a");

            // Result
            throwIf.Should().NotBeNull();
            throwIf.Should().BeOfType<IfAssertion<A>>();
            throwIf.Value.Should().Be(a);

            var context = throwIf.Context();
            context.Condition.Should().Be(Condition.If);
            context.ParameterName.Should().Be("a");
            context.ExceptionExpression.Should().BeNull();
        }

        [TestCase]
        public void ThrowIf_NullableParameterNameIsNotNull_ReturnIfAssertion()
        {
            // Arrange
            int? number = 1;

            // Act
            var throwIf = number.ThrowIf("number");

            // Result
            throwIf.Should().NotBeNull();
            throwIf.Should().BeOfType<NullableIfAssertion<int>>();
            throwIf.Value.Should().Be(number);

            var context = throwIf.Context();
            context.Condition.Should().Be(Condition.If);
            context.ParameterName.Should().Be("number");
            context.ExceptionExpression.Should().BeNull();
        }

        [TestCase]
        public void ThrowIf_ParameterNameIsNull_ThrowArgumentNullException()
        {
            // Arrange
            var a = new A();

            // Act
            Action act = () => a.ThrowIf((string)null);

            // Result
            act.ShouldThrow<ArgumentNullException>()
                .Where(exception => exception.ParamName.Equals("parameterName"));
        }

        [TestCase]
        public void ThrowIf_NullableParameterNameIsNull_ThrowArgumentNullException()
        {
            // Arrange
            int? number = 1;

            // Act
            Action act = () => number.ThrowIf((string)null);

            // Result
            act.ShouldThrow<ArgumentNullException>()
                .Where(exception => exception.ParamName.Equals("parameterName"));
        }

        #endregion

        #region Exception Expression

        [TestCase]
        public void ThrowIf_ExceptionExpressionIsNotNull_ReturnIfAssertion()
        {
            // Arrange
            var a = new A();

            // Act
            var throwIf = a.ThrowIf(() => new CustomException());

            // Result
            throwIf.Should().NotBeNull();
            throwIf.Should().BeOfType<IfAssertion<A>>();
            throwIf.Value.Should().Be(a);

            var context = throwIf.Context();
            context.Condition.Should().Be(Condition.If);
            context.ParameterName.Should().BeNull();
            context.ExceptionExpression.Should().NotBeNull();
            context.ExceptionExpression.Compile().Invoke().Should().BeOfType<CustomException>();
        }

        [TestCase]
        public void ThrowIf_NullableExceptionExpressionIsNotNull_ReturnIfAssertion()
        {
            // Arrange
            int? number = 1;

            // Act
            var throwIf = number.ThrowIf(() => new CustomException());

            // Result
            throwIf.Should().NotBeNull();
            throwIf.Should().BeOfType<NullableIfAssertion<int>>();
            throwIf.Value.Should().Be(number);

            var context = throwIf.Context();
            context.Condition.Should().Be(Condition.If);
            context.ParameterName.Should().BeNull();
            context.ExceptionExpression.Should().NotBeNull();
            context.ExceptionExpression.Compile().Invoke().Should().BeOfType<CustomException>();
        }

        [TestCase]
        public void ThrowIf_ExceptionExpressionIsNull_ThrowArgumentNullException()
        {
            // Arrange
            var a = new A();

            // Act
            Action act = () => a.ThrowIf((Expression<Func<Exception>>)null);

            // Result
            act.ShouldThrow<ArgumentNullException>()
                .Where(exception => exception.ParamName.Equals("exceptionExpression"));
        }

        [TestCase]
        public void ThrowIf_NullableExceptionExpressionIsNull_ThrowArgumentNullException()
        {
            // Arrange
            int? number = 1;

            // Act
            Action act = () => number.ThrowIf((Expression<Func<Exception>>)null);

            // Result
            act.ShouldThrow<ArgumentNullException>()
                .Where(exception => exception.ParamName.Equals("exceptionExpression"));
        }

        [TestCase]
        public void ThrowIf_AssertNullExceptionFromExpression_ThrowNullExceptionReferenceException()
        {
            // Arrange
            var a = new A();

            // Act
            Action act = () =>
            {
                var throwIf = a.ThrowIf(() => (Exception)null);
                throwIf.Assert(true, throwIf.Context().ExceptionExpression);
            };

            // Result
            act.ShouldThrow<NullExceptionReferenceException>();
        }

        [TestCase]
        public void ThrowIf_NullableAssertNullExceptionFromExpression_ThrowNullExceptionReferenceException()
        {
            // Arrange
            int? number = 1;

            // Act
            Action act = () =>
            {
                var throwIf = number.ThrowIf(() => (Exception)null);
                throwIf.Assert(true, throwIf.Context().ExceptionExpression);
            };

            // Result
            act.ShouldThrow<NullExceptionReferenceException>();
        }

        #endregion
    }
}