﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq.Expressions;
using ArgumentAssertions.Generic;
using ArgumentAssertions.Generic.Internal;
using FluentAssertions;
using NUnit.Framework;

namespace ArgumentAssertions.Test.Generic
{
    [TestFixture]
    [Category("Unit")]
    [ExcludeFromCodeCoverage]
    public class GenericThrowIfNotTest
    {
        #region Test Data

        public class A {}

        public class CustomException : Exception {}

        #endregion

        #region Property Expression

        [TestCase]
        public void ThrowIfNot_PropertyExpressionIsValid_ReturnIfAssertion()
        {
            // Arrange
            var a = new A();

            // Act
            var throwIfNot = a.ThrowIfNot(() => a);

            // Result
            throwIfNot.Should().NotBeNull();
            throwIfNot.Should().BeOfType<IfNotAssertion<A>>();
            throwIfNot.Value.Should().Be(a);

            var context = throwIfNot.Context();
            context.Condition.Should().Be(Condition.IfNot);
            context.ParameterName.Should().Be("a");
            context.ExceptionExpression.Should().BeNull();
        }

        [TestCase]
        public void ThrowIfNot_NullablePropertyExpressionIsValid_ReturnIfAssertion()
        {
            // Arrange
            int? number = 1;

            // Act
            var throwIfNot = number.ThrowIfNot(() => number);

            // Result
            throwIfNot.Should().NotBeNull();
            throwIfNot.Should().BeOfType<NullableIfNotAssertion<int>>();
            throwIfNot.Value.Should().Be(number);

            var context = throwIfNot.Context();
            context.Condition.Should().Be(Condition.IfNot);
            context.ParameterName.Should().Be("number");
            context.ExceptionExpression.Should().BeNull();
        }

        [TestCase]
        public void ThrowIfNot_PropertyExpressionIsNull_ReturnIfAssertion()
        {
            // Arrange
            var a = new A();

            // Act
            var throwIfNot = a.ThrowIfNot((Expression<Func<A>>)null);

            // Result
            throwIfNot.Should().NotBeNull();
            throwIfNot.Should().BeOfType<IfNotAssertion<A>>();
            throwIfNot.Value.Should().Be(a);

            var context = throwIfNot.Context();
            context.Condition.Should().Be(Condition.IfNot);
            context.ParameterName.Should().Be("undefined");
            context.ExceptionExpression.Should().BeNull();
        }

        [TestCase]
        public void ThrowIfNot_NullablePropertyExpressionIsNull_ReturnIfAssertion()
        {
            // Arrange
            int? number = 1;

            // Act
            var throwIfNot = number.ThrowIfNot((Expression<Func<int?>>)null);

            // Result
            throwIfNot.Should().NotBeNull();
            throwIfNot.Should().BeOfType<NullableIfNotAssertion<int>>();
            throwIfNot.Value.Should().Be(number);

            var context = throwIfNot.Context();
            context.Condition.Should().Be(Condition.IfNot);
            context.ParameterName.Should().Be("undefined");
            context.ExceptionExpression.Should().BeNull();
        }

        [TestCase]
        public void ThrowIfNot_PropertyInExpressionIsNull_ReturnIfAssertion()
        {
            // Arrange
            var a = new A();

            // Act
            var throwIfNot = a.ThrowIfNot(() => (A)null);

            // Result
            throwIfNot.Should().NotBeNull();
            throwIfNot.Should().BeOfType<IfNotAssertion<A>>();
            throwIfNot.Value.Should().Be(a);

            var context = throwIfNot.Context();
            context.Condition.Should().Be(Condition.IfNot);
            context.ParameterName.Should().Be("undefined");
            context.ExceptionExpression.Should().BeNull();
        }

        [TestCase]
        public void ThrowIfNot_NullablePropertyInExpressionIsNull_ReturnIfAssertion()
        {
            // Arrange
            int? number = 1;

            // Act
            var throwIfNot = number.ThrowIfNot(() => (int?)null);

            // Result
            throwIfNot.Should().NotBeNull();
            throwIfNot.Should().BeOfType<NullableIfNotAssertion<int>>();
            throwIfNot.Value.Should().Be(number);

            var context = throwIfNot.Context();
            context.Condition.Should().Be(Condition.IfNot);
            context.ParameterName.Should().Be("undefined");
            context.ExceptionExpression.Should().BeNull();
        }

        #endregion

        #region Property Name

        [TestCase]
        public void ThrowIfNot_ParameterNameIsNotNull_ReturnIfAssertion()
        {
            // Arrange
            var a = new A();

            // Act
            var throwIfNot = a.ThrowIfNot("a");

            // Result
            throwIfNot.Should().NotBeNull();
            throwIfNot.Should().BeOfType<IfNotAssertion<A>>();
            throwIfNot.Value.Should().Be(a);

            var context = throwIfNot.Context();
            context.Condition.Should().Be(Condition.IfNot);
            context.ParameterName.Should().Be("a");
            context.ExceptionExpression.Should().BeNull();
        }

        [TestCase]
        public void ThrowIfNot_NullableParameterNameIsNotNull_ReturnIfAssertion()
        {
            // Arrange
            int? number = 1;

            // Act
            var throwIfNot = number.ThrowIfNot("number");

            // Result
            throwIfNot.Should().NotBeNull();
            throwIfNot.Should().BeOfType<NullableIfNotAssertion<int>>();
            throwIfNot.Value.Should().Be(number);

            var context = throwIfNot.Context();
            context.Condition.Should().Be(Condition.IfNot);
            context.ParameterName.Should().Be("number");
            context.ExceptionExpression.Should().BeNull();
        }

        [TestCase]
        public void ThrowIfNot_ParameterNameIsNull_ThrowArgumentNullException()
        {
            // Arrange
            var a = new A();

            // Act
            Action act = () => a.ThrowIfNot((string)null);

            // Result
            act.ShouldThrow<ArgumentNullException>()
                .Where(exception => exception.ParamName.Equals("parameterName"));
        }

        [TestCase]
        public void ThrowIfNot_NullableParameterNameIsNull_ThrowArgumentNullException()
        {
            // Arrange
            int? number = 1;

            // Act
            Action act = () => number.ThrowIfNot((string)null);

            // Result
            act.ShouldThrow<ArgumentNullException>()
                .Where(exception => exception.ParamName.Equals("parameterName"));
        }

        #endregion

        #region Exception Expression

        [TestCase]
        public void ThrowIfNot_ExceptionExpressionIsNotNull_ReturnIfAssertion()
        {
            // Arrange
            var a = new A();

            // Act
            var throwIfNot = a.ThrowIfNot(() => new CustomException());

            // Result
            throwIfNot.Should().NotBeNull();
            throwIfNot.Should().BeOfType<IfNotAssertion<A>>();
            throwIfNot.Value.Should().Be(a);

            var context = throwIfNot.Context();
            context.Condition.Should().Be(Condition.IfNot);
            context.ParameterName.Should().BeNull();
            context.ExceptionExpression.Should().NotBeNull();
            context.ExceptionExpression.Compile().Invoke().Should().BeOfType<CustomException>();
        }

        [TestCase]
        public void ThrowIfNot_NullableExceptionExpressionIsNotNull_ReturnIfAssertion()
        {
            // Arrange
            int? number = 1;

            // Act
            var throwIfNot = number.ThrowIfNot(() => new CustomException());

            // Result
            throwIfNot.Should().NotBeNull();
            throwIfNot.Should().BeOfType<NullableIfNotAssertion<int>>();
            throwIfNot.Value.Should().Be(number);

            var context = throwIfNot.Context();
            context.Condition.Should().Be(Condition.IfNot);
            context.ParameterName.Should().BeNull();
            context.ExceptionExpression.Should().NotBeNull();
            context.ExceptionExpression.Compile().Invoke().Should().BeOfType<CustomException>();
        }

        [TestCase]
        public void ThrowIfNot_ExceptionExpressionIsNull_ThrowArgumentNullException()
        {
            // Arrange
            var a = new A();

            // Act
            Action act = () => a.ThrowIfNot((Expression<Func<Exception>>)null);

            // Result
            act.ShouldThrow<ArgumentNullException>()
                .Where(exception => exception.ParamName.Equals("exceptionExpression"));
        }

        [TestCase]
        public void ThrowIfNot_NullableExceptionExpressionIsNull_ThrowArgumentNullException()
        {
            // Arrange
            int? number = 1;

            // Act
            Action act = () => number.ThrowIfNot((Expression<Func<Exception>>)null);

            // Result
            act.ShouldThrow<ArgumentNullException>()
                .Where(exception => exception.ParamName.Equals("exceptionExpression"));
        }

        [TestCase]
        public void ThrowIfNot_AssertNullExceptionFromExpression_ThrowNullExceptionReferenceException()
        {
            // Arrange
            var a = new A();

            // Act
            Action act = () =>
            {
                var throwIfNot = a.ThrowIfNot(() => (Exception)null);
                throwIfNot.Assert(false, throwIfNot.Context().ExceptionExpression);
            };

            // Result
            act.ShouldThrow<NullExceptionReferenceException>();
        }

        [TestCase]
        public void ThrowIfNot_NullableAssertNullExceptionFromExpression_ThrowNullExceptionReferenceException()
        {
            // Arrange
            int? number = 1;

            // Act
            Action act = () =>
            {
                var throwIfNot = number.ThrowIfNot(() => (Exception)null);
                throwIfNot.Assert(false, throwIfNot.Context().ExceptionExpression);
            };

            // Result
            act.ShouldThrow<NullExceptionReferenceException>();
        }

        #endregion
    }
}