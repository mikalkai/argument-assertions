﻿using System;
using System.Diagnostics.CodeAnalysis;
using ArgumentAssertions.Generic;
using FluentAssertions;
using NUnit.Framework;

namespace ArgumentAssertions.Test.Generic
{
    [TestFixture]
    [Category("Unit")]
    [ExcludeFromCodeCoverage]
    [SuppressMessage("ReSharper", "ExpressionIsAlwaysNull")]
    public class GenericNullTest
    {
        #region Test Data

        public class A
        {
            public B Bb { get; set; }
        }

        public class B
        {
            public C Cc { get; set; }
        }

        public class C
        {
            public D? Dd { get; set; }
        }

        public struct D {}

        public class CustomException : Exception {}

        #endregion

        [TestCase]
        public void ThrowIfNull_AIsNull_ThrowArgumentNullException()
        {
            // Arrange
            A a = null;

            // Act
            Action act = () => a.ThrowIf(() => a).Null();

            // Result
            act.ShouldThrow<ArgumentNullException>()
                .Where(exception => exception.Message.Contains("Parameter 'a' should not be null."));
        }

        [TestCase]
        public void ThrowIfNull_AIsNotNull_DoNotThrowArgumentNullException()
        {
            // Arrange
            var a = new A();

            // Act
            Action act = () => a.ThrowIf(() => a).Null();

            // Result
            act.ShouldNotThrow<ArgumentNullException>();
        }

        [TestCase]
        public void ThrowIfNull_BIsNull_ThrowArgumentNullException()
        {
            // Arrange
            var a = new A();

            // Act
            Action act = () => { a.Bb.ThrowIf(() => a.Bb).Null(); };

            // Result
            act.ShouldThrow<ArgumentNullException>()
                .Where(exception => exception.Message.Contains("Parameter 'a.Bb' should not be null."));
        }

        [TestCase]
        public void ThrowIfNull_BIsNotNull_DoNotThrowArgumentNullException()
        {
            // Arrange
            var a = new A
            {
                Bb = new B()
            };

            // Act
            Action act = () => { a.Bb.ThrowIf(() => a.Bb).Null(); };

            // Result
            act.ShouldNotThrow<ArgumentNullException>();
        }

        [TestCase]
        public void ThrowIfNull_CIsNull_ThrowArgumentNullException()
        {
            // Arrange
            var a = new A
            {
                Bb = new B()
            };

            // Act
            Action act = () => { a.Bb.Cc.ThrowIf(() => a.Bb.Cc).Null(); };

            // Result
            act.ShouldThrow<ArgumentNullException>()
                .Where(exception => exception.Message.Contains("Parameter 'a.Bb.Cc' should not be null."));
        }

        [TestCase]
        public void ThrowIfNull_CIsNotNull_DoNotThrowArgumentNullException()
        {
            // Arrange
            var a = new A
            {
                Bb = new B
                {
                    Cc = new C()
                }
            };

            // Act
            Action act = () => { a.Bb.Cc.ThrowIf(() => a.Bb.Cc).Null(); };

            // Result
            act.ShouldNotThrow<ArgumentNullException>();
        }

        [TestCase]
        public void ThrowIfNull_DIsNull_ThrowArgumentNullException()
        {
            // Arrange
            var a = new A
            {
                Bb = new B
                {
                    Cc = new C()
                }
            };

            // Act
            Action act = () => { a.Bb.Cc.Dd.ThrowIf(() => a.Bb.Cc.Dd).Null(); };

            // Result
            act.ShouldThrow<ArgumentNullException>()
                .Where(exception => exception.Message.Contains("Parameter 'a.Bb.Cc.Dd' should not be null."));
        }

        [TestCase]
        public void ThrowIfNull_DIsNotNull_ThrowArgumentNullException()
        {
            // Arrange
            var a = new A
            {
                Bb = new B
                {
                    Cc = new C
                    {
                        Dd = new D()
                    }
                }
            };

            // Act
            Action act = () => { a.Bb.Cc.Dd.ThrowIf(() => a.Bb.Cc.Dd).Null(); };

            // Result
            act.ShouldNotThrow<ArgumentNullException>();
        }

        [TestCase]
        public void ThrowIfNull_CIsNull_ThrowCustomException()
        {
            // Arrange
            var a = new A
            {
                Bb = new B()
            };

            // Act
            Action act = () => { a.Bb.Cc.ThrowIf(() => new CustomException()).Null(); };

            // Result
            act.ShouldThrow<CustomException>();
        }

        [TestCase]
        public void ThrowIfNull_DIsNull_ThrowCustomException()
        {
            // Arrange
            var a = new A
            {
                Bb = new B
                {
                    Cc = new C()
                }
            };

            // Act
            Action act = () => { a.Bb.Cc.Dd.ThrowIf(() => new CustomException()).Null(); };

            // Result
            act.ShouldThrow<CustomException>();
        }
    }
}