﻿using System;
using System.Diagnostics.CodeAnalysis;
using ArgumentAssertions.Generic;
using ArgumentAssertions.String;
using FluentAssertions;
using NUnit.Framework;

namespace ArgumentAssertions.Test.String
{
    [TestFixture]
    [Category("Unit")]
    [ExcludeFromCodeCoverage]
    [SuppressMessage("ReSharper", "ConvertToConstant.Local")]
    public class StringEqualToTest
    {
        [TestCase]
        public void ThrowIfEqualTo_TextIsNotEqualToOther_DoNotThrowArgumentException()
        {
            // Arrange
            var text = "text";
            var other = "other";

            // Act
            Action act = () => text.ThrowIf(() => text).EqualTo(other);

            // Result
            act.ShouldNotThrow<ArgumentException>();
        }

        [TestCase]
        public void ThrowIfEqualTo_TextIsEqualToOther_ThrowArgumentException()
        {
            // Arrange
            var text = "text";
            var other = "text";

            // Act
            Action act = () => text.ThrowIf(() => text).EqualTo(other);

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(exception => exception.Message.Contains("Parameter 'text' should not be equal to 'text'."));
        }

        [TestCase]
        public void ThrowIfEqualTo_TextIsEqualToOtherIgnoreCase_ThrowArgumentException()
        {
            // Arrange
            var text = "text";
            var other = "TEXT";

            // Act
            Action act = () => text.ThrowIf(() => text).EqualTo(other, StringComparison.OrdinalIgnoreCase);

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(exception => exception.Message.Contains("Parameter 'text' should not be equal to 'TEXT'."));
        }
    }
}