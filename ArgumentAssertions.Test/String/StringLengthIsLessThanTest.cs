﻿using System;
using System.Diagnostics.CodeAnalysis;
using ArgumentAssertions.Generic;
using ArgumentAssertions.String;
using FluentAssertions;
using NUnit.Framework;

namespace ArgumentAssertions.Test.String
{
    [TestFixture]
    [Category("Unit")]
    [ExcludeFromCodeCoverage]
    [SuppressMessage("ReSharper", "ConvertToConstant.Local")]
    public class StringLengthIsLessThanTest
    {
        [TestCase]
        public void ThrowIfLengthIsLessThan_TextLengthIsNotLessThanSix_DoNotThrowArgumentException()
        {
            // Arrange
            var text = "1234567";

            // Act
            Action act = () => text.ThrowIf(() => text).LengthIsLessThan(6);

            // Result
            act.ShouldNotThrow<ArgumentException>();
        }

        [TestCase]
        public void ThrowIfLengthIsLessThan_TextLengthIsLessThanSix_ThrowArgumentException()
        {
            // Arrange
            var text = "12345";

            // Act
            Action act = () => text.ThrowIf(() => text).LengthIsLessThan(6);

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(exception => exception.Message.Contains("Parameter 'text.Length' should not be less than '6'."));
        }

        [TestCase]
        public void ThrowIfLengthIsLessThan_TextLengthIsLessThanOther_ThrowArgumentException()
        {
            // Arrange
            var text = "12345";
            var other = "123456";

            // Act
            Action act = () => text.ThrowIf(() => text).LengthIsLessThan(other);

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(exception => exception.Message.Contains("Parameter 'text.Length' should not be less than '6'."));
        }

        [TestCase]
        [SuppressMessage("ReSharper", "ExpressionIsAlwaysNull")]
        public void ThrowIfLengthIsLessThan_TextIsNull_ThrowNullArgumentException()
        {
            // Arrange
            var text = (string) null;

            // Act
            Action act = () => text.ThrowIf(() => text).LengthIsLessThan(6);

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(exception => exception.Message.Contains("Parameter 'text' should not be null."));
        }
    }
}