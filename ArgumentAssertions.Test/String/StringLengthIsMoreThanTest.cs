﻿using System;
using System.Diagnostics.CodeAnalysis;
using ArgumentAssertions.Generic;
using ArgumentAssertions.String;
using FluentAssertions;
using NUnit.Framework;

namespace ArgumentAssertions.Test.String
{
    [TestFixture]
    [Category("Unit")]
    [ExcludeFromCodeCoverage]
    [SuppressMessage("ReSharper", "ConvertToConstant.Local")]
    public class StringLengthIsMoreThanTest
    {
        [TestCase]
        public void ThrowIfLengthIsMoreThan_TextLengthIsNotMoreThanSix_DoNotThrowArgumentException()
        {
            // Arrange
            var text = "12345";

            // Act
            Action act = () => text.ThrowIf(() => text).LengthIsMoreThan(6);

            // Result
            act.ShouldNotThrow<ArgumentException>();
        }

        [TestCase]
        public void ThrowIfLengthIsMoreThan_TextLengthIsMoreThanSix_ThrowArgumentException()
        {
            // Arrange
            var text = "1234567";

            // Act
            Action act = () => text.ThrowIf(() => text).LengthIsMoreThan(6);

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(exception => exception.Message.Contains("Parameter 'text.Length' should not be more than '6'."));
        }

        [TestCase]
        public void ThrowIfLengthIsMoreThan_TextLengthIsMoreThanOther_ThrowArgumentException()
        {
            // Arrange
            var text = "123456";
            var other = "12345";

            // Act
            Action act = () => text.ThrowIf(() => text).LengthIsMoreThan(other);

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(exception => exception.Message.Contains("Parameter 'text.Length' should not be more than '5'."));
        }

        [TestCase]
        [SuppressMessage("ReSharper", "ExpressionIsAlwaysNull")]
        public void ThrowIfLengthIsMoreThan_TextIsNull_ThrowNullArgumentException()
        {
            // Arrange
            var text = (string) null;

            // Act
            Action act = () => text.ThrowIf(() => text).LengthIsMoreThan(6);

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(exception => exception.Message.Contains("Parameter 'text' should not be null."));
        }
    }
}