﻿using System;
using System.Diagnostics.CodeAnalysis;
using ArgumentAssertions.Generic;
using ArgumentAssertions.String;
using FluentAssertions;
using NUnit.Framework;

namespace ArgumentAssertions.Test.String
{
    [TestFixture]
    [Category("Unit")]
    [ExcludeFromCodeCoverage]
    public class StringNullTest
    {
        #region Test Data

        public class A
        {
            public B Bb { get; set; }
        }

        public class B
        {
            public string Text { get; set; }
        }

        public class CustomException : Exception {}

        #endregion

        [TestCase]
        public void ThrowIfNull_TextIsNull_ThrowArgumentNullException()
        {
            // Arrange
            var a = new A
            {
                Bb = new B()
            };

            // Act
            Action act = () => a.Bb.Text.ThrowIf(() => a.Bb.Text).Null();

            // Result
            act.ShouldThrow<ArgumentNullException>()
                .Where(exception => exception.Message.Contains("Parameter 'a.Bb.Text' should not be null."));
        }

        [TestCase]
        public void ThrowIfNull_TextIsNull_ThrowCustomException()
        {
            // Arrange
            var a = new A
            {
                Bb = new B()
            };

            // Act
            Action act = () => a.Bb.Text.ThrowIf(() => new CustomException()).Null();

            // Result
            act.ShouldThrow<CustomException>();
        }

        [TestCase]
        public void ThrowIfNull_TextIsNotNull_DoNotThrowArgumentNullException()
        {
            // Arrange
            var a = new A
            {
                Bb = new B
                {
                    Text = "not-null"
                }
            };

            // Act
            Action act = () => a.Bb.Text.ThrowIf(() => a.Bb.Text).Null();

            // Result
            act.ShouldNotThrow<ArgumentNullException>();
        }
    }
}