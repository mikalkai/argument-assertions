﻿using System;
using System.Diagnostics.CodeAnalysis;
using ArgumentAssertions.Generic;
using ArgumentAssertions.String;
using FluentAssertions;
using NUnit.Framework;

namespace ArgumentAssertions.Test.String
{
    [TestFixture]
    [Category("Unit")]
    [ExcludeFromCodeCoverage]
    public class StringEmptyTest
    {
        #region Test Data

        public class A
        {
            public string Text { get; set; }
        }

        public class CustomException : Exception {}

        #endregion

        [TestCase]
        public void ThrowIfEmpty_StringIsNotEmpty_DoNotThrowArgumentException()
        {
            // Arrange
            var a = new A
            {
                Text = "not empty"
            };

            // Act
            Action act = () => a.Text.ThrowIf(() => a.Text).Empty();

            // Result
            act.ShouldNotThrow<ArgumentException>();
        }

        [TestCase]
        public void ThrowIfEmpty_StringIsEmpty_ThrowArgumentException()
        {
            // Arrange
            var a = new A
            {
                Text = ""
            };

            // Act
            Action act = () => a.Text.ThrowIf(() => a.Text).Empty();

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(exception => exception.Message.Contains("Parameter 'a.Text' should not be empty."));
        }

        [TestCase]
        public void ThrowIfEmpty_StringIsNull_ThrowArgumentNullException()
        {
            // Arrange
            var a = new A
            {
                Text = null
            };

            // Act
            Action act = () => a.Text.ThrowIf(() => a.Text).Empty();

            // Result
            act.ShouldThrow<ArgumentNullException>()
                .Where(exception => exception.Message.Contains("Parameter 'a.Text' should not be null."));
        }

        [TestCase]
        public void ThrowIfEmpty_StringIsEmpty_ThrowCustomException()
        {
            // Arrange
            var a = new A
            {
                Text = ""
            };

            // Act
            Action act = () => a.Text.ThrowIf(() => new CustomException()).Empty();

            // Result
            act.ShouldThrow<CustomException>();
        }
    }
}