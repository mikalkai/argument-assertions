﻿using System;
using System.Diagnostics.CodeAnalysis;
using ArgumentAssertions.Generic;
using ArgumentAssertions.String;
using FluentAssertions;
using NUnit.Framework;

namespace ArgumentAssertions.Test.String
{
    [TestFixture]
    [Category("Unit")]
    [ExcludeFromCodeCoverage]
    [SuppressMessage("ReSharper", "ConvertToConstant.Local")]
    public class StringLengthIsLessOrEqualThanTest
    {
        [TestCase]
        public void ThrowIfLengthIsLessOrEqualThan_TextLengthIsNotLessThanSix_DoNotThrowArgumentException()
        {
            // Arrange
            var text = "1234567";

            // Act
            Action act = () => text.ThrowIf(() => text).LengthIsLessOrEqualThan(6);

            // Result
            act.ShouldNotThrow<ArgumentException>();
        }

        [TestCase("123456")]
        [TestCase("12345")]
        public void ThrowIfLengthIsLessOrEqualThan_TextLengthIsLessOrEqualThanSix_ThrowArgumentException(string value)
        {
            // Arrange
            var text = value;

            // Act
            Action act = () => text.ThrowIf(() => text).LengthIsLessOrEqualThan(6);

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(exception => exception.Message.Contains("Parameter 'text.Length' should not be less or equal than '6'."));
        }

        [TestCase]
        public void ThrowIfLengthIsLessOrEqualThan_TextLengthIsLessOrEqualThanOther_ThrowArgumentException()
        {
            // Arrange
            var text = "12345";
            var other = "123456";

            // Act
            Action act = () => text.ThrowIf(() => text).LengthIsLessOrEqualThan(other);

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(exception => exception.Message.Contains("Parameter 'text.Length' should not be less or equal than '6'."));
        }

        [TestCase]
        [SuppressMessage("ReSharper", "ExpressionIsAlwaysNull")]
        public void ThrowIfLengthIsLessOrEqualThan_TextIsNull_ThrowNullArgumentException()
        {
            // Arrange
            var text = (string) null;

            // Act
            Action act = () => text.ThrowIf(() => text).LengthIsLessOrEqualThan(6);

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(exception => exception.Message.Contains("Parameter 'text' should not be null."));
        }
    }
}