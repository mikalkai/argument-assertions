﻿using System;
using System.Diagnostics.CodeAnalysis;
using ArgumentAssertions.Generic;
using ArgumentAssertions.String;
using FluentAssertions;
using NUnit.Framework;

namespace ArgumentAssertions.Test.String
{
    [TestFixture]
    [Category("Unit")]
    [ExcludeFromCodeCoverage]
    [SuppressMessage("ReSharper", "ConvertToConstant.Local")]
    public class StringLengthIsTest
    {
        [TestCase]
        public void ThrowIfLengthIs_TextLengthIsNot0_DoNotThrowArgumentException()
        {
            // Arrange
            var text = "not zero";

            // Act
            Action act = () => text.ThrowIf(() => text).LengthIs(0);

            // Result
            act.ShouldNotThrow<ArgumentException>();
        }

        [TestCase]
        public void ThrowIfLengthIs_TextLengthIs0_ThrowArgumentException()
        {
            // Arrange
            var text = "";

            // Act
            Action act = () => text.ThrowIf(() => text).LengthIs(0);

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(exception => exception.Message.Contains("Parameter 'text.Length' should not be equal to '0'."));
        }

        [TestCase]
        public void ThrowIfLengthIs_TextLengthIsSameAsOther_ThrowArgumentException()
        {
            // Arrange
            var text = "text";
            var other = "same";

            // Act
            Action act = () => text.ThrowIf(() => text).LengthIs(other);

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(exception => exception.Message.Contains("Parameter 'text.Length' should not be equal to '4'."));
        }

        [TestCase]
        [SuppressMessage("ReSharper", "ExpressionIsAlwaysNull")]
        public void ThrowIfLengthIs_TextIsNull_ThrowNullArgumentException()
        {
            // Arrange
            var text = (string) null;

            // Act
            Action act = () => text.ThrowIf(() => text).LengthIs(0);

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(exception => exception.Message.Contains("Parameter 'text' should not be null."));
        }
    }
}