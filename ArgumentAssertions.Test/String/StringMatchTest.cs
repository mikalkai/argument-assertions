﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Text.RegularExpressions;
using ArgumentAssertions.Generic;
using ArgumentAssertions.String;
using FluentAssertions;
using NUnit.Framework;

namespace ArgumentAssertions.Test.String
{
    [TestFixture]
    [Category("Unit")]
    [ExcludeFromCodeCoverage]
    public class StringMatchTest
    {
        #region Test Data

        public class A
        {
            public string Text { get; set; }
        }

        public class CustomException : Exception {}

        #endregion

        [TestCase]
        public void ThrowIfMatch_TextContainsXXX_ThrowArgumentException()
        {
            // Arrange
            var a = new A
            {
                Text = "xxx is not allowed"
            };

            // Act
            Action act = () => a.Text.ThrowIf(() => a.Text).Match(new Regex("[xxx]"));

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(
                    exception => exception.Message.Contains("Parameter 'a.Text' should not match with pattern '[xxx]'."));
        }

        [TestCase]
        public void ThrowIfMatch_TextDoesNotContainXXX_DoNotThrowArgumentException()
        {
            // Arrange
            var a = new A
            {
                Text = "yyy is allowed"
            };

            // Act
            Action act = () => a.Text.ThrowIf(() => a.Text).Match(new Regex("[xxx]"));

            // Result
            act.ShouldNotThrow<ArgumentException>();
        }

        [TestCase]
        public void ThrowIfNotMatch_TextContainsYYY_DoNotThrowArgumentException()
        {
            // Arrange
            var a = new A
            {
                Text = "numbers are found: 123"
            };

            // Act
            Action act = () => a.Text.ThrowIfNot(() => a.Text).Match(new Regex("[\\d+]"));

            // Result
            act.ShouldNotThrow<ArgumentException>();
        }

        [TestCase]
        public void ThrowIfNotMatch_TextDoeNotContainYYY_ThrowArgumentException()
        {
            // Arrange
            var a = new A
            {
                Text = "numbers are not found"
            };

            // Act
            Action act = () => a.Text.ThrowIfNot(() => a.Text).Match(new Regex("\\d+"));

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(exception => exception.Message.Contains("Parameter 'a.Text' should match with pattern '\\d+'."));
        }

        [TestCase]
        public void ThrowIfNotMatch_TextDoeNotContainYYY_ThrowCustomException()
        {
            // Arrange
            var a = new A
            {
                Text = "numbers are not found"
            };

            // Act
            Action act = () => a.Text.ThrowIfNot(() => new CustomException()).Match(new Regex("\\d+"));

            // Result
            act.ShouldThrow<CustomException>();
        }
    }
}