﻿using System;
using System.Diagnostics.CodeAnalysis;
using ArgumentAssertions.Generic;
using ArgumentAssertions.String;
using FluentAssertions;
using NUnit.Framework;

namespace ArgumentAssertions.Test.String
{
    [TestFixture]
    [Category("Unit")]
    [ExcludeFromCodeCoverage]
    [SuppressMessage("ReSharper", "ConvertToConstant.Local")]
    public class StringLengthIsMoreOrEqualThanTest
    {
        [TestCase]
        public void ThrowIfLengthIsMoreOrEqualThan_TextLengthIsNotMoreThanSix_DoNotThrowArgumentException()
        {
            // Arrange
            var text = "12345";

            // Act
            Action act = () => text.ThrowIf(() => text).LengthIsMoreOrEqualThan(6);

            // Result
            act.ShouldNotThrow<ArgumentException>();
        }

        [TestCase("123456")]
        [TestCase("1234567")]
        public void ThrowIfLengthIsMoreOrEqualThan_TextLengthIsMoreOrEqualThanSix_ThrowArgumentException(string value)
        {
            // Arrange
            var text = value;

            // Act
            Action act = () => text.ThrowIf(() => text).LengthIsMoreOrEqualThan(6);

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(exception => exception.Message.Contains("Parameter 'text.Length' should not be more or equal than '6'."));
        }

        [TestCase]
        public void ThrowIfLengthIsMoreOrEqualThan_TextLengthIsMoreOrEqualThanOther_ThrowArgumentException()
        {
            // Arrange
            var text = "123456";
            var other = "12345";

            // Act
            Action act = () => text.ThrowIf(() => text).LengthIsMoreOrEqualThan(other);

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(exception => exception.Message.Contains("Parameter 'text.Length' should not be more or equal than '5'."));
        }

        [TestCase]
        [SuppressMessage("ReSharper", "ExpressionIsAlwaysNull")]
        public void ThrowIfLengthIsMoreOrEqualThan_TextIsNull_ThrowNullArgumentException()
        {
            // Arrange
            var text = (string) null;

            // Act
            Action act = () => text.ThrowIf(() => text).LengthIsMoreOrEqualThan(6);

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(exception => exception.Message.Contains("Parameter 'text' should not be null."));
        }
    }
}