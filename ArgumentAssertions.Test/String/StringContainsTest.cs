﻿using System;
using System.Diagnostics.CodeAnalysis;
using ArgumentAssertions.Generic;
using ArgumentAssertions.String;
using FluentAssertions;
using NUnit.Framework;

namespace ArgumentAssertions.Test.String
{
    [TestFixture]
    [Category("Unit")]
    [ExcludeFromCodeCoverage]
    public class StringContainsTest
    {
        #region Test Data

        public class A
        {
            public string Text { get; set; }
        }

        public class CustomException : Exception {}

        #endregion

        [TestCase]
        public void ThrowIfContains_TextDoesNotContainMe_DoNotThrowArgumentException()
        {
            // Arrange
            var a = new A
            {
                Text = "find you"
            };

            // Act
            Action act = () => a.Text.ThrowIf(() => a.Text).Contains("me");

            // Result
            act.ShouldNotThrow<ArgumentException>();
        }

        [TestCase]
        public void ThrowIfContains_TextContainsMe_ThrowArgumentException()
        {
            // Arrange
            var a = new A
            {
                Text = "find me"
            };

            // Act
            Action act = () => a.Text.ThrowIf(() => a.Text).Contains("me");

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(exception => exception.Message.Contains("Parameter 'a.Text' should not contain 'me'."));
        }

        [TestCase]
        public void ThrowIfContains_TextContainsMe_ThrowCustomException()
        {
            // Arrange
            var a = new A
            {
                Text = "find me"
            };

            // Act
            Action act = () => a.Text.ThrowIf(() => new CustomException()).Contains("me");

            // Result
            act.ShouldThrow<CustomException>();
        }
    }
}