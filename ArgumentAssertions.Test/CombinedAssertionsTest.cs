﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using ArgumentAssertions.Bool;
using ArgumentAssertions.DateTime;
using ArgumentAssertions.Enumerable;
using ArgumentAssertions.Generic;
using ArgumentAssertions.Numeric;
using ArgumentAssertions.String;
using FluentAssertions;
using NUnit.Framework;

namespace ArgumentAssertions.Test
{
    [TestFixture]
    [Category("Unit")]
    [ExcludeFromCodeCoverage]
    public class CombinedAssertionsTest
    {
        #region Test Data

        public class A
        {
            public string Text { get; set; }

            public int Number { get; set; }

            public double Double { get; set; }

            public decimal Decimal { get; set; }

            public short Short { get; set; }

            public bool Boolean { get; set; }

            public System.DateTime DateTime { get; set; }

            public IEnumerable<int> Numbers { get; set; }

            public B Bb { get; set; }
        }

        public class B
        {
            public int? NullableNumber { get; set; }

            public double? NullableDouble { get; set; }

            public decimal? NullableDecimal { get; set; }

            public short? NullableShort { get; set; }

            public bool? NullableBoolean { get; set; }

            public System.DateTime? NullableDateTime { get; set; }

            public C? Cc { get; set; }
        }

        public struct C { }

        #endregion

        [TestCase]
        public void ThrowIfCombined_NoPreconditions_DoNotThrowArgumentException()
        {
            // Arrange
            var a = new A
            {
                Text = "t",
                Number = 1,
                Double = 1.0,
                Decimal = 3,
                Short = 1,
                Boolean = true,
                DateTime = System.DateTime.Now.AddDays(-1),
                Numbers = new List<int> {1},
                Bb = new B
                {
                    NullableNumber = 1,
                    NullableDouble = 1.0,
                    NullableDecimal = 3,
                    NullableShort = 1,
                    NullableBoolean = true,
                    NullableDateTime = System.DateTime.Now.AddDays(-1),
                    Cc = new C()
                }
            };

            // Act
            Action act = () =>
            {
                a.ThrowIf(() => a).Null();
                a.Text.ThrowIf(() => a.Text).Empty();
                a.Number.ThrowIf(() => a.Number).LessThan(0);
                a.Double.ThrowIf(() => a.Double).MoreThan(1.5);
                a.Decimal.ThrowIf(() => a.Decimal).InBetween(0, 2);
                a.Short.ThrowIf(() => a.Short).EqualTo((short)2);
                a.Boolean.ThrowIf(() => a.Boolean).False();
                a.DateTime.ThrowIf(() => a.DateTime).AfterThanNow();
                a.Numbers.ThrowIf(() => a.Numbers).Empty();

                a.Bb.ThrowIf(() => a.Bb).Null();
                a.Bb.NullableNumber.ThrowIf(() => a.Bb.NullableNumber).LessThan(0);
                a.Bb.NullableDouble.ThrowIf(() => a.Bb.NullableDouble).MoreThan(1.5);
                a.Bb.NullableDecimal.ThrowIf(() => a.Bb.NullableDecimal).InBetween(0, 2);
                a.Bb.NullableShort.ThrowIf(() => a.Bb.NullableShort).EqualTo(2);
                a.Bb.NullableBoolean.ThrowIf(() => a.Bb.NullableBoolean).False();
                a.Bb.NullableDateTime.ThrowIf(() => a.Bb.NullableDateTime).AfterThanNow();

                a.Bb.Cc.ThrowIf(() => a.Bb.Cc).Null();
            };

            // Result
            act.ShouldNotThrow<ArgumentException>();
        }

        [TestCase]
        public void ThrowIfNotCombined_NoPreconditions_DoNotThrowArgumentException()
        {
            // Arrange
            var a = new A
            {
                Text = "",
                Number = -1,
                Double = 1.7,
                Decimal = 1,
                Short = 2,
                Boolean = false,
                DateTime = System.DateTime.Now.AddDays(1),
                Numbers = new List<int>(),
                Bb = new B
                {
                    NullableNumber = -1,
                    NullableDouble = 1.7,
                    NullableDecimal = 1,
                    NullableShort = 2,
                    NullableBoolean = false,
                    NullableDateTime = System.DateTime.Now.AddDays(1),
                    Cc = null
                }
            };

            // Act
            Action act = () =>
            {
                a.Text.ThrowIfNot(() => a.Text).Empty();
                a.Number.ThrowIfNot(() => a.Number).LessThan(0);
                a.Double.ThrowIfNot(() => a.Double).MoreThan(1.5);
                a.Decimal.ThrowIfNot(() => a.Decimal).InBetween(0, 2);
                a.Short.ThrowIfNot(() => a.Short).EqualTo((short)2);
                a.Boolean.ThrowIfNot(() => a.Boolean).False();
                a.DateTime.ThrowIfNot(() => a.DateTime).AfterThanNow();
                a.Numbers.ThrowIfNot(() => a.Numbers).Empty();

                a.Bb.NullableNumber.ThrowIfNot(() => a.Bb.NullableNumber).LessThan(0);
                a.Bb.NullableDouble.ThrowIfNot(() => a.Bb.NullableDouble).MoreThan(1.5);
                a.Bb.NullableDecimal.ThrowIfNot(() => a.Bb.NullableDecimal).InBetween(0, 2);
                a.Bb.NullableShort.ThrowIfNot(() => a.Bb.NullableShort).EqualTo(2);
                a.Bb.NullableBoolean.ThrowIfNot(() => a.Bb.NullableBoolean).False();
                a.Bb.NullableDateTime.ThrowIfNot(() => a.Bb.NullableDateTime).AfterThanNow();

                a.Bb.Cc.ThrowIfNot(() => a.Bb.Cc).Null();
            };

            // Result
            act.ShouldNotThrow<ArgumentException>();
        }
    }
}