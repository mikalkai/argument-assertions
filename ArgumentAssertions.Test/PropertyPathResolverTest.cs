﻿using System.Diagnostics.CodeAnalysis;
using FluentAssertions;
using NUnit.Framework;

namespace ArgumentAssertions.Test
{
    [TestFixture]
    [Category("Unit")]
    [ExcludeFromCodeCoverage]
    public class PropertyPathResolverTest
    {
        public class Parent
        {
            public string Text { get; set; }

            public int Number { get; set; }

            public Child Child { get; set; }
        }

        public class Child
        {
            public bool Flag { get; set; }
        }

        [TestCase]
        public void GetPropertyPath_ExpressionIsParent_ReturnPropertyPath()
        {
            // Arrange
            var parent = new Parent();

            // Act
            var propertyPath = PropertyPathResolver.ResolvePropertyPath(() => parent);

            // Result
            propertyPath.Should().Be("parent");
        }

        [TestCase]
        public void GetPropertyPath_ExpressionIsParentText_ReturnPropertyPath()
        {
            // Arrange
            var parent = new Parent();

            // Act
            var propertyPath = PropertyPathResolver.ResolvePropertyPath(() => parent.Text);

            // Result
            propertyPath.Should().Be("parent.Text");
        }

        [TestCase]
        public void GetPropertyPath_ExpressionIsParentNumber_ReturnPropertyPath()
        {
            // Arrange
            var parent = new Parent();

            // Act
            var propertyPath = PropertyPathResolver.ResolvePropertyPath(() => parent.Number);

            // Result
            propertyPath.Should().Be("parent.Number");
        }

        [TestCase]
        public void GetPropertyPath_ExpressionIsParentChildFlag_ReturnPropertyPath()
        {
            // Arrange
            var parent = new Parent();

            // Act
            var propertyPath = PropertyPathResolver.ResolvePropertyPath(() => parent.Child.Flag);

            // Result
            propertyPath.Should().Be("parent.Child.Flag");
        }

        [TestCase]
        public void GetPropertyPath_ExpressionIsObject_ReturnPropertyPath()
        {
            // Arrange
            var obj = new object();

            // Act
            var propertyPath = PropertyPathResolver.ResolvePropertyPath(() => obj);

            // Result
            propertyPath.Should().Be("obj");
        }

        [TestCase]
        public void GetPropertyPath_ExpressionIsValueType_ReturnPropertyPath()
        {
            // Arrange
            var number = 13;

            // Act
            var propertyPath = PropertyPathResolver.ResolvePropertyPath(() => number);

            // Result
            propertyPath.Should().Be("number");
        }

        [TestCase]
        public void GetPropertyPath_ExpressionIsNull_ReturnUndefined()
        {
            // Arrange

            // Act
            var propertyPath = PropertyPathResolver.ResolvePropertyPath<string>(null);

            // Result
            propertyPath.Should().Be("undefined");
        }
    }
}