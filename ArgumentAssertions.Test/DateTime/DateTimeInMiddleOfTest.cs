﻿using System;
using System.Diagnostics.CodeAnalysis;
using ArgumentAssertions.DateTime;
using ArgumentAssertions.Generic;
using FluentAssertions;
using NUnit.Framework;

namespace ArgumentAssertions.Test.DateTime
{
    [TestFixture]
    [Category("Unit")]
    [ExcludeFromCodeCoverage]
    public class DateTimeInMiddleOfTest
    {
        #region Test Data

        public class CustomException : Exception {}

        #endregion

        [TestCase]
        public void ThrowIfNotInMiddleOf_DateTimeIsInMiddleOfStartAndEnd_DoNotThrowArgumentException()
        {
            // Arrange
            var dateTime = System.DateTime.Now;
            var start = System.DateTime.Now.AddDays(-1);
            var end = System.DateTime.Now.AddDays(1);

            // Act
            Action act = () => dateTime.ThrowIfNot(() => dateTime).InMiddleOf(start, end);

            // Result
            act.ShouldNotThrow<ArgumentException>();
        }

        [TestCase]
        public void ThrowIfNotInMiddleOf_DateTimeIsNotInMiddleOfStartAndEnd_ThrowArgumentException()
        {
            // Arrange
            var dateTime = System.DateTime.Now.AddDays(-2);
            var start = System.DateTime.Now.AddDays(-1);
            var end = System.DateTime.Now.AddDays(1);

            // Act
            Action act = () => dateTime.ThrowIfNot(() => dateTime).InMiddleOf(start, end);

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(exception => exception.Message.Contains($"Parameter 'dateTime' should be in middle of '{start}' and '{end}'."));
        }

        [TestCase]
        public void ThrowIfNotInMiddleOf_DateTimeIsNotInMiddleOfStartAndEnd_ThrowCustomException()
        {
            // Arrange
            var dateTime = System.DateTime.Now.AddDays(-2);
            var start = System.DateTime.Now.AddDays(-1);
            var end = System.DateTime.Now.AddDays(1);

            // Act
            Action act = () => dateTime.ThrowIfNot(() => new CustomException()).InMiddleOf(start, end);

            // Result
            act.ShouldThrow<CustomException>();
        }

        [TestCase]
        public void ThrowIfNotInMiddleOf_NullableDateTimeIsInMiddleOfStartAndEnd_DoNotThrowArgumentException()
        {
            // Arrange
            System.DateTime? dateTime = System.DateTime.Now;
            var start = System.DateTime.Now.AddDays(-1);
            var end = System.DateTime.Now.AddDays(1);

            // Act
            Action act = () => dateTime.ThrowIfNot(() => dateTime).InMiddleOf(start, end);

            // Result
            act.ShouldNotThrow<ArgumentException>();
        }

        [TestCase]
        public void ThrowIfNotInMiddleOf_NullableDateTimeIsNotInMiddleOfStartAndEnd_ThrowArgumentException()
        {
            // Arrange
            System.DateTime? dateTime = System.DateTime.Now.AddDays(-2);
            System.DateTime? start = System.DateTime.Now.AddDays(-1);
            var end = System.DateTime.Now.AddDays(1);

            // Act
            Action act = () => dateTime.ThrowIfNot(() => dateTime).InMiddleOf(start, end);

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(exception => exception.Message.Contains($"Parameter 'dateTime' should be in middle of '{start}' and '{end}'."));
        }

        [TestCase]
        [SuppressMessage("ReSharper", "ExpressionIsAlwaysNull")]
        public void ThrowIfNotInMiddleOf_NullableDateTimeIsNull_ThrowArgumentNullException()
        {
            // Arrange
            System.DateTime? dateTime = null;
            System.DateTime? start = System.DateTime.Now.AddDays(-1);
            var end = System.DateTime.Now.AddDays(1);

            // Act
            Action act = () => dateTime.ThrowIfNot(() => dateTime).InMiddleOf(start, end);

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(exception => exception.Message.Contains($"Parameter 'dateTime' should not be null."));
        }

        [TestCase]
        public void ThrowIfNotInMiddleOf_NullableDateTimeIsNotInMiddleOfStartAndEnd_ThrowCustomException()
        {
            // Arrange
            System.DateTime? dateTime = System.DateTime.Now.AddDays(-2);
            System.DateTime? start = System.DateTime.Now.AddDays(-1);
            System.DateTime? end = System.DateTime.Now.AddDays(1);

            // Act
            Action act = () => dateTime.ThrowIfNot(() => new CustomException()).InMiddleOf(start, end);

            // Result
            act.ShouldThrow<CustomException>();
        }

        [TestCase]
        [SuppressMessage("ReSharper", "ExpressionIsAlwaysNull")]
        public void ThrowIfNotInMiddleOf_NullableDateTimeIsNull_ThrowCustomException()
        {
            // Arrange
            System.DateTime? dateTime = null;
            System.DateTime? start = System.DateTime.Now.AddDays(-1);
            System.DateTime? end = System.DateTime.Now.AddDays(1);

            // Act
            Action act = () => dateTime.ThrowIfNot(() => new CustomException()).InMiddleOf(start, end);

            // Result
            act.ShouldThrow<CustomException>();
        }
    }
}