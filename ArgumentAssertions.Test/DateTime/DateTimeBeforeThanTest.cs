﻿using System;
using System.Diagnostics.CodeAnalysis;
using ArgumentAssertions.DateTime;
using ArgumentAssertions.Generic;
using FluentAssertions;
using NUnit.Framework;

namespace ArgumentAssertions.Test.DateTime
{
    [TestFixture]
    [Category("Unit")]
    [ExcludeFromCodeCoverage]
    public class DateTimeBeforeThanTest
    {
        #region Test Data

        public class CustomException : Exception {}

        #endregion

        [TestCase]
        public void ThrowIfBeforeThan_DateTimeIsNotBeforeThanOther_DoNotThrowArgumentException()
        {
            // Arrange
            var dateTime = System.DateTime.Now.AddDays(1);
            var other = System.DateTime.Now;

            // Act
            Action act = () => dateTime.ThrowIf(() => dateTime).BeforeThan(other);

            // Result
            act.ShouldNotThrow<ArgumentException>();
        }

        [TestCase]
        public void ThrowIfBeforeThan_DateTimeIsBeforeThanOther_ThrowArgumentException()
        {
            // Arrange
            var dateTime = System.DateTime.Now.AddDays(-1);
            var other = System.DateTime.Now;

            // Act
            Action act = () => dateTime.ThrowIf(() => dateTime).BeforeThan(other);

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(exception => exception.Message.Contains($"Parameter 'dateTime' should not be before than '{other}'."));
        }

        [TestCase]
        public void ThrowIfBeforeThan_DateTimeIsBeforeThanOther_ThrowCustomException()
        {
            // Arrange
            var dateTime = System.DateTime.Now.AddDays(-1);
            var other = System.DateTime.Now;

            // Act
            Action act = () => dateTime.ThrowIf(() => new CustomException()).BeforeThan(other);

            // Result
            act.ShouldThrow<CustomException>();
        }

        [TestCase]
        public void ThrowIfBeforeThan_NullableDateTimeIsNotBeforeThanOther_DoNotThrowArgumentException()
        {
            // Arrange
            System.DateTime? dateTime = System.DateTime.Now.AddDays(1);
            var other = System.DateTime.Now;

            // Act
            Action act = () => dateTime.ThrowIf(() => dateTime).BeforeThan(other);

            // Result
            act.ShouldNotThrow<ArgumentException>();
        }

        [TestCase]
        public void ThrowIfBeforeThan_NullableDateTimeIsBeforeThanOther_ThrowArgumentException()
        {
            // Arrange
            System.DateTime? dateTime = System.DateTime.Now.AddDays(-1);
            var other = System.DateTime.Now;

            // Act
            Action act = () => dateTime.ThrowIf(() => dateTime).BeforeThan(other);

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(exception => exception.Message.Contains($"Parameter 'dateTime' should not be before than '{other}'."));
        }

        [TestCase]
        [SuppressMessage("ReSharper", "ExpressionIsAlwaysNull")]
        public void ThrowIfBeforeThan_NullableDateTimeIsNull_ThrowArgumentNullException()
        {
            // Arrange
            System.DateTime? dateTime = null;
            var other = System.DateTime.Now;

            // Act
            Action act = () => dateTime.ThrowIf(() => dateTime).BeforeThan(other);

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(exception => exception.Message.Contains($"Parameter 'dateTime' should not be null."));
        }

        [TestCase]
        public void ThrowIfBeforeThan_NullableDateTimeIsBeforeThanOther_ThrowCustomException()
        {
            // Arrange
            System.DateTime? dateTime = System.DateTime.Now.AddDays(-1);
            var other = System.DateTime.Now;

            // Act
            Action act = () => dateTime.ThrowIf(() => new CustomException()).BeforeThan(other);

            // Result
            act.ShouldThrow<CustomException>();
        }

        [TestCase]
        [SuppressMessage("ReSharper", "ExpressionIsAlwaysNull")]
        public void ThrowIfBeforeThan_NullableDateTimeIsNull_ThrowCustomException()
        {
            // Arrange
            System.DateTime? dateTime = null;
            var other = System.DateTime.Now;

            // Act
            Action act = () => dateTime.ThrowIf(() => new CustomException()).BeforeThan(other);

            // Result
            act.ShouldThrow<CustomException>();
        }
    }
}