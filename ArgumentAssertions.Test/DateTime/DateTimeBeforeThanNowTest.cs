﻿using System;
using System.Diagnostics.CodeAnalysis;
using ArgumentAssertions.DateTime;
using ArgumentAssertions.Generic;
using FluentAssertions;
using NUnit.Framework;

namespace ArgumentAssertions.Test.DateTime
{
    [TestFixture]
    [Category("Unit")]
    [ExcludeFromCodeCoverage]
    public class DateTimeBeforeThanNowTest
    {
        #region Test Data

        public class CustomException : Exception {}

        #endregion

        [TestCase]
        public void ThrowIfBeforeThanNow_DateTimeIsNotBeforeThanNow_DoNotThrowArgumentException()
        {
            // Arrange
            var dateTime = System.DateTime.Now.AddDays(1);

            // Act
            Action act = () => dateTime.ThrowIf(() => dateTime).BeforeThanNow();

            // Result
            act.ShouldNotThrow<ArgumentException>();
        }

        [TestCase]
        public void ThrowIfBeforeThanNow_DateTimeIsBeforeThanNow_ThrowArgumentException()
        {
            // Arrange
            var dateTime = System.DateTime.Now.AddDays(-1);

            // Act
            Action act = () => dateTime.ThrowIf(() => dateTime).BeforeThanNow();

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(exception => exception.Message.Contains($"Parameter 'dateTime' should not be before than '{System.DateTime.Now}'."));
        }

        [TestCase]
        public void ThrowIfBeforeThanNow_DateTimeIsBeforeThanNow_ThrowCustomException()
        {
            // Arrange
            var dateTime = System.DateTime.Now.AddDays(-1);

            // Act
            Action act = () => dateTime.ThrowIf(() => new CustomException()).BeforeThanNow();

            // Result
            act.ShouldThrow<CustomException>();
        }

        [TestCase]
        public void ThrowIfBeforeThanNow_NullableDateTimeIsNotBeforeThanNow_DoNotThrowArgumentException()
        {
            // Arrange
            System.DateTime? dateTime = System.DateTime.Now.AddDays(1);

            // Act
            Action act = () => dateTime.ThrowIf(() => dateTime).BeforeThanNow();

            // Result
            act.ShouldNotThrow<ArgumentException>();
        }

        [TestCase]
        public void ThrowIfBeforeThanNow_NullableDateTimeIsBeforeThanNow_ThrowArgumentException()
        {
            // Arrange
            System.DateTime? dateTime = System.DateTime.Now.AddDays(-1);

            // Act
            Action act = () => dateTime.ThrowIf(() => dateTime).BeforeThanNow();

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(exception => exception.Message.Contains($"Parameter 'dateTime' should not be before than '{System.DateTime.Now}'."));
        }

        [TestCase]
        [SuppressMessage("ReSharper", "ExpressionIsAlwaysNull")]
        public void ThrowIfBeforeThanNow_NullableDateTimeIsNull_ThrowArgumentNullException()
        {
            // Arrange
            System.DateTime? dateTime = null;

            // Act
            Action act = () => dateTime.ThrowIf(() => dateTime).BeforeThanNow();

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(exception => exception.Message.Contains($"Parameter 'dateTime' should not be null."));
        }

        [TestCase]
        public void ThrowIfBeforeThanNow_NullableDateTimeIsBeforeThanNow_ThrowCustomException()
        {
            // Arrange
            System.DateTime? dateTime = System.DateTime.Now.AddDays(-1);

            // Act
            Action act = () => dateTime.ThrowIf(() => new CustomException()).BeforeThanNow();

            // Result
            act.ShouldThrow<CustomException>();
        }

        [TestCase]
        [SuppressMessage("ReSharper", "ExpressionIsAlwaysNull")]
        public void ThrowIfBeforeThanNow_NullableDateTimeIsNull_ThrowCustomException()
        {
            // Arrange
            System.DateTime? dateTime = null;

            // Act
            Action act = () => dateTime.ThrowIf(() => new CustomException()).BeforeThanNow();

            // Result
            act.ShouldThrow<CustomException>();
        }
    }
}