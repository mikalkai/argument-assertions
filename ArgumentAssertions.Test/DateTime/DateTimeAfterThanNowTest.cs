﻿using System;
using System.Diagnostics.CodeAnalysis;
using ArgumentAssertions.DateTime;
using ArgumentAssertions.Generic;
using FluentAssertions;
using NUnit.Framework;

namespace ArgumentAssertions.Test.DateTime
{
    [TestFixture]
    [Category("Unit")]
    [ExcludeFromCodeCoverage]
    public class DateTimeAfterThanNowTest
    {
        #region Test Data

        public class CustomException : Exception {}

        #endregion

        [TestCase]
        public void ThrowIfAfterThanNow_DateTimeIsNotAfterThanNow_DoNotThrowArgumentException()
        {
            // Arrange
            var dateTime = System.DateTime.Now.AddDays(-1);

            // Act
            Action act = () => dateTime.ThrowIf(() => dateTime).AfterThanNow();

            // Result
            act.ShouldNotThrow<ArgumentException>();
        }

        [TestCase]
        public void ThrowIfAfterThanNow_DateTimeIsAfterThanNow_ThrowArgumentException()
        {
            // Arrange
            var dateTime = System.DateTime.Now.AddDays(1);

            // Act
            Action act = () => dateTime.ThrowIf(() => dateTime).AfterThanNow();

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(exception => exception.Message.Contains($"Parameter 'dateTime' should not be after than '{System.DateTime.Now}'."));
        }

        [TestCase]
        public void ThrowIfAfterThanNow_DateTimeIsAfterThanNow_ThrowCustomException()
        {
            // Arrange
            var dateTime = System.DateTime.Now.AddDays(1);

            // Act
            Action act = () => dateTime.ThrowIf(() => new CustomException()).AfterThanNow();

            // Result
            act.ShouldThrow<CustomException>();
        }

        [TestCase]
        public void ThrowIfAfterThanNow_NullableDateTimeIsNotAfterThanNow_DoNotThrowArgumentException()
        {
            // Arrange
            System.DateTime? dateTime = System.DateTime.Now.AddDays(-1);

            // Act
            Action act = () => dateTime.ThrowIf(() => dateTime).AfterThanNow();

            // Result
            act.ShouldNotThrow<ArgumentException>();
        }

        [TestCase]
        public void ThrowIfAfterThanNow_NullableDateTimeIsAfterThanNow_ThrowArgumentException()
        {
            // Arrange
            System.DateTime? dateTime = System.DateTime.Now.AddDays(1);

            // Act
            Action act = () => dateTime.ThrowIf(() => dateTime).AfterThanNow();

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(exception => exception.Message.Contains($"Parameter 'dateTime' should not be after than '{System.DateTime.Now}'."));
        }

        [TestCase]
        [SuppressMessage("ReSharper", "ExpressionIsAlwaysNull")]
        public void ThrowIfAfterThanNow_NullableDateTimeIsNull_ThrowArgumentNullException()
        {
            // Arrange
            System.DateTime? dateTime = null;

            // Act
            Action act = () => dateTime.ThrowIf(() => dateTime).AfterThanNow();

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(exception => exception.Message.Contains($"Parameter 'dateTime' should not be null."));
        }

        [TestCase]
        public void ThrowIfAfterThanNow_NullableDateTimeIsAfterThanNow_ThrowCustomException()
        {
            // Arrange
            System.DateTime? dateTime = System.DateTime.Now.AddDays(1);

            // Act
            Action act = () => dateTime.ThrowIf(() => new CustomException()).AfterThanNow();

            // Result
            act.ShouldThrow<CustomException>();
        }

        [TestCase]
        [SuppressMessage("ReSharper", "ExpressionIsAlwaysNull")]
        public void ThrowIfAfterThanNow_NullableDateTimeIsNull_ThrowCustomException()
        {
            // Arrange
            System.DateTime? dateTime = null;

            // Act
            Action act = () => dateTime.ThrowIf(() => new CustomException()).AfterThanNow();

            // Result
            act.ShouldThrow<CustomException>();
        }
    }
}