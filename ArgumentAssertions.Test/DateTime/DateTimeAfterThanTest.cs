﻿using System;
using System.Diagnostics.CodeAnalysis;
using ArgumentAssertions.DateTime;
using ArgumentAssertions.Generic;
using FluentAssertions;
using NUnit.Framework;

namespace ArgumentAssertions.Test.DateTime
{
    [TestFixture]
    [Category("Unit")]
    [ExcludeFromCodeCoverage]
    public class DateTimeAfterThanTest
    {
        #region Test Data

        public class CustomException : Exception {}

        #endregion

        [TestCase]
        public void ThrowIfAfterThan_DateTimeIsNotAfterThanOther_DoNotThrowArgumentException()
        {
            // Arrange
            var dateTime = System.DateTime.Now.AddDays(-1);
            var other = System.DateTime.Now;

            // Act
            Action act = () => dateTime.ThrowIf(() => dateTime).AfterThan(other);

            // Result
            act.ShouldNotThrow<ArgumentException>();
        }

        [TestCase]
        public void ThrowIfAfterThan_DateTimeIsAfterThanOther_ThrowArgumentException()
        {
            // Arrange
            var dateTime = System.DateTime.Now.AddDays(1);
            var other = System.DateTime.Now;

            // Act
            Action act = () => dateTime.ThrowIf(() => dateTime).AfterThan(other);

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(exception => exception.Message.Contains($"Parameter 'dateTime' should not be after than '{other}'."));
        }

        [TestCase]
        public void ThrowIfAfterThan_DateTimeIsAfterThanOther_ThrowCustomException()
        {
            // Arrange
            var dateTime = System.DateTime.Now.AddDays(1);
            var other = System.DateTime.Now;

            // Act
            Action act = () => dateTime.ThrowIf(() => new CustomException()).AfterThan(other);

            // Result
            act.ShouldThrow<CustomException>();
        }

        [TestCase]
        public void ThrowIfAfterThan_NullableDateTimeIsNotAfterThanOther_DoNotThrowArgumentException()
        {
            // Arrange
            System.DateTime? dateTime = System.DateTime.Now.AddDays(-1);
            var other = System.DateTime.Now;

            // Act
            Action act = () => dateTime.ThrowIf(() => dateTime).AfterThan(other);

            // Result
            act.ShouldNotThrow<ArgumentException>();
        }

        [TestCase]
        public void ThrowIfAfterThan_NullableDateTimeIsAfterThanOther_ThrowArgumentException()
        {
            // Arrange
            System.DateTime? dateTime = System.DateTime.Now.AddDays(1);
            var other = System.DateTime.Now;

            // Act
            Action act = () => dateTime.ThrowIf(() => dateTime).AfterThan(other);

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(exception => exception.Message.Contains($"Parameter 'dateTime' should not be after than '{other}'."));
        }

        [TestCase]
        [SuppressMessage("ReSharper", "ExpressionIsAlwaysNull")]
        public void ThrowIfAfterThan_NullableDateTimeIsNull_ThrowArgumentNullException()
        {
            // Arrange
            System.DateTime? dateTime = null;
            var other = System.DateTime.Now;

            // Act
            Action act = () => dateTime.ThrowIf(() => dateTime).AfterThan(other);

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(exception => exception.Message.Contains($"Parameter 'dateTime' should not be null."));
        }

        [TestCase]
        public void ThrowIfAfterThan_NullableDateTimeIsAfterThanOther_ThrowCustomException()
        {
            // Arrange
            System.DateTime? dateTime = System.DateTime.Now.AddDays(1);
            var other = System.DateTime.Now;

            // Act
            Action act = () => dateTime.ThrowIf(() => new CustomException()).AfterThan(other);

            // Result
            act.ShouldThrow<CustomException>();
        }

        [TestCase]
        [SuppressMessage("ReSharper", "ExpressionIsAlwaysNull")]
        public void ThrowIfAfterThan_NullableDateTimeIsNull_ThrowCustomException()
        {
            // Arrange
            System.DateTime? dateTime = null;
            var other = System.DateTime.Now;

            // Act
            Action act = () => dateTime.ThrowIf(() => new CustomException()).AfterThan(other);

            // Result
            act.ShouldThrow<CustomException>();
        }
    }
}