﻿using System;
using System.Diagnostics.CodeAnalysis;
using ArgumentAssertions.DateTime;
using ArgumentAssertions.Generic;
using FluentAssertions;
using NUnit.Framework;

namespace ArgumentAssertions.Test.DateTime
{
    [TestFixture]
    [Category("Unit")]
    [ExcludeFromCodeCoverage]
    public class DateTimeAfterOrEqualThanNowTest
    {
        #region Test Data

        public class CustomException : Exception { }

        #endregion

        [TestCase]
        public void ThrowIfAfterOrEqualThanNow_DateTimeIsAfterThanNow_DoNotThrowArgumentException()
        {
            // Arrange
            var dateTime = System.DateTime.Now.AddDays(-1);

            // Act
            Action act = () => dateTime.ThrowIf(() => dateTime).AfterOrEqualThanNow();

            // Result
            act.ShouldNotThrow<ArgumentException>();
        }

        [TestCase]
        public void ThrowIfAfterOrEqualThanNow_DateTimeIsBeforeThanNow_ThrowArgumentException()
        {
            // Arrange
            var dateTime = System.DateTime.Now.AddDays(1);

            // Act
            Action act = () => dateTime.ThrowIf(() => dateTime).AfterOrEqualThanNow();

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(exception => exception.Message.Contains($"Parameter 'dateTime' should not be after or equal than '{System.DateTime.Now}'."));
        }

        [TestCase]
        public void ThrowIfAfterOrEqualThanNow_DateTimeIsEqualThanNow_ThrowArgumentException()
        {
            // Arrange
            var dateTime = System.DateTime.Now.AddDays(1);

            // Act
            Action act = () => dateTime.ThrowIf(() => dateTime).AfterOrEqualThanNow();

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(exception => exception.Message.Contains($"Parameter 'dateTime' should not be after or equal than '{System.DateTime.Now}'."));
        }


        [TestCase]
        public void ThrowIfAfterOrEqualThanNow_DateTimeIsBeforeThanNow_ThrowCustomException()
        {
            // Arrange
            var dateTime = System.DateTime.Now.AddDays(1);

            // Act
            Action act = () => dateTime.ThrowIf(() => new CustomException()).AfterOrEqualThanNow();

            // Result
            act.ShouldThrow<CustomException>();
        }

        [TestCase]
        public void ThrowIfAfterOrEqualThanNow_DateTimeIsEqualThanNow_ThrowCustomException()
        {
            // Arrange
            var dateTime = System.DateTime.Now.AddDays(1);

            // Act
            Action act = () => dateTime.ThrowIf(() => new CustomException()).AfterOrEqualThanNow();

            // Result
            act.ShouldThrow<CustomException>();
        }

        [TestCase]
        public void ThrowIfAfterOrEqualThanNow_NullableDateTimeIsAfterThanNow_DoNotThrowArgumentException()
        {
            // Arrange
            System.DateTime? dateTime = System.DateTime.Now.AddDays(-1);

            // Act
            Action act = () => dateTime.ThrowIf(() => dateTime).AfterOrEqualThanNow();

            // Result
            act.ShouldNotThrow<ArgumentException>();
        }

        [TestCase]
        public void ThrowIfAfterOrEqualThanNow_NullableDateTimeIsBeforeThanNow_ThrowArgumentException()
        {
            // Arrange
            System.DateTime? dateTime = System.DateTime.Now.AddDays(1);

            // Act
            Action act = () => dateTime.ThrowIf(() => dateTime).AfterOrEqualThanNow();

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(exception => exception.Message.Contains($"Parameter 'dateTime' should not be after or equal than '{System.DateTime.Now}'."));
        }

        [TestCase]
        public void ThrowIfAfterOrEqualThanNow_NullableDateTimeIsEqualThanNow_ThrowArgumentException()
        {
            // Arrange
            System.DateTime? dateTime = System.DateTime.Now.AddDays(1);

            // Act
            Action act = () => dateTime.ThrowIf(() => dateTime).AfterOrEqualThanNow();

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(exception => exception.Message.Contains($"Parameter 'dateTime' should not be after or equal than '{System.DateTime.Now}'."));
        }

        [TestCase]
        [SuppressMessage("ReSharper", "ExpressionIsAlwaysNull")]
        public void ThrowIfAfterOrEqualThanNow_NullableDateTimeIsNull_ThrowArgumentNullException()
        {
            // Arrange
            System.DateTime? dateTime = null;

            // Act
            Action act = () => dateTime.ThrowIf(() => dateTime).AfterOrEqualThanNow();

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(exception => exception.Message.Contains($"Parameter 'dateTime' should not be null."));
        }

        [TestCase]
        public void ThrowIfAfterOrEqualThanNow_NullableDateTimeIsBeforeThanNow_ThrowCustomException()
        {
            // Arrange
            System.DateTime? dateTime = System.DateTime.Now.AddDays(1);

            // Act
            Action act = () => dateTime.ThrowIf(() => new CustomException()).AfterOrEqualThanNow();

            // Result
            act.ShouldThrow<CustomException>();
        }

        [TestCase]
        public void ThrowIfAfterOrEqualThanNow_NullableDateTimeIsEqualThanNow_ThrowCustomException()
        {
            // Arrange
            System.DateTime? dateTime = System.DateTime.Now.AddDays(1);

            // Act
            Action act = () => dateTime.ThrowIf(() => new CustomException()).AfterOrEqualThanNow();

            // Result
            act.ShouldThrow<CustomException>();
        }

        [TestCase]
        [SuppressMessage("ReSharper", "ExpressionIsAlwaysNull")]
        public void ThrowIfAfterOrEqualThanNow_NullableDateTimeIsNull_ThrowCustomException()
        {
            // Arrange
            System.DateTime? dateTime = null;

            // Act
            Action act = () => dateTime.ThrowIf(() => new CustomException()).AfterOrEqualThanNow();

            // Result
            act.ShouldThrow<CustomException>();
        }
    }
}