﻿using System;
using System.Diagnostics.CodeAnalysis;
using ArgumentAssertions.DateTime;
using ArgumentAssertions.Generic;
using FluentAssertions;
using NUnit.Framework;

namespace ArgumentAssertions.Test.DateTime
{
    [TestFixture]
    [Category("Unit")]
    [ExcludeFromCodeCoverage]
    public class DateTimeAfterOrEqualThanTest
    {
        #region Test Data

        public class CustomException : Exception { }

        #endregion

        [TestCase]
        public void ThrowIfAfterOrEqualThan_DateTimeIsAfterThanOther_DoNotThrowArgumentException()
        {
            // Arrange
            var dateTime = System.DateTime.Now.AddDays(-1);
            var other = System.DateTime.Now;

            // Act
            Action act = () => dateTime.ThrowIf(() => dateTime).AfterOrEqualThan(other);

            // Result
            act.ShouldNotThrow<ArgumentException>();
        }

        [TestCase]
        public void ThrowIfAfterOrEqualThan_DateTimeIsBeforeThanOther_ThrowArgumentException()
        {
            // Arrange
            var dateTime = System.DateTime.Now.AddDays(1);
            var other = System.DateTime.Now;

            // Act
            Action act = () => dateTime.ThrowIf(() => dateTime).AfterOrEqualThan(other);

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(exception => exception.Message.Contains($"Parameter 'dateTime' should not be after or equal than '{other}'."));
        }

        [TestCase]
        public void ThrowIfAfterOrEqualThan_DateTimeIsEqualThanOther_ThrowArgumentException()
        {
            // Arrange
            var dateTime = System.DateTime.Now.AddDays(1);
            var other = dateTime;

            // Act
            Action act = () => dateTime.ThrowIf(() => dateTime).AfterOrEqualThan(other);

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(exception => exception.Message.Contains($"Parameter 'dateTime' should not be after or equal than '{other}'."));
        }


        [TestCase]
        public void ThrowIfAfterOrEqualThan_DateTimeIsBeforeThanOther_ThrowCustomException()
        {
            // Arrange
            var dateTime = System.DateTime.Now.AddDays(1);
            var other = System.DateTime.Now;

            // Act
            Action act = () => dateTime.ThrowIf(() => new CustomException()).AfterOrEqualThan(other);

            // Result
            act.ShouldThrow<CustomException>();
        }

        [TestCase]
        public void ThrowIfAfterOrEqualThan_DateTimeIsEqualThanOther_ThrowCustomException()
        {
            // Arrange
            var dateTime = System.DateTime.Now.AddDays(1);
            var other = dateTime;

            // Act
            Action act = () => dateTime.ThrowIf(() => new CustomException()).AfterOrEqualThan(other);

            // Result
            act.ShouldThrow<CustomException>();
        }

        [TestCase]
        public void ThrowIfAfterOrEqualThan_NullableDateTimeIsAfterThanOther_DoNotThrowArgumentException()
        {
            // Arrange
            System.DateTime? dateTime = System.DateTime.Now.AddDays(-1);
            var other = System.DateTime.Now;

            // Act
            Action act = () => dateTime.ThrowIf(() => dateTime).AfterOrEqualThan(other);

            // Result
            act.ShouldNotThrow<ArgumentException>();
        }

        [TestCase]
        public void ThrowIfAfterOrEqualThan_NullableDateTimeIsBeforeThanOther_ThrowArgumentException()
        {
            // Arrange
            System.DateTime? dateTime = System.DateTime.Now.AddDays(1);
            var other = System.DateTime.Now;

            // Act
            Action act = () => dateTime.ThrowIf(() => dateTime).AfterOrEqualThan(other);

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(exception => exception.Message.Contains($"Parameter 'dateTime' should not be after or equal than '{other}'."));
        }

        [TestCase]
        public void ThrowIfAfterOrEqualThan_NullableDateTimeIsEqualThanOther_ThrowArgumentException()
        {
            // Arrange
            System.DateTime? dateTime = System.DateTime.Now.AddDays(1);
            var other = dateTime;

            // Act
            Action act = () => dateTime.ThrowIf(() => dateTime).AfterOrEqualThan(other);

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(exception => exception.Message.Contains($"Parameter 'dateTime' should not be after or equal than '{other}'."));
        }

        [TestCase]
        [SuppressMessage("ReSharper", "ExpressionIsAlwaysNull")]
        public void ThrowIfAfterOrEqualThan_NullableDateTimeIsNull_ThrowArgumentNullException()
        {
            // Arrange
            System.DateTime? dateTime = null;
            var other = dateTime;

            // Act
            Action act = () => dateTime.ThrowIf(() => dateTime).AfterOrEqualThan(other);

            // Result
            act.ShouldThrow<ArgumentException>()
                .Where(exception => exception.Message.Contains($"Parameter 'dateTime' should not be null."));
        }

        [TestCase]
        public void ThrowIfAfterOrEqualThan_NullableDateTimeIsBeforeThanOther_ThrowCustomException()
        {
            // Arrange
            System.DateTime? dateTime = System.DateTime.Now.AddDays(1);
            var other = System.DateTime.Now;

            // Act
            Action act = () => dateTime.ThrowIf(() => new CustomException()).AfterOrEqualThan(other);

            // Result
            act.ShouldThrow<CustomException>();
        }

        [TestCase]
        public void ThrowIfAfterOrEqualThan_NullableDateTimeIsEqualThanOther_ThrowCustomException()
        {
            // Arrange
            System.DateTime? dateTime = System.DateTime.Now.AddDays(1);
            var other = dateTime;

            // Act
            Action act = () => dateTime.ThrowIf(() => new CustomException()).AfterOrEqualThan(other);

            // Result
            act.ShouldThrow<CustomException>();
        }

        [TestCase]
        [SuppressMessage("ReSharper", "ExpressionIsAlwaysNull")]
        public void ThrowIfAfterOrEqualThan_NullableDateTimeIsNull_ThrowCustomException()
        {
            // Arrange
            System.DateTime? dateTime = null;
            var other = dateTime;

            // Act
            Action act = () => dateTime.ThrowIf(() => new CustomException()).AfterOrEqualThan(other);

            // Result
            act.ShouldThrow<CustomException>();
        }
    }
}