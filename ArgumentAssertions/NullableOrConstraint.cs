﻿using ArgumentAssertions.Generic.Internal;

namespace ArgumentAssertions
{
    public class NullableOrConstraint<T>
        where T : struct
    {
        private readonly INullableAssertion<T> _assertion;
        private readonly AssertionContext _context;

        public NullableOrConstraint(INullableAssertion<T> assertion)
        {
            _assertion = assertion;
            _context = assertion.Context();
        }

        public INullableAssertion<T> OrIf => _context.ParameterName != null
            ? new NullableIfAssertion<T>(_assertion.Value, _context.ParameterName)
            : new NullableIfAssertion<T>(_assertion.Value, _context.ExceptionExpression);

        public INullableAssertion<T> OrIfNot => _context.ParameterName != null
            ? new NullableIfNotAssertion<T>(_assertion.Value, _context.ParameterName)
            : new NullableIfNotAssertion<T>(_assertion.Value, _context.ExceptionExpression);


        public static implicit operator T? (NullableOrConstraint<T> constraint)
        {
            return constraint._assertion.Value;
        }
    }
}