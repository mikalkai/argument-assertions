﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace ArgumentAssertions
{
    public class NullableIfNotAssertion<T> : NullableAssertion<T>
        where T : struct
    {
        public NullableIfNotAssertion(T? value, string parameterName)
            : base(value, Condition.IfNot, parameterName) {}

        public NullableIfNotAssertion(T? value, Expression<Func<Exception>> exceptionExpression)
            : base(value, Condition.IfNot, exceptionExpression) {}

        public override T? Assert(Func<bool> evaluate, Expression<Func<Exception>> exceptionExpr)
        {
            return Assert(evaluate(), exceptionExpr);
        }

        public override T? Assert(bool result, Expression<Func<Exception>> exceptionExpr)
        {
            if (result || IgnoreFuncs.Any(func => func()))
            {
                return Value;
            }

            throw CompileException(exceptionExpr);
        }
    }
}