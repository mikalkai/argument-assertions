﻿using System;

namespace ArgumentAssertions
{
    public class NullExceptionReferenceException : Exception
    {
        public NullExceptionReferenceException()
         : base("Compiled expression returned a null reference.") {}
    }
}