﻿using System;

namespace ArgumentAssertions.DateTime
{
    public static class DateTimeExceptionMessages
    {
        /// <summary>
        /// Format message for date time before than.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="dateTime">Compared date time.</param>
        /// <returns>Exception message.</returns>
        public static string BeforeThanMessage(this AssertionContext context, System.DateTime? dateTime)
        {
            var condition = context.Condition;
            var paramName = context.ParameterName;
            switch (condition)
            {
                case Condition.If:
                    return $"Parameter '{paramName}' should not be before than '{dateTime}'.";
                case Condition.IfNot:
                    return $"Parameter '{paramName}' should be before than '{dateTime}'.";
                default:
                    throw new ArgumentOutOfRangeException(nameof(condition), condition, null);
            }
        }

        /// <summary>
        /// Format message for date time before or equal than.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="dateTime">Compared date time.</param>
        /// <returns>Exception message.</returns>
        public static string BeforeOrEqualThanMessage(this AssertionContext context, System.DateTime? dateTime)
        {
            var condition = context.Condition;
            var paramName = context.ParameterName;
            switch (condition)
            {
                case Condition.If:
                    return $"Parameter '{paramName}' should not be before or equal than '{dateTime}'.";
                case Condition.IfNot:
                    return $"Parameter '{paramName}' should be before or equal than '{dateTime}'.";
                default:
                    throw new ArgumentOutOfRangeException(nameof(condition), condition, null);
            }
        }

        /// <summary>
        /// Format message for date time after than.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="dateTime">Compared date time.</param>
        /// <returns>Exception message.</returns>
        public static string AfterThanMessage(this AssertionContext context, System.DateTime? dateTime)
        {
            var condition = context.Condition;
            var paramName = context.ParameterName;
            switch (condition)
            {
                case Condition.If:
                    return $"Parameter '{paramName}' should not be after than '{dateTime}'.";
                case Condition.IfNot:
                    return $"Parameter '{paramName}' should be after than '{dateTime}'.";
                default:
                    throw new ArgumentOutOfRangeException(nameof(condition), condition, null);
            }
        }

        /// <summary>
        /// Format message for date time after or equal than.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="dateTime">Compared date time.</param>
        /// <returns>Exception message.</returns>
        public static string AfterOrEqualThanMessage(this AssertionContext context, System.DateTime? dateTime)
        {
            var condition = context.Condition;
            var paramName = context.ParameterName;
            switch (condition)
            {
                case Condition.If:
                    return $"Parameter '{paramName}' should not be after or equal than '{dateTime}'.";
                case Condition.IfNot:
                    return $"Parameter '{paramName}' should be after or equal than '{dateTime}'.";
                default:
                    throw new ArgumentOutOfRangeException(nameof(condition), condition, null);
            }
        }

        /// <summary>
        /// Format message for date time in a time window.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="start">Start time.</param>
        /// <param name="end">End time</param>
        /// <returns>Exception message.</returns>
        public static string InMiddleOfMessage(this AssertionContext context, System.DateTime? start, System.DateTime? end)
        {
            var condition = context.Condition;
            var paramName = context.ParameterName;
            switch (condition)
            {
                case Condition.If:
                    return $"Parameter '{paramName}' should not be in middle of '{start}' and '{end}'.";
                case Condition.IfNot:
                    return $"Parameter '{paramName}' should be in middle of '{start}' and '{end}'.";
                default:
                    throw new ArgumentOutOfRangeException(nameof(condition), condition, null);
            }
        }

    }
}