﻿using System;
using ArgumentAssertions.Generic.Internal;

namespace ArgumentAssertions.DateTime
{
    public static class DateTimeInMiddleOf
    {
        /// <summary>
        /// Throw <see cref="ArgumentException"/> if date time is in middle of a time window.
        /// </summary>
        /// <param name="assertion"></param>
        /// <param name="start">Start time.</param>
        /// <param name="end">End time.</param>
        /// <example>
        /// Below example throws ArgumentException with message "Parameter 'dateTime' should be in middle of '{start}' and '{end}'."
        /// <code>
        /// var dateTime = DateTime.Now;
        /// var start = DateTime.Now.AddDays(1);
        /// var end = DateTime.Now.AddDays(2);
        /// dateTime.ThrowIfNot(() => dateTime).InMiddleOf(start, end);
        /// </code>
        /// </example>
        public static OrConstraint<System.DateTime> InMiddleOf(this IAssertion<System.DateTime> assertion, System.DateTime start, System.DateTime end)
        {
            var context = assertion.Context();

            assertion.Assert(assertion.Value >= start && assertion.Value <= end,
                assertion.ArgumentExceptionExpression(context.InMiddleOfMessage(start, end)));

            return new OrConstraint<System.DateTime>(assertion);
        }

        /// <summary>
        /// Throw <see cref="ArgumentException"/> if date time is in middle of a time window.
        /// </summary>
        /// <param name="assertion"></param>
        /// <param name="start">Start time.</param>
        /// <param name="end">End time.</param>
        /// <example>
        /// Below example throws ArgumentException with message "Parameter 'dateTime' should be in middle of '{start}' and '{end}'."
        /// <code>
        /// DateTime? dateTime = DateTime.Now;
        /// DateTime? start = DateTime.Now.AddDays(1);
        /// DateTime? end = DateTime.Now.AddDays(2);
        /// dateTime.ThrowIfNot(() => dateTime).InMiddleOf(start, end);
        /// </code>
        /// </example>
        public static NullableOrConstraint<System.DateTime> InMiddleOf(this INullableAssertion<System.DateTime> assertion, System.DateTime? start, System.DateTime? end)
        {
            var context = assertion.Context();

            assertion.ThrowIfNull();
            assertion.Assert(assertion.Value >= start && assertion.Value <= end,
                assertion.ArgumentExceptionExpression(context.InMiddleOfMessage(start, end)));

            return new NullableOrConstraint<System.DateTime>(assertion);
        }
    }
}