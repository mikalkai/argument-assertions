﻿using System;
using ArgumentAssertions.Generic;
using ArgumentAssertions.Generic.Internal;

namespace ArgumentAssertions.DateTime
{
    public static class DateTimeAfterThanNow
    {
        /// <summary>
        /// Throw <see cref="ArgumentException"/> if date time is after than now.
        /// </summary>
        /// <param name="assertion"></param>
        /// <example>
        /// Below example throws ArgumentException with message "Parameter 'dateTime' should not be before '{DateTime.Now}'."
        /// <code>
        /// var dateTime = DateTime.Now.AddDays(-1);
        /// dateTime.ThrowIf(() => dateTime).AfterThanNow();
        /// </code>
        /// </example>
        public static OrConstraint<System.DateTime> AfterThanNow(this IAssertion<System.DateTime> assertion)
        {
            return assertion.AfterThan(System.DateTime.Now);
        }

        /// <summary>
        /// Throw <see cref="ArgumentException"/> if date time is after than now.
        /// </summary>
        /// <param name="assertion"></param>
        /// <example>
        /// Below example throws ArgumentException with message "Parameter 'dateTime' should not be before '{DateTime.Now}'."
        /// <code>
        /// DateTime? dateTime = DateTime.Now.AddDays(-1);
        /// dateTime.ThrowIf(() => dateTime).AfterThanNow();
        /// </code>
        /// </example>
        public static NullableOrConstraint<System.DateTime> AfterThanNow(this INullableAssertion<System.DateTime> assertion)
        {
            assertion.ThrowIfNull();
            return assertion.AfterThan(System.DateTime.Now);
        }
    }
}