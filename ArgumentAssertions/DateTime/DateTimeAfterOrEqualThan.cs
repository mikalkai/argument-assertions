﻿using System;
using ArgumentAssertions.Generic.Internal;

namespace ArgumentAssertions.DateTime
{
    public static class DateTimeAfterOrEqualThan
    {
        /// <summary>
        /// Throw <see cref="ArgumentException"/> if date time is after or equal than other.
        /// </summary>
        /// <param name="assertion"></param>
        /// <param name="dateTime">Compared date time object.</param>
        /// <returns>Or constraint.</returns>
        /// <example>
        /// Below example throws ArgumentException with message "Parameter 'dateTime' should not be before '{other}'."
        /// <code>
        /// var dateTime = DateTime.Now.AddDays(-1);
        /// var other = DateTime.Now;
        /// dateTime.ThrowIf(() => dateTime).AfterOrEqualThan(other);
        /// </code>
        /// </example>
        public static OrConstraint<System.DateTime> AfterOrEqualThan(this IAssertion<System.DateTime> assertion, System.DateTime dateTime)
        {
            var context = assertion.Context();

            assertion.Assert(assertion.Value >= dateTime, 
                assertion.ArgumentExceptionExpression(context.AfterOrEqualThanMessage(dateTime)));

            return new OrConstraint<System.DateTime>(assertion);
        }

        /// <summary>
        /// Throw <see cref="ArgumentException"/> if date time is after or equal or equal than other.
        /// </summary>
        /// <param name="assertion"></param>
        /// <param name="dateTime">Compared date time object.</param>
        /// <example>
        /// Below example throws ArgumentException with message "Parameter 'dateTime' should not be before or equal than '{other}'."
        /// <code>
        /// var dateTime = DateTime.Now;
        /// var other = DateTime.Now;
        /// dateTime.ThrowIf(() => dateTime).AfterOrEqualThan(other);
        /// </code>
        /// </example>
        public static NullableOrConstraint<System.DateTime> AfterOrEqualThan(this INullableAssertion<System.DateTime> assertion, System.DateTime? dateTime)
        {
            var context = assertion.Context();

            assertion.ThrowIfNull();
            assertion.Assert(assertion.Value >= dateTime, 
                assertion.ArgumentExceptionExpression(context.AfterOrEqualThanMessage(dateTime)));

            return new NullableOrConstraint<System.DateTime>(assertion);
        }
    }
}