﻿using System;

namespace ArgumentAssertions.DateTime
{
    public static class DateTimeAfterOrEqualThanNow
    {
        /// <summary>
        /// Throw <see cref="ArgumentException"/> if date time is after or equal than now.
        /// </summary>
        /// <param name="assertion"></param>
        /// <example>
        /// Below example throws ArgumentException with message "Parameter 'dateTime' should not be before '{DateTime.Now}'."
        /// <code>
        /// var dateTime = DateTime.Now.AddDays(-1);
        /// dateTime.ThrowIf(() => dateTime).AfterOrEqualThanNow();
        /// </code>
        /// </example>
        public static OrConstraint<System.DateTime> AfterOrEqualThanNow(this IAssertion<System.DateTime> assertion)
        {
            return assertion.AfterOrEqualThan(System.DateTime.Now);
        }

        /// <summary>
        /// Throw <see cref="ArgumentException"/> if date time is after or equal or equal than now.
        /// </summary>
        /// <param name="assertion"></param>
        /// <example>
        /// Below example throws ArgumentException with message "Parameter 'dateTime' should not be before or equal than '{DateTime.Now}'."
        /// <code>
        /// var dateTime = DateTime.Now;
        /// dateTime.ThrowIf(() => dateTime).AfterOrEqualThanNow();
        /// </code>
        /// </example>
        public static NullableOrConstraint<System.DateTime> AfterOrEqualThanNow(this INullableAssertion<System.DateTime> assertion)
        {
            return assertion.AfterOrEqualThan(System.DateTime.Now);
        }
    }
}