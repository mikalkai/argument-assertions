﻿using System;
using System.Linq.Expressions;
using ArgumentAssertions.Generic;

namespace ArgumentAssertions.DateTime
{
    public static class DateTimeBeforeOrEqualThanNowNow
    {
        /// <summary>
        /// Throw <see cref="ArgumentException"/> if date time is before or equal than now.
        /// </summary>
        /// <param name="assertion"></param>
        /// <example>
        /// Below example throws ArgumentException with message "Parameter 'dateTime' should not be before or equal than '{DateTime.Now}'."
        /// <code>
        /// var dateTime = DateTime.Now.AddDays(-1);
        /// dateTime.ThrowIf(() => dateTime).BeforeOrEqualThanNow();
        /// </code>
        /// </example>
        public static OrConstraint<System.DateTime> BeforeOrEqualThanNow(this IAssertion<System.DateTime> assertion)
        {
            return assertion.BeforeOrEqualThan(System.DateTime.Now);
        }

        /// <summary>
        /// Throw <see cref="ArgumentException"/> if date time is before or equal than now.
        /// </summary>
        /// <param name="assertion"></param>
        /// <example>
        /// Below example throws ArgumentException with message "Parameter 'dateTime' should not be before or equal than '{DateTime.Now}'."
        /// <code>
        /// var dateTime = DateTime.Now;
        /// dateTime.ThrowIf(() => dateTime).BeforeOrEqualThanNow();
        /// </code>
        /// </example>
        public static NullableOrConstraint<System.DateTime> BeforeOrEqualThanNow(this INullableAssertion<System.DateTime> assertion)
        {
            return assertion.BeforeOrEqualThan(System.DateTime.Now);
        }
    }
}