﻿using System;

namespace ArgumentAssertions.DateTime
{
    public static class DateTimeBeforeThanNow
    {
        /// <summary>
        /// Throw <see cref="ArgumentException"/> if date time is before than now.
        /// </summary>
        /// <param name="assertion"></param>
        /// <example>
        /// Below example throws ArgumentException with message "Parameter 'dateTime' should not be before '{DateTime.Now}'."
        /// <code>
        /// var dateTime = DateTime.Now.AddDays(-1);
        /// dateTime.ThrowIf(() => dateTime).BeforeThanNow();
        /// </code>
        /// </example>
        public static OrConstraint<System.DateTime> BeforeThanNow(this IAssertion<System.DateTime> assertion)
        {
            return assertion.BeforeThan(System.DateTime.Now);
        }

        /// <summary>
        /// Throw <see cref="ArgumentException"/> if date time is before than other.
        /// </summary>
        /// <param name="assertion"></param>
        /// <example>
        /// Below example throws ArgumentException with message "Parameter 'dateTime' should not be before '{DateTime.Now}'."
        /// <code>
        /// DateTime? dateTime = DateTime.Now.AddDays(-1);
        /// dateTime.ThrowIf(() => dateTime).BeforeThanNow();
        /// </code>
        /// </example>
        public static NullableOrConstraint<System.DateTime> BeforeThanNow(this INullableAssertion<System.DateTime> assertion)
        {
            return assertion.BeforeThan(System.DateTime.Now);
        }
    }
}