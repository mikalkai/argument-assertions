﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace ArgumentAssertions
{
    public class NullableIfAssertion<T> : NullableAssertion<T>
        where T : struct 
    {
        public NullableIfAssertion(T? value, string parameterName)
            : base(value, Condition.If, parameterName) {}

        public NullableIfAssertion(T? value, Expression<Func<Exception>> exceptionExpression)
            : base(value, Condition.If, exceptionExpression) {}

        public override T? Assert(Func<bool> evaluate, Expression<Func<Exception>> exceptionExpr)
        {
            return Assert(evaluate(), exceptionExpr);
        }

        public override T? Assert(bool result, Expression<Func<Exception>> exceptionExpr)
        {
            if (!result || IgnoreFuncs.Any(func => func()))
            {
                return Value;
            }

            throw CompileException(exceptionExpr);
        }
    }
}