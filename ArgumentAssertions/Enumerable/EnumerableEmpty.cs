﻿using System;
using System.Collections;
using ArgumentAssertions.Enumerable.Internal;
using ArgumentAssertions.Generic.Internal;

namespace ArgumentAssertions.Enumerable
{
    public static class EnumerableEmpty
    {
        /// <summary>
        /// Throw <see cref="ArgumentException"/> if the enumerable is empty.
        /// </summary>
        /// <param name="assertion"></param>
        /// <remarks>
        /// Null collection is assumed to be empty.
        /// </remarks>
        /// <example>
        /// Below example throws ArgumentException with message "Parameter 'list' should not be empty."
        /// <code>
        /// var list = new List&lt;string&gt;();
        /// list.ThrowIf(() => list).Empty();
        /// </code>
        /// </example>
        public static OrConstraint<T> Empty<T>(this IAssertion<T> assertion)
            where T : class, IEnumerable
        {
            var context = assertion.Context();

            assertion.ThrowIfNull();
            assertion.Assert(() => assertion.Value.Count() == 0,
                assertion.ArgumentExceptionExpression(context.EmptyMessage()));

            return new OrConstraint<T>(assertion);
        }
    }
}