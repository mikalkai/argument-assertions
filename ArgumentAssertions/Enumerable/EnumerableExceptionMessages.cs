﻿using System;

namespace ArgumentAssertions.Enumerable
{
    public static class EnumerableExceptionMessages
    {
        /// <summary>
        /// Format contains an item message.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="item">Expected item.</param>
        /// <returns>Exception message.</returns>
        public static string ContainsMessage(this AssertionContext context, object item)
        {
            var condition = context.Condition;
            var paramName = context.ParameterName;
            switch (condition)
            {
                case Condition.If:
                    return $"Parameter '{paramName}' should not contain item '{item}'.";
                case Condition.IfNot:
                    return $"Parameter '{paramName}' should contain item '{item}'.";
                default:
                    throw new ArgumentOutOfRangeException(nameof(condition), condition, null);
            }
        }

        /// <summary>
        /// Format a generic empty message.
        /// </summary>
        /// <param name="context"></param>
        /// <returns>Error message.</returns>
        public static string EmptyMessage(this AssertionContext context)
        {
            var condition = context.Condition;
            var paramName = context.ParameterName;
            switch (condition)
            {
                case Condition.If:
                    return $"Parameter '{paramName}' should not be empty.";
                case Condition.IfNot:
                    return $"Parameter '{paramName}' should be empty.";
                default:
                    throw new ArgumentOutOfRangeException(nameof(condition), condition, null);
            }
        }
    }
}