﻿using System.Collections;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace ArgumentAssertions.Enumerable.Internal
{
    internal static class EnumerableCount
    {
        /// <summary>
        /// Extension in enumerable to check element count.
        /// </summary>
        /// <param name="enumerable"></param>
        /// <returns>Element count.</returns>
        [SuppressMessage("ReSharper", "TailRecursiveCall")]
        [SuppressMessage("ReSharper", "ConvertIfStatementToReturnStatement")]
        internal static int Count(this IEnumerable enumerable)
        {
            var collection = enumerable as ICollection;
            if (collection != null)
            {
                return collection.Count;
            }

            var s = enumerable as string;
            if (s != null)
            {
                return s.Length;
            }

            return enumerable.Cast<object>().Count();
        }
    }
}