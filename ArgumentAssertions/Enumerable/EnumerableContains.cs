﻿using System;
using System.Collections.Generic;
using System.Linq;
using ArgumentAssertions.Generic.Internal;

namespace ArgumentAssertions.Enumerable
{
    public static class EnumerableContains
    {
        /// <summary>
        /// Throw <see cref="ArgumentException"/> if the enumerable contains a specific item.
        /// </summary>
        /// <typeparam name="T">Type of item in <see cref="IEnumerable{T}"/></typeparam>
        /// <param name="assertion"></param>
        /// <param name="func">Function to evaluate item.</param>
        /// <returns>Or constraint.</returns>
        /// <remarks>
        /// Null collection throws <see cref="ArgumentNullException"/>.
        /// </remarks>
        /// <example>
        /// Below example throws ArgumentException with message "Parameter 'list' should not contain item 'bar'."
        /// <code>
        /// var list = new List&lt;string&gt;{ "foo", "bar" };
        /// list.ThrowIf(() => list).Contains(s => s.Equals("bar"));
        /// </code>
        /// </example>
        public static OrConstraint<IEnumerable<T>> Contains<T>(this IAssertion<IEnumerable<T>> assertion, Func<T, bool> func)
        {
            var context = assertion.Context();

            assertion.ThrowIfNull();
            assertion.Assert(assertion.Value.Any(func),
                assertion.ArgumentExceptionExpression(context.ContainsMessage(assertion.Value.FirstOrDefault(func))));

            return new OrConstraint<IEnumerable<T>>(assertion);
        }

        /// <summary>
        /// Throw <see cref="ArgumentException"/> if the enumerable contains a specific item.
        /// Comparison is performed on Equals basis.
        /// </summary>
        /// <typeparam name="T">Type of item in <see cref="IEnumerable{T}"/></typeparam>
        /// <param name="assertion"></param>
        /// <param name="item">Item to be searched.</param>
        /// <returns>Or constraint.</returns>
        public static OrConstraint<IEnumerable<T>> Contains<T>(this IAssertion<IEnumerable<T>> assertion, T item)
        {
            var context = assertion.Context();

            assertion.ThrowIfNull();
            assertion.Assert(assertion.Value.Any(arg => arg.Equals(item)),
                assertion.ArgumentExceptionExpression(context.ContainsMessage(item)));

            return new OrConstraint<IEnumerable<T>>(assertion);
        }
    }
}