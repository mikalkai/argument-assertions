﻿using System;
using ArgumentAssertions.Generic;
using ArgumentAssertions.Generic.Internal;
using ArgumentAssertions.Numeric;

namespace ArgumentAssertions.String
{
    public static class StringLengthIsLessOrEqualThan
    {
        /// <summary>
        /// Throw <see cref="ArgumentException"/> if the string length is less or equal than compared one.
        /// </summary>
        /// <param name="assertion"></param>
        /// <param name="length">Compared length.</param>
        /// <returns>Or constraint.</returns>
        /// <remarks>
        /// Null string throws a <see cref="ArgumentNullException"/>.
        /// </remarks>
        public static OrConstraint<string> LengthIsLessOrEqualThan(this IAssertion<string> assertion, int length)
        {
            assertion.ThrowIfNull();
            assertion.PropertyIs(s => s.Length, "Length").LessOrEqualThan(length);

            return new OrConstraint<string>(assertion);
        }

        /// <summary>
        /// Throw <see cref="ArgumentException"/> if the string length is less than compared one.
        /// </summary>
        /// <param name="assertion"></param>
        /// <param name="other">Other string.</param>
        /// <returns>Or constraint.</returns>
        /// <remarks>
        /// Null string throws a <see cref="ArgumentNullException"/>.
        /// </remarks>
        public static OrConstraint<string> LengthIsLessOrEqualThan(this IAssertion<string> assertion, string other)
        {
            return assertion.LengthIsLessOrEqualThan(other?.Length ?? 0);
        }
    }
}