﻿using System;
using ArgumentAssertions.Generic.Internal;

namespace ArgumentAssertions.String
{
    public static class StringContains
    {
        /// <summary>
        /// Throw <see cref="ArgumentException"/> if the string contains the given pattern.
        /// Search is case sensitive.
        /// </summary>
        /// <param name="assertion"></param>
        /// <param name="pattern">Pattern to search.</param>
        /// <example>
        /// Below example throws ArgumentException with message "Parameter 'str' should not contain 'me'."
        /// <code>
        /// string str = "find me";
        /// str.ThrowIf(() => str).Contains("me");
        /// </code>
        /// </example>
        public static OrConstraint<string> Contains(this IAssertion<string> assertion, string pattern)
        {
            var context = assertion.Context();

            assertion.ThrowIfNull();
            assertion.Assert(assertion.Value.Contains(pattern),
                assertion.ArgumentExceptionExpression(context.ContainsMessage(pattern)));

            return new OrConstraint<string>(assertion);
        }
    }
}