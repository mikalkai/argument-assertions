﻿using System;
using ArgumentAssertions.Generic;
using ArgumentAssertions.Generic.Internal;

namespace ArgumentAssertions.String
{
    public static class StringLengthIs
    {
        /// <summary>
        /// Throw <see cref="ArgumentException"/> if the string length is same than compared one.
        /// </summary>
        /// <param name="assertion"></param>
        /// <param name="length">Compared length.</param>
        /// <returns>Or constraint.</returns>
        /// <remarks>
        /// Null string throws a <see cref="ArgumentNullException"/>.
        /// </remarks>
        public static OrConstraint<string> LengthIs(this IAssertion<string> assertion, int length)
        {
            assertion.ThrowIfNull();
            assertion.PropertyIs(s => s.Length, "Length").EqualTo(length);

            return new OrConstraint<string>(assertion);
        }

        /// <summary>
        /// Throw <see cref="ArgumentException"/> if the string length is same than compared one.
        /// </summary>
        /// <param name="assertion"></param>
        /// <param name="other"></param>
        /// <returns>Or constraint.</returns>
        /// <remarks>
        /// Null string throws a <see cref="ArgumentNullException"/>.
        /// </remarks>
        public static OrConstraint<string> LengthIs(this IAssertion<string> assertion, string other)
        {
            return assertion.LengthIs(other?.Length ?? 0);
        }
    }
}