﻿using System;
using ArgumentAssertions.Generic;
using ArgumentAssertions.Generic.Internal;

namespace ArgumentAssertions.String
{
    public static class StringEqualTo
    {
        /// <summary>
        /// Throw <see cref="ArgumentException"/> if the string does not equal to the given parameter
        /// with the specified comparison options. Default option is <see cref="StringComparison.Ordinal"/>
        /// </summary>
        /// <param name="assertion"></param>
        /// <param name="other">Other string to be compared.</param>
        /// <param name="comparison">Comparison options.</param>
        /// <returns>Or constraint.</returns>
        public static OrConstraint<string> EqualTo(this IAssertion<string> assertion, string other, 
            StringComparison comparison = StringComparison.Ordinal)
        {
            var context = assertion.Context();

            assertion.Assert(assertion.Value.Equals(other, comparison),
                assertion.ArgumentExceptionExpression(context.EqualToMessage(other)));

            return new OrConstraint<string>(assertion);
        }
    }
}