﻿using System;
using ArgumentAssertions.Enumerable;
using ArgumentAssertions.Generic.Internal;

namespace ArgumentAssertions.String
{
    public static class StringEmpty
    {
        /// <summary>
        /// Throw <see cref="ArgumentException"/> if the string is empty.
        /// </summary>
        /// <param name="assertion"></param>
        /// <example>
        /// Below example throws ArgumentException with message "Parameter 'str' should not be empty."
        /// <code>
        /// string str = "";
        /// str.ThrowIf(() => str).Empty();
        /// </code>
        /// </example>
        public static OrConstraint<string> Empty(this IAssertion<string> assertion)
        {
            var context = assertion.Context();

            assertion.ThrowIfNull();
            assertion.Assert(assertion.Value.Length == 0,
                assertion.ArgumentExceptionExpression(context.EmptyMessage()));

            return new OrConstraint<string>(assertion);
        }
    }
}