﻿using System;
using ArgumentAssertions.Generic;
using ArgumentAssertions.Generic.Internal;

namespace ArgumentAssertions.String
{
    public static class StringNull
    {
        /// <summary>
        /// Throw <see cref="ArgumentNullException"/> if the string is null.
        /// </summary>
        /// <param name="assertion"></param>
        /// <example>
        /// Below example throws ArgumentNullException with message "Parameter 'a.Bb.Text' should not be null."
        /// <code>
        /// var a = new A
        /// {
        ///     Bb = new B 
        ///     { 
        ///         Text = null 
        ///     } 
        /// };
        /// a.ThrowIf().Null(() => a);
        /// a.Bb.ThrowIf().Null(() => a.Bb);
        /// a.Bb.Text.ThrowIf().Null(() => a.Bb.Text);
        /// </code>
        /// </example>
        public static OrConstraint<string> Null(this IAssertion<string> assertion)
        {
            var context = assertion.Context();

            assertion.Assert(assertion.Value == null,
                assertion.ArgumentNullExceptionExpression(context.NullMessage()));

            return new OrConstraint<string>(assertion);
        }
    }
}