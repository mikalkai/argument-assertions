﻿using System;
using System.Text.RegularExpressions;
using ArgumentAssertions.Generic.Internal;

namespace ArgumentAssertions.String
{
    public static class StringMatch
    {
        /// <summary>
        /// Throw <see cref="ArgumentException"/> if the string matches to a given regex.
        /// </summary>
        /// <param name="assertion"></param>
        /// <param name="regex">Regular expression to match.</param>
        /// <example>
        /// Below example throws ArgumentException with message "Parameter 'str' should match with pattern '\d+'."
        /// <code>
        /// string str = "not a number";
        /// str.ThrowIfNot(() => str).Match(new Regex("\\d+"));
        /// </code>
        /// </example>
        public static OrConstraint<string> Match(this IAssertion<string> assertion, Regex regex)
        {
            var context = assertion.Context();

            assertion.ThrowIfNull();
            assertion.Assert(regex.IsMatch(assertion.Value ?? string.Empty),
                assertion.ArgumentExceptionExpression(context.MatchMessage(regex.ToString())));

            return new OrConstraint<string>(assertion);
        }
    }
}