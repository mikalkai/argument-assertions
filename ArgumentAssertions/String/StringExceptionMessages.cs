﻿using System;

namespace ArgumentAssertions.String
{
    public static class StringExceptionMessages
    {
        /// <summary>
        /// Error message for a regex match.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="pattern">Regex pattern.</param>
        /// <returns>The message.</returns>
        public static string MatchMessage(this AssertionContext context, string pattern)
        {
            var condition = context.Condition;
            var paramName = context.ParameterName;
            switch (condition)
            {
                case Condition.If:
                    return $"Parameter '{paramName}' should not match with pattern '{pattern}'.";
                case Condition.IfNot:
                    return $"Parameter '{paramName}' should match with pattern '{pattern}'.";
                default:
                    throw new ArgumentOutOfRangeException(nameof(condition), condition, null);
            }
        }

        /// <summary>
        /// Error message for a contains pattern.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="pattern">The pattern.</param>
        /// <returns>The message.</returns>
        public static string ContainsMessage(this AssertionContext context, string pattern)
        {
            var condition = context.Condition;
            var paramName = context.ParameterName;
            switch (condition)
            {
                case Condition.If:
                    return $"Parameter '{paramName}' should not contain '{pattern}'.";
                case Condition.IfNot:
                    return $"Parameter '{paramName}' should contain '{pattern}'.";
                default:
                    throw new ArgumentOutOfRangeException(nameof(condition), condition, null);
            }
        }
    }
}