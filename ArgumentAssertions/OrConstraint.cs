﻿using System;
using ArgumentAssertions.Generic.Internal;

namespace ArgumentAssertions
{
    public class OrConstraint<T>
    {
        private readonly IAssertion<T> _assertion;
        private readonly AssertionContext _context;

        public OrConstraint(IAssertion<T> assertion)
        {
            if (assertion == null)
            {
                throw new ArgumentNullException(nameof(assertion));
            }
            _assertion = assertion;
            _context = assertion.Context();
        }

        public IAssertion<T> OrIf => _context.ParameterName != null
            ? new IfAssertion<T>(_assertion.Value, _context.ParameterName)
            : new IfAssertion<T>(_assertion.Value, _context.ExceptionExpression);

        public IAssertion<T> OrIfNot => _context.ParameterName != null
            ? new IfNotAssertion<T>(_assertion.Value, _context.ParameterName)
            : new IfNotAssertion<T>(_assertion.Value, _context.ExceptionExpression);

        public static implicit operator T(OrConstraint<T> constraint)
        {
            return constraint._assertion.Value;
        }
    }
}