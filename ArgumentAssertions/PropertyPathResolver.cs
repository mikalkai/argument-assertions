﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace ArgumentAssertions
{
    public static class PropertyPathResolver
    {
        public static string ResolvePropertyPath<T1, T2>(this Expression<Func<T1, T2>> expression)
        {
            return (expression as LambdaExpression).ResolvePropertyPath();
        }

        public static string ResolvePropertyPath<T>(this Expression<Func<T>> expression)
        {
            return (expression as LambdaExpression).ResolvePropertyPath();
        }

        public static string ResolvePropertyPath(this LambdaExpression lambda)
        {
            if (lambda == null)
            {
                return "undefined";
            }

            var memberExpr = AsMemberExpression(lambda.Body);
            if (memberExpr == null)
            {
                return "undefined";
            }

            var properties = new List<string>();
            while (memberExpr != null)
            {
                properties.Insert(0, memberExpr.Member.Name);
                memberExpr = AsMemberExpression(memberExpr.Expression);
            }

            return string.Join(".", properties);
        }

        private static MemberExpression AsMemberExpression(this Expression expression)
        {
            if (expression.NodeType == ExpressionType.Convert)
            {
                return ((UnaryExpression) expression).Operand as MemberExpression;
            }
            if (expression.NodeType == ExpressionType.MemberAccess)
            {
                return expression as MemberExpression;
            }
            return null;
        }
    }
}