﻿namespace ArgumentAssertions
{
    public enum Condition
    {
        /// <summary>
        /// Assert if the condition matches.
        /// </summary>
        If,

        /// <summary>
        /// Asset if the condition does not match.
        /// </summary>
        IfNot
    }
}