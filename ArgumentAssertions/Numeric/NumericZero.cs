﻿using System;
using System.Diagnostics.CodeAnalysis;
using ArgumentAssertions.Generic.Internal;

namespace ArgumentAssertions.Numeric
{
    public static class NumericZero
    {
        /// <summary>
        /// Throw <see cref="ArgumentException"/> if a numeric value is zero.
        /// </summary>
        /// <param name="assertion"></param>
        /// <example>
        /// Below example throws ArgumentException with message "Parameter 'number' should not be zero."
        /// <code>
        /// int number = 0;
        /// number.ThrowIf(() => number).Zero();
        /// </code>
        /// </example>
        public static OrConstraint<T> Zero<T>(this IAssertion<T> assertion)
            where T : struct, IComparable, IComparable<T>, IConvertible, IEquatable<T>, IFormattable
        {
            var context = assertion.Context();

            assertion.Assert(assertion.Value.CompareTo(0) == 0,
                assertion.ArgumentExceptionExpression(context.ZeroMessage()));

            return new OrConstraint<T>(assertion);
        }

        /// <summary>
        /// Throw <see cref="ArgumentException"/> if a numeric value is zero.
        /// </summary>
        /// <param name="assertion"></param>
        /// <example>
        /// Below example throws ArgumentException with message "Parameter 'number' should not be zero."
        /// <code>
        /// int? number = 0;
        /// number.ThrowIf(() => number).Zero();
        /// </code>
        /// </example>
        [SuppressMessage("ReSharper", "UseNullPropagation")]
        public static NullableOrConstraint<T> Zero<T>(this INullableAssertion<T> assertion)
            where T : struct, IComparable, IComparable<T>, IConvertible, IEquatable<T>, IFormattable
        {
            var context = assertion.Context();

            assertion.ThrowIfNull();
            assertion.Assert(() =>
            {
                var nullableType = assertion.Value;
                if (!nullableType.HasValue)
                {
                    return false;
                }
                return nullableType.Value.CompareTo(0) == 0;
            }, assertion.ArgumentExceptionExpression(context.ZeroMessage()));

            return new NullableOrConstraint<T>(assertion);
        }
    }
}