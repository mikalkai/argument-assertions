﻿using System;

namespace ArgumentAssertions.Numeric
{
    public static class NumericExceptionMessages
    {
        /// <summary>
        /// Format message for int less than message.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="value">The value.</param>
        /// <returns>Exception message.</returns>
        public static string LessThanMessage(this AssertionContext context, object value)
        {
            var condition = context.Condition;
            var paramName = context.ParameterName;
            switch (condition)
            {
                case Condition.If:
                    return $"Parameter '{paramName}' should not be less than '{value}'.";
                case Condition.IfNot:
                    return $"Parameter '{paramName}' should be less than '{value}'.";
                default:
                    throw new ArgumentOutOfRangeException(nameof(condition), condition, null);
            }
        }

        /// <summary>
        /// Format message for int less or equal than message.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="value">The value.</param>
        /// <returns>Exception message.</returns>
        public static string LessOrEqualThanMessage(this AssertionContext context, object value)
        {
            var condition = context.Condition;
            var paramName = context.ParameterName;
            switch (condition)
            {
                case Condition.If:
                    return $"Parameter '{paramName}' should not be less or equal than '{value}'.";
                case Condition.IfNot:
                    return $"Parameter '{paramName}' should be less or equal than '{value}'.";
                default:
                    throw new ArgumentOutOfRangeException(nameof(condition), condition, null);
            }
        }

        /// <summary>
        /// Format message for numeric value more than message.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="value">The value.</param>
        /// <returns>Exception message.</returns>
        public static string MoreThanMessage(this AssertionContext context, object value)
        {
            var condition = context.Condition;
            var paramName = context.ParameterName;
            switch (condition)
            {
                case Condition.If:
                    return $"Parameter '{paramName}' should not be more than '{value}'.";
                case Condition.IfNot:
                    return $"Parameter '{paramName}' should be more than '{value}'.";
                default:
                    throw new ArgumentOutOfRangeException(nameof(condition), condition, null);
            }
        }

        /// <summary>
        /// Format message for numeric value more or equal than message.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="value">The value.</param>
        /// <returns>Exception message.</returns>
        public static string MoreOrEqualThanMessage(this AssertionContext context, object value)
        {
            var condition = context.Condition;
            var paramName = context.ParameterName;
            switch (condition)
            {
                case Condition.If:
                    return $"Parameter '{paramName}' should not be more or equal than '{value}'.";
                case Condition.IfNot:
                    return $"Parameter '{paramName}' should be more or equal than '{value}'.";
                default:
                    throw new ArgumentOutOfRangeException(nameof(condition), condition, null);
            }
        }

        /// <summary>
        /// Format message for int in between message.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="low">Low value.</param>
        /// <param name="high">High value.</param>
        /// <returns>Exception message.</returns>
        public static string InBetweenMessage(this AssertionContext context, object low, object high)
        {
            var condition = context.Condition;
            var paramName = context.ParameterName;
            switch (condition)
            {
                case Condition.If:
                    return $"Parameter '{paramName}' should not be in between '{low}' and '{high}'.";
                case Condition.IfNot:
                    return $"Parameter '{paramName}' should be in between '{low}' and '{high}'.";
                default:
                    throw new ArgumentOutOfRangeException(nameof(condition), condition, null);
            }
        }

        /// <summary>
        /// Format message for numeric value being negative.
        /// </summary>
        /// <param name="context"></param>
        /// <returns>Exception message.</returns>
        public static string NegativeMessage(this AssertionContext context)
        {
            var condition = context.Condition;
            var paramName = context.ParameterName;
            switch (condition)
            {
                case Condition.If:
                    return $"Parameter '{paramName}' should not be negative.";
                case Condition.IfNot:
                    return $"Parameter '{paramName}' should be negative.";
                default:
                    throw new ArgumentOutOfRangeException(nameof(condition), condition, null);
            }
        }

        /// <summary>
        /// Format message for numeric value being positive.
        /// </summary>
        /// <param name="context"></param>
        /// <returns>Exception message.</returns>
        public static string PositiveMessage(this AssertionContext context)
        {
            var condition = context.Condition;
            var paramName = context.ParameterName;
            switch (condition)
            {
                case Condition.If:
                    return $"Parameter '{paramName}' should not be positive.";
                case Condition.IfNot:
                    return $"Parameter '{paramName}' should be positive.";
                default:
                    throw new ArgumentOutOfRangeException(nameof(condition), condition, null);
            }
        }

        /// <summary>
        /// Format message for numeric value being zero.
        /// </summary>
        /// <param name="context"></param>
        /// <returns>Exception message.</returns>
        public static string ZeroMessage(this AssertionContext context)
        {
            var condition = context.Condition;
            var paramName = context.ParameterName;
            switch (condition)
            {
                case Condition.If:
                    return $"Parameter '{paramName}' should not be zero.";
                case Condition.IfNot:
                    return $"Parameter '{paramName}' should be zero.";
                default:
                    throw new ArgumentOutOfRangeException(nameof(condition), condition, null);
            }
        }
    }
}