﻿using System;
using System.Diagnostics.CodeAnalysis;
using ArgumentAssertions.Generic.Internal;

namespace ArgumentAssertions.Numeric
{
    public static class NumericNegative
    {
        /// <summary>
        /// Throw <see cref="ArgumentException"/> if a numeric value is negative.
        /// </summary>
        /// <param name="assertion"></param>
        /// <example>
        /// Below example throws ArgumentException with message "Parameter 'number' should not be negative."
        /// <code>
        /// int number = -1;
        /// number.ThrowIf(() => number).Negative();
        /// </code>
        /// </example>
        public static OrConstraint<T> Negative<T>(this IAssertion<T> assertion)
            where T : struct, IComparable, IComparable<T>, IConvertible, IEquatable<T>, IFormattable
        {
            var context = assertion.Context();

            assertion.Assert(assertion.Value.CompareTo(0) < 0,
                assertion.ArgumentExceptionExpression(context.NegativeMessage()));

            return new OrConstraint<T>(assertion);
        }

        /// <summary>
        /// Throw <see cref="ArgumentException"/> if a numeric value is negative.
        /// </summary>
        /// <param name="assertion"></param>
        /// <example>
        /// Below example throws ArgumentException with message "Parameter 'number' should not be negative."
        /// <code>
        /// int? number = -1;
        /// number.ThrowIf(() => number).Negative();
        /// </code>
        /// </example>
        [SuppressMessage("ReSharper", "UseNullPropagation")]
        public static NullableOrConstraint<T> Negative<T>(this INullableAssertion<T> assertion)
            where T : struct, IComparable, IComparable<T>, IConvertible, IEquatable<T>, IFormattable
        {
            var context = assertion.Context();

            assertion.ThrowIfNull();
            assertion.Assert(() =>
            {
                var nullableType = assertion.Value;
                if (!nullableType.HasValue)
                {
                    return false;
                }
                return nullableType.Value.CompareTo(0) < 0;
            }, assertion.ArgumentExceptionExpression(context.NegativeMessage()));

            return new NullableOrConstraint<T>(assertion);
        }
    }
}