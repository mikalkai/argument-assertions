﻿using System;
using ArgumentAssertions.Generic.Internal;

namespace ArgumentAssertions.Numeric
{
    public static class NumericInBetween
    {
        /// <summary>
        /// Throw <see cref="ArgumentException"/> if a numeric value is in the expected range.
        /// </summary>
        /// <param name="assertion"></param>
        /// <param name="low">Low value.</param>
        /// <param name="high">High value.</param>
        /// <example>
        /// Below example throws ArgumentException with message "Parameter 'number' should be in between '0' and '2'."
        /// <code>
        /// var number = 3;
        /// var list = new List&lt;int&gt;{0, 1, 2};
        /// number.ThrowIfNot(() => number).InBetween(0, list.Count - 1);
        /// </code>
        /// </example>
        public static OrConstraint<T> InBetween<T>(this IAssertion<T> assertion, T low, T high)
            where T : struct, IComparable, IComparable<T>, IConvertible, IEquatable<T>, IFormattable
        {
            var context = assertion.Context();

            assertion.Assert(assertion.Value.CompareTo(low) >= 0 && assertion.Value.CompareTo(high) <= 0,
                assertion.ArgumentExceptionExpression(context.InBetweenMessage(low, high)));

            return new OrConstraint<T>(assertion);
        }

        /// <summary>
        /// Throw <see cref="ArgumentException"/> if a numeric value is in the expected range.
        /// </summary>
        /// <param name="assertion"></param>
        /// <param name="low">Low value.</param>
        /// <param name="high">High value.</param>
        /// <example>
        /// Below example throws ArgumentException with message "Parameter 'number' should be in between '0' and '2'."
        /// <code>
        /// int? number = 3;
        /// var list = new List&lt;int&gt;{0, 1, 2};
        /// number.ThrowIfNot(() => number).InBetween(0, list.Count - 1);
        /// </code>
        /// </example>
        public static NullableOrConstraint<T> InBetween<T>(this INullableAssertion<T> assertion, T? low, T? high)
            where T : struct, IComparable, IComparable<T>, IConvertible, IEquatable<T>, IFormattable
        {
            var context = assertion.Context();

            assertion.ThrowIfNull();
            assertion.Assert(() =>
            {
                var nullable = assertion.Value;
                if (!nullable.HasValue || !low.HasValue || !high.HasValue)
                {
                    return false;
                }
                return nullable.Value.CompareTo(low.Value) >= 0 && nullable.Value.CompareTo(high.Value) <= 0;
            }, assertion.ArgumentExceptionExpression(context.InBetweenMessage(low, high)));

            return new NullableOrConstraint<T>(assertion);
        }
    }
}