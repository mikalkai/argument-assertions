﻿using System;
using ArgumentAssertions.Generic.Internal;

namespace ArgumentAssertions.Numeric
{
    public static class NumericLessThan
    {
        /// <summary>
        /// Throw <see cref="ArgumentException"/> if a numeric value is less than expected.
        /// </summary>
        /// <param name="assertion"></param>
        /// <param name="value">Expected value.</param>
        /// <example>
        /// Below example throws ArgumentException with message "Parameter 'number' should not be less than '1'."
        /// <code>
        /// int number = 0;
        /// number.ThrowIf(() => number).LessThan(1);
        /// </code>
        /// </example>
        public static OrConstraint<T> LessThan<T>(this IAssertion<T> assertion, T value)
            where T : struct, IComparable, IComparable<T>, IConvertible, IEquatable<T>, IFormattable
        {
            var context = assertion.Context();

            assertion.Assert(assertion.Value.CompareTo(value) < 0,
                assertion.ArgumentExceptionExpression(context.LessThanMessage(value)));

            return new OrConstraint<T>(assertion);
        }

        /// <summary>
        /// Throw <see cref="ArgumentException"/> if a numeric is less than expected.
        /// </summary>
        /// <param name="assertion"></param>
        /// <param name="value">Expected value.</param>
        /// <example>
        /// Below example throws ArgumentException with message "Parameter 'number' should not be less than '1'."
        /// <code>
        /// int? number = 0;
        /// number.ThrowIf(() => number).LessThan(1);
        /// </code>
        /// </example>
        public static NullableOrConstraint<T> LessThan<T>(this INullableAssertion<T> assertion, T? value)
            where T : struct, IComparable, IComparable<T>, IConvertible, IEquatable<T>, IFormattable
        {
            var context = assertion.Context();

            assertion.ThrowIfNull();
            assertion.Assert(() =>
            {
                var nullable = assertion.Value;
                if (!nullable.HasValue || !value.HasValue)
                {
                    return false;
                }
                return nullable.Value.CompareTo(value.Value) < 0;
            }, assertion.ArgumentExceptionExpression(context.LessThanMessage(value)));

            return new NullableOrConstraint<T>(assertion);
        }
    }
}