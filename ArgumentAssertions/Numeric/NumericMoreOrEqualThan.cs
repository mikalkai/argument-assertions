﻿using System;
using ArgumentAssertions.Generic.Internal;

namespace ArgumentAssertions.Numeric
{
    public static class NumericMoreOrEqualThan
    {
        /// <summary>
        /// Throw <see cref="ArgumentException"/> if a numeric value is more than expected.
        /// </summary>
        /// <param name="assertion"></param>
        /// <param name="value">Expected value.</param>
        /// <example>
        /// Below example throws ArgumentException with message "Parameter 'number' should not be more or equal than '1'."
        /// <code>
        /// int number = 1;
        /// number.ThrowIf(() => number).MoreOrEqualThan(1);
        /// </code>
        /// </example>
        public static OrConstraint<T> MoreOrEqualThan<T>(this IAssertion<T> assertion, T value)
            where T : struct, IComparable, IComparable<T>, IConvertible, IEquatable<T>, IFormattable
        {
            var context = assertion.Context();

            assertion.Assert(assertion.Value.CompareTo(value) >= 0,
                assertion.ArgumentExceptionExpression(context.MoreOrEqualThanMessage(value)));

            return new OrConstraint<T>(assertion);
        }

        /// <summary>
        /// Throw <see cref="ArgumentException"/> if a numeric value is more than expected.
        /// </summary>
        /// <param name="assertion"></param>
        /// <param name="value">Expected value.</param>
        /// <example>
        /// Below example throws ArgumentException with message "Parameter 'number' should not be greater or equal than '1'."
        /// <code>
        /// int? number = 1;
        /// number.ThrowIf(() => number).MoreOrEqualThan(1);
        /// </code>
        /// </example>
        public static NullableOrConstraint<T> MoreOrEqualThan<T>(this INullableAssertion<T> assertion, T? value)
            where T : struct, IComparable, IComparable<T>, IConvertible, IEquatable<T>, IFormattable
        {
            var context = assertion.Context();

            assertion.ThrowIfNull();
            assertion.Assert(() =>
            {
                var nullable = assertion.Value;
                if (!nullable.HasValue || !value.HasValue)
                {
                    return false;
                }
                return nullable.Value.CompareTo(value.Value) >= 0;
            }, assertion.ArgumentExceptionExpression(context.MoreOrEqualThanMessage(value)));

            return new NullableOrConstraint<T>(assertion);
        }
    }
}