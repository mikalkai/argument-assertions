﻿using System;
using ArgumentAssertions.Generic.Internal;

namespace ArgumentAssertions.Numeric
{
    public static class NumericLessOrEqualThan
    {
        /// <summary>
        /// Throw <see cref="ArgumentException"/> if a numeric value is less or equal than expected.
        /// </summary>
        /// <param name="assertion"></param>
        /// <param name="value">Expected value.</param>
        /// <example>
        /// Below example throws ArgumentException with message "Parameter 'number' should not be less or equal than '0'."
        /// <code>
        /// int number = 0;
        /// number.ThrowIf(() => number).LessThanOrEqual(0);
        /// </code>
        /// </example>
        public static OrConstraint<T> LessOrEqualThan<T>(this IAssertion<T> assertion, T value)
            where T : struct, IComparable, IComparable<T>, IConvertible, IEquatable<T>, IFormattable
        {
            var context = assertion.Context();

            assertion.Assert(assertion.Value.CompareTo(value) <= 0,
                assertion.ArgumentExceptionExpression(context.LessOrEqualThanMessage(value)));

            return new OrConstraint<T>(assertion);
        }

        /// <summary>
        /// Throw <see cref="ArgumentException"/> a numeric value is less or equal than expected.
        /// </summary>
        /// <param name="assertion"></param>
        /// <param name="value">Expected value.</param>
        /// <example>
        /// Below example throws ArgumentException with message "Parameter 'number' should not be less or equal than '0'."
        /// <code>
        /// int? number = 0;
        /// number.ThrowIf(() => number).LessThanOrEqual(0);
        /// </code>
        /// </example>
        public static NullableOrConstraint<T> LessOrEqualThan<T>(this INullableAssertion<T> assertion, T? value)
            where T : struct, IComparable, IComparable<T>, IConvertible, IEquatable<T>, IFormattable
        {
            var context = assertion.Context();

            assertion.ThrowIfNull();
            assertion.Assert(() =>
            {
                var nullable = assertion.Value;
                if (!nullable.HasValue || !value.HasValue)
                {
                    return false;
                }
                return nullable.Value.CompareTo(value.Value) <= 0;
            }, assertion.ArgumentExceptionExpression(context.LessOrEqualThanMessage(value)));

            return new NullableOrConstraint<T>(assertion);
        }
    }
}