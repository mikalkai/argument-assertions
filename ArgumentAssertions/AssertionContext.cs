﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace ArgumentAssertions
{
    /// <summary>
    /// Base class for all assertions.
    /// </summary>
    public abstract class AssertionContext
    {
        protected AssertionContext(Condition condition, string parameterName)
        {
            if (parameterName == null)
            {
                throw new ArgumentNullException(nameof(parameterName));
            }
            Condition = condition;
            ParameterName = parameterName;
        }

        protected AssertionContext(Condition condition, Expression<Func<Exception>> exceptionExpression)
        {
            if (exceptionExpression == null)
            {
                throw new ArgumentNullException(nameof(exceptionExpression));
            }
            Condition = condition;
            ExceptionExpression = exceptionExpression;
        }

        /// <summary>
        /// Get current condition.
        /// </summary>
        public Condition Condition { get; }

        /// <summary>
        /// Get associated parameter name.
        /// <remarks>
        /// Is null when custom exception is used.
        /// </remarks>
        /// </summary>
        public string ParameterName { get; }

        /// <summary>
        /// Get associated expression that returns a custom exception.
        /// </summary>
        /// <remarks>
        /// Is null when property name is used.
        /// </remarks>
        public Expression<Func<Exception>> ExceptionExpression { get; }

        /// <summary>
        /// Get associated ignore functions.
        /// </summary>
        public ICollection<Func<bool>> IgnoreFuncs { get; } = new List<Func<bool>>();

        /// <summary>
        /// Compile associated expression in to exception.
        /// </summary>
        /// <param name="exceptionExpression">Exception expression.</param>
        /// <returns></returns>
        protected Exception CompileException(Expression<Func<Exception>> exceptionExpression)
        {
            var exception = exceptionExpression.Compile().Invoke();
            if (exception == null)
            {
                throw new NullExceptionReferenceException();
            }
            return exception;
        }
    }
}