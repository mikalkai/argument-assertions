﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq.Expressions;

namespace ArgumentAssertions
{
    [SuppressMessage("ReSharper", "TypeParameterCanBeVariant")]
    public interface INullableAssertion<T>
        where T : struct
    {
        /// <summary>
        /// Get value associated with the assertion.
        /// </summary>
        T? Value { get; }

        /// <summary>
        /// Assert function which returns a boolean result.
        /// </summary>
        /// <param name="evaluate">Evaluation function.</param>
        /// <param name="exceptionExpr">Expression where the exception is compiled.</param>
        /// <returns></returns>
        T? Assert(Func<bool> evaluate, Expression<Func<Exception>> exceptionExpr);

        /// <summary>
        /// Assert function which returns a boolean result.
        /// </summary>
        /// <param name="result">Result to be asserted.</param>
        /// <param name="exceptionExpr">Expression where the exception is compiled.</param>
        /// <returns></returns>
        T? Assert(bool result, Expression<Func<Exception>> exceptionExpr);
    }
}