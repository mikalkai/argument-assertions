﻿using System;
using System.Linq.Expressions;

namespace ArgumentAssertions
{
    /// <summary>
    /// Base class for a assertion bind with a type.
    /// </summary>
    /// <typeparam name="T">Object type.</typeparam>
    public abstract class Assertion<T> : AssertionContext, IAssertion<T>
    {
        protected Assertion(T value, Condition condition, string parameterName)
            : base(condition, parameterName)
        {
            Value = value;
        }

        protected Assertion(T value, Condition condition, Expression<Func<Exception>> exceptionExpression)
            : base(condition, exceptionExpression)
        {
            Value = value;
        }

        public T Value { get; }

        public abstract T Assert(Func<bool> evaluate, Expression<Func<Exception>> exceptionExpr);

        public abstract T Assert(bool result, Expression<Func<Exception>> exceptionExpr);
    }
}