﻿using System;

namespace ArgumentAssertions.Bool
{
    public static class BoolExceptionMessages
    {
        /// <summary>
        /// Format message for bool is true.
        /// </summary>
        /// <param name="context"></param>
        /// <returns>Exception message.</returns>
        public static string TrueMessage(this AssertionContext context)
        {
            var condition = context.Condition;
            var paramName = context.ParameterName;
            switch (condition)
            {
                case Condition.If:
                    return $"Parameter '{paramName}' should not be 'true'.";
                case Condition.IfNot:
                    return $"Parameter '{paramName}' should be 'true'.";
                default:
                    throw new ArgumentOutOfRangeException(nameof(condition), condition, null);
            }
        }

        /// <summary>
        /// Format message for bool is false.
        /// </summary>
        /// <param name="context"></param>
        /// <returns>Exception message.</returns>
        public static string FalseMessage(this AssertionContext context)
        {
            var condition = context.Condition;
            var paramName = context.ParameterName;
            switch (condition)
            {
                case Condition.If:
                    return $"Parameter '{paramName}' should not be 'false'.";
                case Condition.IfNot:
                    return $"Parameter '{paramName}' should be 'false'.";
                default:
                    throw new ArgumentOutOfRangeException(nameof(condition), condition, null);
            }
        }
    }
}