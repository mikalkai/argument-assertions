﻿using System;
using ArgumentAssertions.Generic.Internal;

namespace ArgumentAssertions.Bool
{
    public static class BoolTrue
    {
        /// <summary>
        /// Throw <see cref="ArgumentException"/> if boolean is not true.
        /// </summary>
        /// <param name="assertion"></param>
        /// <example>
        /// Below example throws ArgumentException with message "Parameter 'flag' should be 'true'."
        /// <code>
        /// var flag = false;
        /// flag.ThrowIfNot(() => flag).True();
        /// </code>
        /// </example>
        public static bool True(this IAssertion<bool> assertion)
        {
            var context = assertion.Context();
            return assertion.Assert(assertion.Value, assertion.ArgumentExceptionExpression(context.TrueMessage()));
        }

        /// <summary>
        /// Throw <see cref="ArgumentException"/> if boolean is not true.
        /// </summary>
        /// <param name="assertion"></param>
        /// <example>
        /// Below example throws ArgumentException with message "Parameter 'flag' should be 'true'."
        /// <code>
        /// bool? flag = false;
        /// flag.ThrowIfNot(() => flag).True();
        /// </code>
        /// </example>
        public static bool? True(this INullableAssertion<bool> assertion)
        {
            var context = assertion.Context();

            assertion.ThrowIfNull();
            return assertion.Assert(() =>
            {
                var b = assertion.Value;
                return b.HasValue && b.Value;
            }, assertion.ArgumentExceptionExpression(context.TrueMessage()));
        }
    }
}