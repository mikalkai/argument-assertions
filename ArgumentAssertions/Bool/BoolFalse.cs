﻿using System;
using ArgumentAssertions.Generic.Internal;

namespace ArgumentAssertions.Bool
{
    public static class BoolFalse
    {
        /// <summary>
        /// Throw <see cref="ArgumentException"/> if boolean is false.
        /// </summary>
        /// <param name="assertion"></param>
        /// <example>
        /// Below example throws ArgumentException with message "Parameter 'flag' should be 'False'."
        /// <code>
        /// var flag = false;
        /// flag.ThrowIfNot(() => flag).False();
        /// </code>
        /// </example>
        public static bool False(this IAssertion<bool> assertion)
        {
            var context = assertion.Context();

            return assertion.Assert(!assertion.Value, assertion.ArgumentExceptionExpression(context.FalseMessage()));
        }

        /// <summary>
        /// Throw <see cref="ArgumentException"/> if boolean is false.
        /// </summary>
        /// <example>
        /// Below example throws ArgumentException with message "Parameter 'flag' should be 'False'."
        /// <code>
        /// bool? flag = false;
        /// flag.ThrowIfNot(() => flag).False();
        /// </code>
        /// </example>
        public static bool? False(this INullableAssertion<bool> assertion)
        {
            var context = assertion.Context();

            assertion.ThrowIfNull();
            return assertion.Assert(() =>
            {
                var b = assertion.Value;
                return b.HasValue && !b.Value;
            }, assertion.ArgumentExceptionExpression(context.FalseMessage()));
        }
    }
}