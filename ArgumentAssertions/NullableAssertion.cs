﻿using System;
using System.Linq.Expressions;

namespace ArgumentAssertions
{
    public abstract class NullableAssertion<T> : AssertionContext, INullableAssertion<T>
        where T : struct
    {
        protected NullableAssertion(T? value, Condition condition, string parameterName)
            : base(condition, parameterName)
        {
            Value = value;
        }

        protected NullableAssertion(T? value, Condition condition, Expression<Func<Exception>> exceptionExpression)
            : base(condition, exceptionExpression)
        {
            Value = value;
        }

        public T? Value { get; }

        public abstract T? Assert(Func<bool> evaluate, Expression<Func<Exception>> exceptionExpr);

        public abstract T? Assert(bool result, Expression<Func<Exception>> exceptionExpr);
    }
}