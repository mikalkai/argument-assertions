﻿using System;

namespace ArgumentAssertions.Generic
{
    public static class GenericExceptionMessages
    {
        /// <summary>
        /// Format a generic null message.
        /// </summary>
        /// <param name="context"></param>
        /// <returns>Error message.</returns>
        public static string NullMessage(this AssertionContext context)
        {
            var condition = context.Condition;
            var paramName = context.ParameterName;
            switch (condition)
            {
                case Condition.If:
                    return $"Parameter '{paramName}' should not be null.";
                case Condition.IfNot:
                    return $"Parameter '{paramName}' should be null.";
                default:
                    throw new ArgumentOutOfRangeException(nameof(condition), condition, null);
            }
        }

        /// <summary>
        /// Format a generic equal to message.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="obj">Object to compare.</param>
        /// <returns>Error message.</returns>
        public static string EqualToMessage(this AssertionContext context, object obj)
        {
            var condition = context.Condition;
            var paramName = context.ParameterName;
            switch (condition)
            {
                case Condition.If:
                    return $"Parameter '{paramName}' should not be equal to '{obj}'.";
                case Condition.IfNot:
                    return $"Parameter '{paramName}' should be equal to '{obj}'.";
                default:
                    throw new ArgumentOutOfRangeException(nameof(condition), condition, null);
            }
        }

        /// <summary>
        /// Format a generic valid message.
        /// </summary>
        /// <param name="context"></param>
        /// <returns>Error message.</returns>
        public static string ValidMessage(this AssertionContext context)
        {
            var condition = context.Condition;
            var paramName = context.ParameterName;
            switch (condition)
            {
                case Condition.If:
                    return $"Parameter '{paramName}' should not be valid.";
                case Condition.IfNot:
                    return $"Parameter '{paramName}' should be valid.";
                default:
                    throw new ArgumentOutOfRangeException(nameof(condition), condition, null);
            }
        }
    }
}