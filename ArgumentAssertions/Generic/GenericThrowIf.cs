﻿using System;
using System.Linq.Expressions;

namespace ArgumentAssertions.Generic
{
    public static class GenericThrowIf
    {
        /// <summary>
        /// Create an assertion bind to a generic class type.
        /// </summary>
        /// <typeparam name="T">Type of asserted object.</typeparam>
        /// <param name="t"></param>
        /// <param name="propertyExpression">Expression from which the property name is resolved.</param>
        /// <returns>The assertion.</returns>
        /// <remarks>In case property expression is null, property returned is null or property cannot be resolved 
        /// the parameter name is resolved to 'unedfined'.
        /// </remarks>
        /// <example>
        /// Below example creates an IfAssertion which can be asserted:
        /// <code>
        /// var a = new A();
        /// a.ThrowIf(() => a);
        /// </code>
        /// </example>
        public static IAssertion<T> ThrowIf<T>(this T t, Expression<Func<T>> propertyExpression)
        {
            return new IfAssertion<T>(t, propertyExpression.ResolvePropertyPath());
        }

        /// <summary>
        /// Create an assertion bind to a generic class type.
        /// </summary>
        /// <typeparam name="T">Type of asserted object.</typeparam>
        /// <param name="t"></param>
        /// <param name="paramName">Name for the parameter used in exception.</param>
        /// <returns>The assertion.</returns>
        public static IAssertion<T> ThrowIf<T>(this T t, string paramName)
        {
            return new IfAssertion<T>(t, paramName);
        }

        /// <summary>
        /// Create an assertion bind to a generic class type.
        /// </summary>
        /// <typeparam name="T">Type of asserted object.</typeparam>
        /// <param name="t"></param>
        /// <param name="exceptionExpr">Expression that returns a custom exception.</param>
        /// <returns>The assertion.</returns>
        public static IAssertion<T> ThrowIf<T>(this T t, Expression<Func<Exception>> exceptionExpr)
        {
            return new IfAssertion<T>(t, exceptionExpr);
        }

        /// <summary>
        /// Create an assertion bind to a nullable type.
        /// </summary>
        /// <typeparam name="T">Type of asserted object.</typeparam>
        /// <param name="t"></param>
        /// <param name="propertyExpression">Expression from which the property name is resolved.</param>
        /// <returns>The assertion.</returns>
        public static INullableAssertion<T> ThrowIf<T>(this T? t, Expression<Func<T?>> propertyExpression)
            where T : struct
        {
            return new NullableIfAssertion<T>(t, propertyExpression.ResolvePropertyPath());
        }

        /// <summary>
        /// Create an assertion bind to a nullable type.
        /// </summary>
        /// <typeparam name="T">Type of asserted object.</typeparam>
        /// <param name="t"></param>
        /// <param name="paramName">Name for the parameter used in exception.</param>
        /// <returns>The assertion.</returns>
        /// <remarks>In case property expression is null, property returned is null or property cannot be resolved 
        /// the parameter name is resolved to 'unedfined'.
        /// </remarks>
        /// <example>
        /// Below example creates an IfAssertion which can be asserted:
        /// <code>
        /// var a = new A();
        /// a.ThrowIf(() => a);
        /// </code>
        /// </example>
        public static INullableAssertion<T> ThrowIf<T>(this T? t, string paramName)
            where T : struct 
        {
            return new NullableIfAssertion<T>(t, paramName);
        }

        /// <summary>
        /// Create an assertion bind to a nullable type.
        /// </summary>
        /// <typeparam name="T">Type of asserted object.</typeparam>
        /// <param name="t"></param>
        /// <param name="exceptionExpr">Expression that returns a custom exception.</param>
        /// <returns>The assertion.</returns>
        public static INullableAssertion<T> ThrowIf<T>(this T? t, Expression<Func<Exception>> exceptionExpr) 
            where T : struct
        {
            return new NullableIfAssertion<T>(t, exceptionExpr);
        }
    }
}