﻿namespace ArgumentAssertions.Generic.Internal
{
    internal static class GenericThrowIfNull
    {
        internal static OrConstraint<T> ThrowIfNull<T>(this IAssertion<T> assertion)
            where T : class
        {
            var context = assertion.Context();
            (context.ExceptionExpression != null
                ? assertion.Value.ThrowIf(context.ExceptionExpression)
                : assertion.Value.ThrowIf(context.ParameterName)).Null();

            return new OrConstraint<T>(assertion);
        }

        internal static NullableOrConstraint<T> ThrowIfNull<T>(this INullableAssertion<T> assertion)
            where T : struct
        {
            var context = assertion.Context();
            (context.ExceptionExpression != null
                ? assertion.Value.ThrowIf(context.ExceptionExpression)
                : assertion.Value.ThrowIf(context.ParameterName)).Null();

            return new NullableOrConstraint<T>(assertion);
        }
    }
}