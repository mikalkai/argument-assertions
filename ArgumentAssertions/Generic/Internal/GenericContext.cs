﻿namespace ArgumentAssertions.Generic.Internal
{
    public static class GenericContext
    {
        /// <summary>
        /// Cast assertion interface in to assertion context.
        /// </summary>
        /// <typeparam name="T">Type of object.</typeparam>
        /// <param name="assertion"></param>
        /// <returns>Assertion context.</returns>
        public static AssertionContext Context<T>(this IAssertion<T> assertion)
        {
            return (AssertionContext) assertion;
        }

        /// <summary>
        /// Cast assertion interface in to assertion context.
        /// </summary>
        /// <typeparam name="T">Type of object.</typeparam>
        /// <param name="assertion"></param>
        /// <returns>Assertion context.</returns>
        public static AssertionContext Context<T>(this INullableAssertion<T> assertion) 
            where T : struct
        {
            return (AssertionContext) assertion;
        }
    }
}