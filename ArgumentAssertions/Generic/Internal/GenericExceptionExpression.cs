﻿using System;
using System.Linq.Expressions;

namespace ArgumentAssertions.Generic.Internal
{
    public static class GenericExceptionExpression
    {
        /// <summary>
        /// Resolve argument expression from assertion.
        /// </summary>
        /// <typeparam name="T">TYpe of object.</typeparam>
        /// <param name="assertion"></param>
        /// <param name="message">Message used in exception.</param>
        /// <returns>Exception expression.</returns>
        public static Expression<Func<Exception>> ArgumentExceptionExpression<T>(this IAssertion<T> assertion, string message)
        {
            var assertionContext = assertion.Context();
            return assertionContext.ExceptionExpression ??
                (() => new ArgumentException(message, assertionContext.ParameterName));
        }

        /// <summary>
        /// Resolve argument null expression from assertion.
        /// </summary>
        /// <typeparam name="T">TYpe of object.</typeparam>
        /// <param name="assertion"></param>
        /// <param name="message">Message used in exception.</param>
        /// <returns>Exception expression.</returns>
        public static Expression<Func<Exception>> ArgumentNullExceptionExpression<T>(this IAssertion<T> assertion, string message)
        {
            var assertionContext = assertion.Context();
            return assertionContext.ExceptionExpression ??
                (() => new ArgumentNullException(assertionContext.ParameterName, message));
        }

        /// <summary>
        /// Resolve argument expression from assertion.
        /// </summary>
        /// <typeparam name="T">TYpe of object.</typeparam>
        /// <param name="assertion"></param>
        /// <param name="message">Message used in exception.</param>
        /// <returns>Exception expression.</returns>
        public static Expression<Func<Exception>> ArgumentExceptionExpression<T>(this INullableAssertion<T> assertion, string message)
            where T : struct
        {
            var assertionContext = assertion.Context();
            return assertionContext.ExceptionExpression ??
                (() => new ArgumentException(message, assertionContext.ParameterName));
        }

        /// <summary>
        /// Resolve argument null expression from assertion.
        /// </summary>
        /// <typeparam name="T">TYpe of object.</typeparam>
        /// <param name="assertion"></param>
        /// <param name="message">Message used in exception.</param>
        /// <returns>Exception expression.</returns>
        public static Expression<Func<Exception>> ArgumentNullExceptionExpression<T>(this INullableAssertion<T> assertion, string message) 
            where T : struct
        {
            var assertionContext = assertion.Context();
            return assertionContext.ExceptionExpression ??
                (() => new ArgumentNullException(assertionContext.ParameterName, message));
        }
    }
}