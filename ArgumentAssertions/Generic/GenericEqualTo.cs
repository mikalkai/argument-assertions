﻿using System;
using ArgumentAssertions.Generic.Internal;

namespace ArgumentAssertions.Generic
{
    public static class GenericEqualTo
    {
        /// <summary>
        /// Throw <see cref="ArgumentException"/> if the object is equal to other object.
        /// </summary>
        /// <typeparam name="T">Type of object.</typeparam>
        /// <param name="assertion"></param>
        /// <param name="t">Compared object.</param>
        /// <example>
        /// Below example throws ArgumentException with message "Parameter 'a' should not be equal to '{b}'."
        /// <code>
        /// var a = new A();
        /// var b = a;
        /// a.ThrowIf(() => a).EqualTo(b);
        /// </code>
        /// </example>
        public static OrConstraint<T> EqualTo<T>(this IAssertion<T> assertion, T t)
        {
            var context = assertion.Context();

            assertion.Assert(Equals(assertion.Value, t),
                assertion.ArgumentExceptionExpression(context.EqualToMessage(t)));

            return new OrConstraint<T>(assertion);
        }

        /// <summary>
        /// Throw <see cref="ArgumentException"/> if the nullable type is equal to other nullable type.
        /// </summary>
        /// <typeparam name="T">Nullable type.</typeparam>
        /// <param name="assertion"></param>
        /// <param name="t">Compared nullable type.</param>
        /// <example>
        /// Below example throws ArgumentException with message "Parameter 'number' should not be equal to '2'."
        /// <code>
        /// int? number = 2;
        /// number.ThrowIf(() => number).EqualTo(2);
        /// </code>
        /// </example>
        public static NullableOrConstraint<T> EqualTo<T>(this INullableAssertion<T> assertion, T? t)
            where T : struct
        {
            var context = assertion.Context();

            assertion.Assert(Equals(assertion.Value, t),
                assertion.ArgumentExceptionExpression(context.EqualToMessage(t)));

            return new NullableOrConstraint<T>(assertion);
        }
    }
}