﻿using System;
using ArgumentAssertions.Generic.Internal;

namespace ArgumentAssertions.Generic
{
    public static class GenericValid
    {
        /// <summary>
        /// Throw <see cref="ArgumentException"/> if the custom validation fails.
        /// </summary>
        /// <typeparam name="T">Type of object.</typeparam>
        /// <param name="assertion"></param>
        /// <param name="func">Custom validation function.</param>
        /// <returns>Or constraint.</returns>
        /// <example>
        /// Below example throws ArgumentException with message "Parameter 'a' should be valid."
        /// <code>
        /// var a = new A { IsValid = false };
        /// a.ThrowIfNot(() => a).Valid(arg => arg.IsValid);
        /// </code>
        /// </example>
        public static OrConstraint<T> Valid<T>(this IAssertion<T> assertion, Func<T, bool> func)
        {
            var context = assertion.Context();

            assertion.Assert(func(assertion.Value),
                assertion.ArgumentExceptionExpression(context.ValidMessage()));

            return new OrConstraint<T>(assertion);
        }

        /// <summary>
        /// Throw <see cref="ArgumentException"/> if the custom validation fails.
        /// </summary>
        /// <typeparam name="T">Type of object.</typeparam>
        /// <param name="assertion"></param>
        /// <param name="func">Custom validation function.</param>
        /// <param name="message">Message bind with exception.</param>
        /// <returns>Or constraint.</returns>
        public static OrConstraint<T> Valid<T>(this IAssertion<T> assertion, Func<T, bool> func, string message)
        {
            assertion.Assert(func(assertion.Value),
                assertion.ArgumentExceptionExpression(message));

            return new OrConstraint<T>(assertion);
        }

        /// <summary>
        /// Throw <see cref="ArgumentException"/> if the custom validation fails.
        /// </summary>
        /// <typeparam name="T">Type of object.</typeparam>
        /// <param name="assertion"></param>
        /// <param name="func">Custom validation function.</param>
        /// <example>
        /// Below example throws CustomException.
        /// <code>
        /// DateTime? dateTime = DateTime.Now.AddDays(-1);
        /// a.ThrowIfNot().Valid(arg => arg != DateTime.Now, () => new CustomException());
        /// </code>
        /// </example>
        public static NullableOrConstraint<T> Valid<T>(this INullableAssertion<T> assertion, Func<T?, bool> func)
            where T : struct
        {
            var context = assertion.Context();

            assertion.Assert(func(assertion.Value),
                assertion.ArgumentExceptionExpression(context.ValidMessage()));

            return new NullableOrConstraint<T>(assertion);
        }

        /// <summary>
        /// Throw <see cref="ArgumentException"/> if the custom validation fails.
        /// </summary>
        /// <typeparam name="T">Type of object.</typeparam>
        /// <param name="assertion"></param>
        /// <param name="func">Custom validation function.</param>
        /// <param name="message">Message bind with exception.</param>
        public static NullableOrConstraint<T> Valid<T>(this INullableAssertion<T> assertion, Func<T?, bool> func, string message)
            where T : struct
        {
            assertion.Assert(func(assertion.Value),
                assertion.ArgumentExceptionExpression(message));

            return new NullableOrConstraint<T>(assertion);
        }
    }
}