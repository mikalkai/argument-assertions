﻿using System;
using ArgumentAssertions.Generic.Internal;

namespace ArgumentAssertions.Generic
{
    public static class GenericNull
    {
        /// <summary>
        /// Throw <see cref="ArgumentNullException"/> if the object is null.
        /// </summary>
        /// <typeparam name="T">Type of object.</typeparam>
        /// <param name="assertion"></param>
        /// <returns>Or constraint.</returns>
        /// <example>
        /// Below example throws ArgumentNullException with message "Parameter 'a.Bb.Cc' should not be null."
        /// <code>
        /// var a = new A
        /// {
        ///     Bb = new B 
        ///     { 
        ///         Cc = null 
        ///     } 
        /// };
        /// a.ThrowIf(() => a).Null();
        /// a.Bb.ThrowIf(() => a.Bb).Null();
        /// a.Bb.Cc.ThrowIf(() => a.Bb.Cc).Null();
        /// </code>
        /// </example>
        public static OrConstraint<T> Null<T>(this IAssertion<T> assertion)
            where T : class
        {
            var context = assertion.Context();

            assertion.Assert(Equals(assertion.Value, default(T)),
                assertion.ArgumentNullExceptionExpression(context.NullMessage()));

            return new OrConstraint<T>(assertion);
        }

        /// <summary>
        /// Throw <see cref="ArgumentNullException"/> if the nullable type has no value.
        /// </summary>
        /// <typeparam name="T">Nullable type.</typeparam>
        /// <param name="assertion"></param>
        /// <returns>Or constraint.</returns>
        /// <example>
        /// Below example throws ArgumentNullException with message "Parameter 'a.Bb.Cc.DateTime' should not be null."
        /// <code>
        /// var a = new A
        /// {
        ///     Bb = new B 
        ///     { 
        ///         Cc = new C
        ///         {
        ///             DateTime = null
        ///         }
        ///     } 
        /// };
        /// a.ThrowIf(() => a).Null();
        /// a.Bb.ThrowIf(() => a.Bb).Null();
        /// a.Bb.Cc.ThrowIf(() => a.Bb.Cc).Null();
        /// a.Bb.Cc.DateTime.ThrowIf(() => a.Bb.Cc.DateTime).Null();
        /// </code>
        /// </example>
        public static NullableOrConstraint<T> Null<T>(this INullableAssertion<T> assertion)
            where T : struct
        {
            var context = assertion.Context();

            assertion.Assert(!assertion.Value.HasValue,
                assertion.ArgumentNullExceptionExpression(context.NullMessage()));

            return new NullableOrConstraint<T>(assertion);
        }
    }
}