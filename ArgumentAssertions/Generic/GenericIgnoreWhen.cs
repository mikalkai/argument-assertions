﻿using System;
using ArgumentAssertions.Generic.Internal;

namespace ArgumentAssertions.Generic
{
    public static class GenericIgnoreWhen
    {
        /// <summary>
        /// Ignore assertion if the condition matches.
        /// </summary>
        /// <typeparam name="T">Type of asserted object.</typeparam>
        /// <param name="assertion"></param>
        /// <param name="ignoreFunc"></param>
        /// <returns></returns>
        public static IAssertion<T> IgnoreWhen<T>(this IAssertion<T> assertion, Func<bool> ignoreFunc)
        {
            var assertionContext = assertion.Context();
            assertionContext.IgnoreFuncs.Add(ignoreFunc);
            return assertion;            
        }

        /// <summary>
        /// Ignore assertion if the condition matches.
        /// </summary>
        /// <typeparam name="T">Type of asserted object.</typeparam>
        /// <param name="assertion"></param>
        /// <param name="ignoreFunc"></param>
        /// <returns></returns>
        public static INullableAssertion<T> IgnoreWhen<T>(this INullableAssertion<T> assertion, Func<bool> ignoreFunc) 
            where T : struct
        {
            var assertionContext = assertion.Context();
            assertionContext.IgnoreFuncs.Add(ignoreFunc);
            return assertion;
        }
    }
}