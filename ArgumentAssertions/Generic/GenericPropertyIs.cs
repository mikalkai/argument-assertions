﻿using System;
using System.Linq.Expressions;
using ArgumentAssertions.Generic.Internal;

namespace ArgumentAssertions.Generic
{
    public static class GenericPropertyIs
    {
        /// <summary>
        /// Navigate the asserted object properties to create a new assertion.
        /// </summary>
        /// <typeparam name="T1">Type of asserted object.</typeparam>
        /// <typeparam name="T2">Type of asserted object property.</typeparam>
        /// <param name="assertion"></param>
        /// <param name="expression">Property expression.</param>
        /// <returns>Assertion bind to the navigated property.</returns>
        public static IAssertion<T2> PropertyIs<T1, T2>(this IAssertion<T1> assertion, Expression<Func<T1, T2>> expression)
        {
            var context = assertion.Context();
            var value = expression.Compile().Invoke(assertion.Value);
            switch (context.Condition)
            {
                case Condition.If:
                    return context.ExceptionExpression != null
                        ? new IfAssertion<T2>(value, context.ExceptionExpression)
                        : new IfAssertion<T2>(value, $"{context.ParameterName}.{expression.ResolvePropertyPath()}");
                case Condition.IfNot:
                    return context.ExceptionExpression != null
                        ? new IfNotAssertion<T2>(value, context.ExceptionExpression)
                        : new IfNotAssertion<T2>(value, $"{context.ParameterName}.{expression.ResolvePropertyPath()}");
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        /// <summary>
        /// Navigate the asserted object properties to create a new assertion.
        /// </summary>
        /// <typeparam name="T1">Type of asserted object.</typeparam>
        /// <typeparam name="T2">Type of asserted nullable object property.</typeparam>
        /// <param name="assertion"></param>
        /// <param name="expression">Property expression.</param>
        /// <returns>Assertion bind to the navigated property.</returns>
        public static INullableAssertion<T2> PropertyIs<T1, T2>(this IAssertion<T1> assertion, Expression<Func<T1, T2?>> expression)
            where T2 : struct
        {
            var context = assertion.Context();
            var value = expression.Compile().Invoke(assertion.Value);
            switch (context.Condition)
            {
                case Condition.If:
                    return context.ExceptionExpression != null
                        ? new NullableIfAssertion<T2>(value, context.ExceptionExpression)
                        : new NullableIfAssertion<T2>(value, $"{context.ParameterName}.{expression.ResolvePropertyPath()}");
                case Condition.IfNot:
                    return context.ExceptionExpression != null
                        ? new NullableIfNotAssertion<T2>(value, context.ExceptionExpression)
                        : new NullableIfNotAssertion<T2>(value, $"{context.ParameterName}.{expression.ResolvePropertyPath()}");
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        /// <summary>
        /// Navigate the asserted object properties to create a new assertion.
        /// </summary>
        /// <typeparam name="T1">Type of asserted object.</typeparam>
        /// <typeparam name="T2">Type of asserted nullable object property.</typeparam>
        /// <param name="assertion"></param>
        /// <param name="expression">Property expression.</param>
        /// <returns>Assertion bind to the navigated property.</returns>
        public static IAssertion<T2> PropertyIs<T1, T2>(this INullableAssertion<T1> assertion, Expression<Func<T1?, T2>> expression)
            where T1 : struct
        {
            var context = assertion.Context();
            var value = expression.Compile().Invoke(assertion.Value);
            switch (context.Condition)
            {
                case Condition.If:
                    return context.ExceptionExpression != null
                        ? new IfAssertion<T2>(value, context.ExceptionExpression)
                        : new IfAssertion<T2>(value, $"{context.ParameterName}.{expression.ResolvePropertyPath()}");
                case Condition.IfNot:
                    return context.ExceptionExpression != null
                        ? new IfNotAssertion<T2>(value, context.ExceptionExpression)
                        : new IfNotAssertion<T2>(value, $"{context.ParameterName}.{expression.ResolvePropertyPath()}");
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        /// <summary>
        /// Navigate the asserted object properties to create a new context.
        /// </summary>
        /// <typeparam name="T1">Type of asserted object.</typeparam>
        /// <typeparam name="T2">Type of asserted nullable object property.</typeparam>
        /// <param name="assertion"></param>
        /// <param name="expression">Property expression.</param>
        /// <returns>Assertion bind to the navigated property.</returns>
        public static INullableAssertion<T2> PropertyIs<T1, T2>(this INullableAssertion<T1> assertion, Expression<Func<T1?, T2?>> expression)
            where T1 : struct
            where T2 : struct 
        {
            var context = assertion.Context();
            var value = expression.Compile().Invoke(assertion.Value);
            switch (context.Condition)
            {
                case Condition.If:
                    return context.ExceptionExpression != null
                        ? new NullableIfAssertion<T2>(value, context.ExceptionExpression)
                        : new NullableIfAssertion<T2>(value, $"{context.ParameterName}.{expression.ResolvePropertyPath()}");
                case Condition.IfNot:
                    return context.ExceptionExpression != null
                        ? new NullableIfNotAssertion<T2>(value, context.ExceptionExpression)
                        : new NullableIfNotAssertion<T2>(value, $"{context.ParameterName}.{expression.ResolvePropertyPath()}");
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        /// <summary>
        /// Navigate the asserted object properties to create a new assertion.
        /// </summary>
        /// <typeparam name="T1">Type of asserted object.</typeparam>
        /// <typeparam name="T2">Type of asserted object property.</typeparam>
        /// <param name="assertion"></param>
        /// <param name="expression">Property expression.</param>
        /// <param name="paramName">Explicit name for the parameter.</param>
        /// <returns>Assertion bind to the navigated property.</returns>
        public static IAssertion<T2> PropertyIs<T1, T2>(this IAssertion<T1> assertion, Expression<Func<T1, T2>> expression, string paramName)
        {
            var assertionContext = assertion.Context();
            var value = expression.Compile().Invoke(assertion.Value);
            switch (assertionContext.Condition)
            {
                case Condition.If:
                    return assertionContext.ExceptionExpression != null
                        ? new IfAssertion<T2>(value, assertionContext.ExceptionExpression)
                        : new IfAssertion<T2>(value, $"{assertionContext.ParameterName}.{paramName}");
                case Condition.IfNot:
                    return assertionContext.ExceptionExpression != null
                        ? new IfNotAssertion<T2>(value, assertionContext.ExceptionExpression)
                        : new IfNotAssertion<T2>(value, $"{assertionContext.ParameterName}.{paramName}");
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        /// <summary>
        /// Navigate the asserted object properties to create a new assertion.
        /// </summary>
        /// <typeparam name="T1">Type of asserted object.</typeparam>
        /// <typeparam name="T2">Type of asserted nullable object property.</typeparam>
        /// <param name="assertion"></param>
        /// <param name="expression">Property expression.</param>
        /// <param name="paramName">Explicit name for the parameter.</param>
        /// <returns>Assertion bind to the navigated property.</returns>
        public static INullableAssertion<T2> PropertyIs<T1, T2>(this IAssertion<T1> assertion, Expression<Func<T1, T2?>> expression, string paramName)
            where T2 : struct
        {
            var assertionContext = assertion.Context();
            var value = expression.Compile().Invoke(assertion.Value);
            switch (assertionContext.Condition)
            {
                case Condition.If:
                    return assertionContext.ExceptionExpression != null
                        ? new NullableIfAssertion<T2>(value, assertionContext.ExceptionExpression)
                        : new NullableIfAssertion<T2>(value, $"{assertionContext.ParameterName}.{paramName}");
                case Condition.IfNot:
                    return assertionContext.ExceptionExpression != null
                        ? new NullableIfNotAssertion<T2>(value, assertionContext.ExceptionExpression)
                        : new NullableIfNotAssertion<T2>(value, $"{assertionContext.ParameterName}.{paramName}");
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        /// <summary>
        /// Navigate the asserted object properties to create a new assertion.
        /// </summary>
        /// <typeparam name="T1">Type of asserted object.</typeparam>
        /// <typeparam name="T2">Type of asserted nullable object property.</typeparam>
        /// <param name="assertion"></param>
        /// <param name="expression">Property expression.</param>
        /// <param name="paramName">Explicit name for the parameter.</param>
        /// <returns>Assertion bind to the navigated property.</returns>
        public static IAssertion<T2> PropertyIs<T1, T2>(this INullableAssertion<T1> assertion, Expression<Func<T1?, T2>> expression, string paramName)
            where T1 : struct
        {
            var context = assertion.Context();
            var value = expression.Compile().Invoke(assertion.Value);
            switch (context.Condition)
            {
                case Condition.If:
                    return context.ExceptionExpression != null
                        ? new IfAssertion<T2>(value, context.ExceptionExpression)
                        : new IfAssertion<T2>(value, $"{context.ParameterName}.{paramName}");
                case Condition.IfNot:
                    return context.ExceptionExpression != null
                        ? new IfNotAssertion<T2>(value, context.ExceptionExpression)
                        : new IfNotAssertion<T2>(value, $"{context.ParameterName}.{paramName}");
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        /// <summary>
        /// Navigate the asserted object properties to create a new assertion.
        /// </summary>
        /// <typeparam name="T1">Type of asserted object.</typeparam>
        /// <typeparam name="T2">Type of asserted nullable object property.</typeparam>
        /// <param name="assertion"></param>
        /// <param name="expression">Property expression.</param>
        /// <param name="paramName">Explicit name for the parameter.</param>
        /// <returns>Assertion bind to the navigated property.</returns>
        public static INullableAssertion<T2> PropertyIs<T1, T2>(this INullableAssertion<T1> assertion, Expression<Func<T1?, T2?>> expression, string paramName)
            where T1 : struct
            where T2 : struct
        {
            var context = assertion.Context();
            var value = expression.Compile().Invoke(assertion.Value);
            switch (context.Condition)
            {
                case Condition.If:
                    return context.ExceptionExpression != null
                        ? new NullableIfAssertion<T2>(value, context.ExceptionExpression)
                        : new NullableIfAssertion<T2>(value, $"{context.ParameterName}.{paramName}");
                case Condition.IfNot:
                    return context.ExceptionExpression != null
                        ? new NullableIfNotAssertion<T2>(value, context.ExceptionExpression)
                        : new NullableIfNotAssertion<T2>(value, $"{context.ParameterName}.{paramName}");
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}