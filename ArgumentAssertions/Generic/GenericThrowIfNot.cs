﻿using System;
using System.Linq.Expressions;

namespace ArgumentAssertions.Generic
{
    public static class GenericThrowIfNot
    {
        /// <summary>
        /// Create an assertion bind to a generic class type.
        /// </summary>
        /// <typeparam name="T">Type of asserted object.</typeparam>
        /// <param name="t"></param>
        /// <param name="propertyExpression">Expression from which the property name is resolved.</param>
        /// <returns>The assertion.</returns>
        public static IAssertion<T> ThrowIfNot<T>(this T t, Expression<Func<T>> propertyExpression)
        {
            return new IfNotAssertion<T>(t, propertyExpression.ResolvePropertyPath());
        }

        /// <summary>
        /// Create an assertion bind to a generic class type.
        /// </summary>
        /// <typeparam name="T">Type of asserted object.</typeparam>
        /// <param name="t"></param>
        /// <param name="paramName">Name for the parameter used in exception.</param>
        /// <returns>The assertion.</returns>
        public static IAssertion<T> ThrowIfNot<T>(this T t, string paramName)
        {
            return new IfNotAssertion<T>(t, paramName);
        }

        /// <summary>
        /// Create an assertion bind to a generic class type.
        /// </summary>
        /// <typeparam name="T">Type of asserted object.</typeparam>
        /// <param name="t"></param>
        /// <param name="exceptionExpr">Expression that returns a custom exception.</param>
        /// <returns>The assertion.</returns>
        public static IAssertion<T> ThrowIfNot<T>(this T t, Expression<Func<Exception>> exceptionExpr)
        {
            return new IfNotAssertion<T>(t, exceptionExpr);
        }

        /// <summary>
        /// Create an assertion bind to a nullable type.
        /// </summary>
        /// <typeparam name="T">Type of asserted object.</typeparam>
        /// <param name="t"></param>
        /// <param name="propertyExpression">Expression from which the property name is resolved.</param>
        /// <returns>The assertion.</returns>
        public static INullableAssertion<T> ThrowIfNot<T>(this T? t, Expression<Func<T?>> propertyExpression)
            where T : struct
        {
            return new NullableIfNotAssertion<T>(t, propertyExpression.ResolvePropertyPath());
        }

        /// <summary>
        /// Create an assertion bind to a nullable type.
        /// </summary>
        /// <typeparam name="T">Type of asserted object.</typeparam>
        /// <param name="t"></param>
        /// <param name="paramName">Name for the parameter used in exception.</param>
        /// <returns>The assertion.</returns>
        public static INullableAssertion<T> ThrowIfNot<T>(this T? t, string paramName)
            where T : struct
        {
            return new NullableIfNotAssertion<T>(t, paramName);
        }

        /// <summary>
        /// Create an assertion bind to a nullable type.
        /// </summary>
        /// <typeparam name="T">Type of asserted object.</typeparam>
        /// <param name="t"></param>
        /// <param name="exceptionExpr">Expression that returns a custom exception.</param>
        /// <returns>The assertion.</returns>
        public static INullableAssertion<T> ThrowIfNot<T>(this T? t, Expression<Func<Exception>> exceptionExpr)
            where T : struct
        {
            return new NullableIfNotAssertion<T>(t, exceptionExpr);
        }
    }
}