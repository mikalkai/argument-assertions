#------------------------------------------------------------
# Clean old nugets, build solution in release configuration,
# pack nugets and push.
#------------------------------------------------------------

# Parameters
Param (
	[switch]$push
)

# Definitions

$MSBuild = "C:\Program Files (x86)\MSBuild\14.0\Bin\MsBuild.exe"

# 1. Cleanup

del *.nupkg

# 2. Build solution

& $MSBuild /p:Configuration=Release

# 3. Pack nugets

Get-ChildItem "." -Recurse -Filter *.nuspec | ForEach-Object {
	$CSProj = $_.FullName -replace "nuspec","csproj"
	nuget pack $CSProj -Symbols -Properties Configuration=Release 
}

# 4. Delete packages without symbols

Get-ChildItem "." -Filter *.nupkg | Where-Object {
	$_.FullName -inotlike "*symbols*" } | ForEach-Object { 
		Remove-Item $_.FullName
	}

# 5. Rename symbol packages

Get-ChildItem "." -Filter *.nupkg | ForEach-Object { 
	$NewFile = $_.FullName -replace ".symbols",""
	Rename-Item -path $_.FullName -newname $NewFile
}

# 6. Push new packages

if ($push) {
	Get-ChildItem "." -Filter *.nupkg | ForEach-Object {
		nuget push $_
	}
}
