# Nuget #

[http://www.nuget.org/packages/ArgumentAssertions/](http://www.nuget.org/packages/ArgumentAssertions/)

# Version #

1.1.2

# Introduction #
Every professional product or a piece of software should have proper argument validation against errorneous implementation, malicious requests, etc.

Consider if the following code:

```
#!c#
public class Foo
{
    private readonly string _bar;

    public Foo(string bar)
    {
        if (bar == null)
        {
            throw new ArgumentNullException(bar, "Parameter 'bar' should not be null.");
        }
        if (bar.Length == 0)
        {
            throw new ArgumentException("Parameter 'bar' should not be empty.", bar);
        }
        _bar = bar;
    }
}
```

Intent is good but the amount of boiler plate is huge! Consider refactoring the name of the parameters. How many times you forget to change the name also in the exception message?

What if you could validate the above argument just by telling the argument in question to throw if it is null or empty:

```
#!c#
public class Foo
{
    private readonly string _bar;

    public Foo(string bar)
    {
        _bar = bar.ThrowIf(() => bar).Null.OrIf.Empty();
    }
}
```

Well now you can! Inspired by the magnificent [Fluent Assertions](http://www.fluentassertions.com/), Argument Assertions provides a fluent, easy to use API with strong compile time type checking. It has support for generic assertions of any type, numeric assertions, string assertions, etc. It also supports basic If and IfNot clauses. The default notation can be easily extended with your own custom assertions.

# Key Features #

To start with you can call **ThrowIf()** or **ThrowIfNot()** on any type. This will create an Assertion object which has different extensions based on the type in question:

```
#!c#
var number = 0;
number.ThrowIf(() => number);
number.ThrowIfNot(() => number);
```

The assertion object returned is either Assertion<T> or NullableAssertion<T> depending on the type the call was made. Every assertion has three different overloads which are used to resolve what kind of exception is thrown and what is the parameter name.

## Resolve parameter name from expression ##

Parameter name used in an ArgumentException can be resolved form an expression. This so called property path resolving looks up the whole member expression tree and constructs the parameter name from it.

The below code will throw ArgumentException with a "Parameter 'a.Bb.Text' should not be null." message.

```
#!c#
var a = new A
{
    Bb = new B
    {
        Text = null;
    }
};
a.ThrowIf(() => a).Null();
a.Bb.ThrowIf(() => a.Bb).Null();
a.Bb.Text.ThrowIf(() => a.Bb.Text).Null();
```

The expression is expecting a same type to be returned from the expression so its quite safe in terms giving an incorrect parameter in the expression. Resolving parameter name from expressions is always refactor and type safe.

## Use nameof() or plain string ##

You can also use plain string to give a name to the parameter under assertion. The below code will throw ArgumentException with a "Parameter 'list' should not be empty." message.

```
#!c#
var list = new List<int>();
list.ThrowIf("list").Empty();
list.ThrowIf(nameof(string)).Empty();
```

## Throw a custom exception ##

In some cases it is a good idea to use typed exceptions to find out easily what has gone wrong. For this purpose you can use the overloaded version which takes an expression that returns an exception. The expression is only compiled in case the assertion fails.

Below code will throw SettingsNotFoundException.

```
#!c#
var settings = LoadSettings();
settings.ThrowIf(() => new SettingsNotFoundException()).Null();
```

## Chaining assertions ##

Assertions can be chained by calling another available assertion on the returned Or constraint. Or constraint has two properties **OrIf** and **OrIfNot** which both works in similar way than **ThrowIf** and **ThrowIfNot**.

```
#!c#
var list = new List<int>();
list.ThrowIf(() => list).Null()
    .OrIf().Empty()
    .OrIf().Contains(number => number == 0);
```

## Property navigation ##

Asserted object properties can also be asserted by using **PropertyIs** operation. Property is converts the exsiting assertion object to the type available in the property. All assertions available to this type can be used to assert the value.

```
#!c#
var a = new A
{
	Text = "text is longer",
};
a.ThrowIf(() => a).PropertyIs(arg => arg.Text).Empty()
	.OrIf.PropertyIs(s => s.Length).MoreThan(6);
```

Notice that the asserted type is always changed to the property type. This means that the value cannot be returned in a fluent way back to the original object type.

Below example compiles:
```
#!c#
string str = ""
string other = str.ThrowIf(() => str).Null();
```

But the below example does not compile. The asserted type has changed to int which cannot be implicitly assigned to string:
```
#!c#
string str = ""
string other = str.ThrowIf(() => str).Null()
    .OrIf.PropertyIs(s => s.Length).Zero();
```

## Ignore when ##

In some cases the assertion is needed to be ignored. For these purposes you can use **IgnoreWhen** operation:

```
#!c#
var a = new A
{
    StartTime = DateTime.Now,
    EndTime = null
}
a.ThrowIf(() => a.StartTime).IgnoreWhen(() => !a.EndTime.HasValue).IsBeforeThan(a.EndTime);
```

There can be 0...n ignore when conditions.

# Assertions #

There are several different extensions based on the actual type being asserted. These extensions will be available only for the particular type on compile time. Below are explained in more detailed level.

## Generic ##

Below assertions are available for most types.

### Null ###

Null assertion is available for any type that is a class.

```
#!c#
[TestCase]
public void ThrowIfNull_AIsNull_ThrowArgumentNullException()
{
	// Arrange
	A a = null;

	// Act
	Action act = () => a.ThrowIf(() => a).Null();

	// Result
	act.ShouldThrow<ArgumentNullException>()
		.Where(exception => exception.Message.Contains("Parameter 'a' should not be null."));
}
```

### EqualTo ###

EqualTo assertion is available to any type.

```
#!c#
[TestCase]
public void ThrowIfEqualTo_ObjIsEqualToObj_ThrowArgumentException()
{
	// Arrange
	var obj = new object();

	// Act
	Action act = () => obj.ThrowIf(() => obj).EqualTo(obj);

	// Result
	act.ShouldThrow<ArgumentException>()
		.Where(exception => exception.Message.Contains($"Parameter 'obj' should not be equal to '{obj}'."));
}
```

### Valid ###

Valid assertion is available to any type. Valid assertion takes a custom function as a parameter which returns true in case assertion succeeds.


```
#!c#
[TestCase]
public void ThrowIfNotValid_AIsNotValid_ThrowArgumentException()
{
	// Arrange
	var a = new A
	{
		IsValid = false
	};

	// Act
	Action act = () => a.ThrowIfNot(() => a).Valid(arg => arg.IsValid);

	// Result
	act.ShouldThrow<ArgumentException>()
		.Where(exception => exception.Message.Contains($"Parameter 'a' should be valid."));
}

```

## Numeric ##

Numeric assertions are available to any type that is comparable like all numeric types short, int, long, decimal.

**REMARKS!** DateTime can also be asserted with numeric assertions. It is still better to use specific DateTime assertions for DateTime objects.

### LessThan ###

LessThan can be used on any numeric type.

```
#!c#
[TestCase]
public void ThrowIfLessThan_NumberIsLessThan_ThrowArgumentException()
{
	// Arrange
	var number = 2;

	// Act
	Action act = () => number.ThrowIf(() => number).LessThan(5);

	// Result
	act.ShouldThrow<ArgumentException>()
		.Where(exception => exception.Message.Contains("Parameter 'number' should not be less than '5'."));
}
```

### LessOrEqualThan ###

LessOrEqualThan can be used on any numeric type.

```
#!c#
[TestCase(1)]
[TestCase(0)]
public void ThrowIfLessOrEqualThan_NumberIsLessThan_ThrowArgumentException(int number)
{
	// Arrange

	// Act
	Action act = () => number.ThrowIf(() => number).LessOrEqualThan(1);

	// Result
	act.ShouldThrow<ArgumentException>()
		.Where(
			exception => exception.Message.Contains("Parameter 'number' should not be less or equal than '1'."));
}
```

### MoreThan ###

MoreThan can be used on any numeric type.

```
#!c#
[TestCase]
public void ThrowIfMoreThan_NumberIsMoreThan_ThrowArgumentException()
{
	// Arrange
	var number = 1;

	// Act
	Action act = () => number.ThrowIf(() => number).MoreThan(0);

	// Result
	act.ShouldThrow<ArgumentException>()
		.Where(exception => exception.Message.Contains("Parameter 'number' should not be more than '0'."));
}
```

### MoreOrEqualThan ###

MoreOrEqualThan can be used on any numeric type.

```
#!c#
[TestCase(1)]
[TestCase(2)]
public void ThrowIfMoreOrEqualThan_NumberIsMoreOrEqualThan_ThrowArgumentException(int number)
{
	// Arrange

	// Act
	Action act = () => number.ThrowIf(() => number).MoreOrEqualThan(1);

	// Result
	act.ShouldThrow<ArgumentException>()
		.Where(exception => exception.Message.Contains("Parameter 'number' should not be more or equal than '1'."));
}
```

### InBetween ###

InBetween can be used on any numeric type.

```
#!c#
[TestCase]
public void ThrowIfNotInBetween_NumberIsInBetween_ThrowArgumentException()
{
	// Arrange
	var ints = new List<int> { 0, 1, 2 };
	var number = ints.Count;

	// Act
	Action act = () => number.ThrowIfNot(() => number).InBetween(0, ints.Count - 1);

	// Result
	act.ShouldThrow<ArgumentException>()
		.Where(exception => exception.Message.Contains("Parameter 'number' should be in between '0' and '2'."));
}
```

### Negative ###

Negative can be used on any numeric type.

```
#!c#
[TestCase]
public void ThrowIfNegative_NumberIsNegative_ThrowArgumentException()
{
	// Arrange
	var number = -1;

	// Act
	Action act = () => number.ThrowIf(() => number).Negative();

	// Result
	act.ShouldThrow<ArgumentException>()
		.Where(exception => exception.Message.Contains("Parameter 'number' should not be negative."));
}
```

### Positive ###

Positive can be used on any numeric type.

```
#!c#
[TestCase]
public void ThrowIfPositive_NumberIsPositive_ThrowArgumentException()
{
	// Arrange
	var number = 1;

	// Act
	Action act = () => number.ThrowIf(() => number).Positive();

	// Result
	act.ShouldThrow<ArgumentException>()
		.Where(exception => exception.Message.Contains("Parameter 'number' should not be positive."));
}
```

### Zero ###

Zero can be used on any numeric type.

```
#!c#
[TestCase]
public void ThrowIfZero_NumberIsZero_ThrowArgumentException()
{
	// Arrange
	var number = 0;

	// Act
	Action act = () => number.ThrowIf(() => number).Zero();

	// Result
	act.ShouldThrow<ArgumentException>()
		.Where(exception => exception.Message.Contains("Parameter 'number' should not be zero."));
}
```

## Enumerable ##

Enumerable assertions can be used on any type that is extending IEnumerable. Type checks against ICollection and string are done under the hood to make comparison against length faster on these types.

### Empty ###

Empty can be used on any type extending IEnumerable.

```
#!c#
[TestCase]
public void ThrowIfEmpty_EnumerableIsEmpty_ThrowArgumentException()
{
	// Arrange
	var a = new A
	{
		Numbers = new List<int>()
	};

	// Act
	Action act = () => a.Numbers.ThrowIf(() => a.Numbers).Empty();

	// Result
	act.ShouldThrow<ArgumentException>()
		.Where(exception => exception.Message.Contains("Parameter 'a.Numbers' should not be empty."));
}
```

### Contains ###

Contains can be used on any type extending IEnumerable<T>. There are two overloads where the actual expected item can be given as a parameter or a function resolving the expected item.

```
#!c#
[TestCase]
public void ThrowIfContains_EnumerableContainsZero_ThrowArgumentException()
{
	// Arrange
	var ints = new List<int> { 0, 1, 2, 3 };

	// Act
	Action act1 = () => ints.ThrowIf(() => ints).Contains(i => i == 0);
	Action act2 = () => ints.ThrowIf(() => ints).Contains(0);

	// Result
	act1.ShouldThrow<ArgumentException>()
		.Where(exception => exception.Message.Contains("Parameter 'ints' should not contain item '0'."));

	act2.ShouldThrow<ArgumentException>()
		.Where(exception => exception.Message.Contains("Parameter 'ints' should not contain item '0'."));
}
```

## Bool ##

Bool assertions can only be used on bool types.

### True ###

Assert if the bool is true.

```
#!c#
[TestCase]
public void ThrowIfNotTrue_BoolIsFalse_ThrowArgumentException()
{
	// Arrange
	var b = false;

	// Act
	Action act = () => b.ThrowIfNot(() => b).True();

	// Result
	act.ShouldThrow<ArgumentException>()
		.Where(exception => exception.Message.Contains("Parameter 'b' should be 'true'."));
}
```

### False ###

Assert if bool is false.

```
#!c#
[TestCase]
public void ThrowIfFalse_BoolIsFalse_ThrowArgumentException()
{
	// Arrange
	var b = false;

	// Act
	Action act = () => b.ThrowIf(() => b).False();

	// Result
	act.ShouldThrow<ArgumentException>()
		.Where(exception => exception.Message.Contains("Parameter 'b' should not be 'false'."));
}
```

## String ##

String assertions can be only used on string types.

### Null ###

Assert if string is null.

```
#!c#
[TestCase]
public void ThrowIfNull_TextIsNull_ThrowArgumentNullException()
{
	// Arrange
	var a = new A
	{
		Bb = new B()
	};

	// Act
	Action act = () => a.Bb.Text.ThrowIf(() => a.Bb.Text).Null();

	// Result
	act.ShouldThrow<ArgumentNullException>()
		.Where(exception => exception.Message.Contains("Parameter 'a.Bb.Text' should not be null."));
}
```

### Empty ###

Assert if string is empty or null.

```
#!c#
[TestCase]
public void ThrowIfEmpty_StringIsEmpty_ThrowArgumentException()
{
	// Arrange
	var a = new A
	{
		Text = ""
	};

	// Act
	Action act = () => a.Text.ThrowIf(() => a.Text).Empty();

	// Result
	act.ShouldThrow<ArgumentException>()
		.Where(exception => exception.Message.Contains("Parameter 'a.Text' should not be empty."));
}
```

### Match ###

Assert if string does not match to a specified regex pattern.

```
#!c#
[TestCase]
public void ThrowIfMatch_TextContainsXXX_ThrowArgumentException()
{
	// Arrange
	var a = new A
	{
		Text = "xxx is not allowed"
	};

	// Act
	Action act = () => a.Text.ThrowIf(() => a.Text).Match(new Regex("[xxx]"));

	// Result
	act.ShouldThrow<ArgumentException>()
		.Where(
			exception => exception.Message.Contains("Parameter 'a.Text' should not match with pattern '[xxx]'."));
}
```

### Contains ###

Assert if string contains a specified sequence.

```
#!c#
[TestCase]
public void ThrowIfContains_TextContainsMe_ThrowArgumentException()
{
	// Arrange
	var a = new A
	{
		Text = "find me"
	};

	// Act
	Action act = () => a.Text.ThrowIf(() => a.Text).Contains("me");

	// Result
	act.ShouldThrow<ArgumentException>()
		.Where(exception => exception.Message.Contains("Parameter 'a.Text' should not contain 'me'."));
}
```

### EqualTo ###

Assert if string is equal to other. StringComparison parameters can be used to fine tune the result.

```
#!c#
[TestCase]
public void ThrowIfEqualTo_TextIsEqualToOtherIgnoreCase_ThrowArgumentException()
{
	// Arrange
	var text = "text";
	var other = "TEXT";

	// Act
	Action act = () => text.ThrowIf(() => text).EqualTo(other, StringComparison.OrdinalIgnoreCase);

	// Result
	act.ShouldThrow<ArgumentException>()
		.Where(exception => exception.Message.Contains("Parameter 'text' should not be equal to 'TEXT'."));
}
```

### LengthIs ###

Assert if string length is as expected.

```
#!c#
[TestCase]
public void ThrowIfLengthIs_TextLengthIs0_ThrowArgumentException()
{
	// Arrange
	var text = "";

	// Act
	Action act = () => text.ThrowIf(() => text).LengthIs(0);

	// Result
	act.ShouldThrow<ArgumentException>()
		.Where(exception => exception.Message.Contains("Parameter 'text.Length' should not be equal to '0'."));
}
```

### LengthIsLessThan ###

Assert if string length is less than expected. Other string can also be used as the other value.

```
#!c#
[TestCase]
public void ThrowIfLengthIsLessThan_TextLengthIsLessThanSix_ThrowArgumentException()
{
	// Arrange
	var text = "12345";

	// Act
	Action act = () => text.ThrowIf(() => text).LengthIsLessThan(6);

	// Result
	act.ShouldThrow<ArgumentException>()
		.Where(exception => exception.Message.Contains("Parameter 'text.Length' should not be less than '6'."));
}

[TestCase]
public void ThrowIfLengthIsLessThan_TextLengthIsLessThanOther_ThrowArgumentException()
{
	// Arrange
	var text = "12345";
	var other = "123456";

	// Act
	Action act = () => text.ThrowIf(() => text).LengthIsLessThan(other);

	// Result
	act.ShouldThrow<ArgumentException>()
		.Where(exception => exception.Message.Contains("Parameter 'text.Length' should not be less than '6'."));
}
```

### LengthIsLessOrEqualThan ###

Assert if string length is less or equal than expected. Other string can also be used as the other value.

```
#!c#
[TestCase("123456")]
[TestCase("12345")]
public void ThrowIfLengthIsLessOrEqualThan_TextLengthIsLessOrEqualThanSix_ThrowArgumentException(string value)
{
	// Arrange
	var text = value;

	// Act
	Action act = () => text.ThrowIf(() => text).LengthIsLessOrEqualThan(6);

	// Result
	act.ShouldThrow<ArgumentException>()
		.Where(exception => exception.Message.Contains("Parameter 'text.Length' should not be less or equal than '6'."));
}

[TestCase]
public void ThrowIfLengthIsLessOrEqualThan_TextLengthIsLessOrEqualThanOther_ThrowArgumentException()
{
	// Arrange
	var text = "12345";
	var other = "123456";

	// Act
	Action act = () => text.ThrowIf(() => text).LengthIsLessOrEqualThan(other);

	// Result
	act.ShouldThrow<ArgumentException>()
		.Where(exception => exception.Message.Contains("Parameter 'text.Length' should not be less or equal than '6'."));
}
```

### LengthIsMoreThan ###

Assert if string length is more than expected. Other string can also be used as the other value.

```
#!c#
[TestCase]
public void ThrowIfLengthIsMoreThan_TextLengthIsMoreThanSix_ThrowArgumentException()
{
	// Arrange
	var text = "1234567";

	// Act
	Action act = () => text.ThrowIf(() => text).LengthIsMoreThan(6);

	// Result
	act.ShouldThrow<ArgumentException>()
		.Where(exception => exception.Message.Contains("Parameter 'text.Length' should not be more than '6'."));
}

[TestCase]
public void ThrowIfLengthIsMoreThan_TextLengthIsMoreThanOther_ThrowArgumentException()
{
	// Arrange
	var text = "123456";
	var other = "12345";

	// Act
	Action act = () => text.ThrowIf(() => text).LengthIsMoreThan(other);

	// Result
	act.ShouldThrow<ArgumentException>()
		.Where(exception => exception.Message.Contains("Parameter 'text.Length' should not be more than '5'."));
}
```

### LengthIsMoreOrEqualThan ###

Assert if string length is more or equal than expected. Other string can also be used as the other value.

```
#!c#
[TestCase("123456")]
[TestCase("1234567")]
public void ThrowIfLengthIsMoreOrEqualThan_TextLengthIsMoreOrEqualThanSix_ThrowArgumentException(string value)
{
	// Arrange
	var text = value;

	// Act
	Action act = () => text.ThrowIf(() => text).LengthIsMoreOrEqualThan(6);

	// Result
	act.ShouldThrow<ArgumentException>()
		.Where(exception => exception.Message.Contains("Parameter 'text.Length' should not be more or equal than '6'."));
}

[TestCase]
public void ThrowIfLengthIsMoreOrEqualThan_TextLengthIsMoreOrEqualThanOther_ThrowArgumentException()
{
	// Arrange
	var text = "123456";
	var other = "12345";

	// Act
	Action act = () => text.ThrowIf(() => text).LengthIsMoreOrEqualThan(other);

	// Result
	act.ShouldThrow<ArgumentException>()
		.Where(exception => exception.Message.Contains("Parameter 'text.Length' should not be more or equal than '5'."));
}
```

## DateTime ##

DateTime assertions can be used only on DateTime type.

### AfterOrEqualThanNow ###

Assert if DateTime is after or equal to the current time.

```
#!c#
[TestCase]
public void ThrowIfAfterOrEqualThanNow_DateTimeIsBeforeThanNow_ThrowArgumentException()
{
	// Arrange
	var dateTime = System.DateTime.Now.AddDays(1);

	// Act
	Action act = () => dateTime.ThrowIf(() => dateTime).AfterOrEqualThanNow();

	// Result
	act.ShouldThrow<ArgumentException>()
		.Where(exception => exception.Message.Contains($"Parameter 'dateTime' should not be after or equal than '{System.DateTime.Now}'."));
}
```

### AfterOrEqualThan ###

Assert if DateTime is after or equal than specified time.

```
#!c#
[TestCase]
public void ThrowIfAfterOrEqualThan_DateTimeIsBeforeThanOther_ThrowArgumentException()
{
	// Arrange
	var dateTime = System.DateTime.Now.AddDays(1);
	var other = System.DateTime.Now;

	// Act
	Action act = () => dateTime.ThrowIf(() => dateTime).AfterOrEqualThan(other);

	// Result
	act.ShouldThrow<ArgumentException>()
		.Where(exception => exception.Message.Contains($"Parameter 'dateTime' should not be after or equal than '{other}'."));
}
```

### AfterThanNow ###

Assert if DateTime is after than current time.

```
#!c#
[TestCase]
public void ThrowIfAfterThanNow_DateTimeIsAfterThanNow_ThrowArgumentException()
{
	// Arrange
	var dateTime = System.DateTime.Now.AddDays(1);

	// Act
	Action act = () => dateTime.ThrowIf(() => dateTime).AfterThanNow();

	// Result
	act.ShouldThrow<ArgumentException>()
		.Where(exception => exception.Message.Contains($"Parameter 'dateTime' should not be after than '{System.DateTime.Now}'."));
}
```

### AfterThan ###

Assert if DateTime is after than speicified time.

```
#!c#
[TestCase]
public void ThrowIfAfterThan_DateTimeIsAfterThanOther_ThrowArgumentException()
{
	// Arrange
	var dateTime = System.DateTime.Now.AddDays(1);
	var other = System.DateTime.Now;

	// Act
	Action act = () => dateTime.ThrowIf(() => dateTime).AfterThan(other);

	// Result
	act.ShouldThrow<ArgumentException>()
		.Where(exception => exception.Message.Contains($"Parameter 'dateTime' should not be after than '{other}'."));
}
```

### BeforeOrEqualThanNow ###

Assert if DateTime is before or equal than current time.

```
#!c#
[TestCase]
public void ThrowIfBeforeOrEqualThanNow_DateTimeIsBeforeThanNow_ThrowArgumentException()
{
	// Arrange
	var dateTime = System.DateTime.Now.AddDays(-1);

	// Act
	Action act = () => dateTime.ThrowIf(() => dateTime).BeforeOrEqualThanNow();

	// Result
	act.ShouldThrow<ArgumentException>()
		.Where(exception => exception.Message.Contains($"Parameter 'dateTime' should not be before or equal than '{System.DateTime.Now}'."));
}
```

### BeforeOrEqualThan ###

Assert if DateTime is before or equal than specified time.

```
#!c#
[TestCase]
public void ThrowIfBeforeOrEqualThan_DateTimeIsBeforeThanOther_ThrowArgumentException()
{
	// Arrange
	var dateTime = System.DateTime.Now.AddDays(-1);
	var other = System.DateTime.Now;

	// Act
	Action act = () => dateTime.ThrowIf(() => dateTime).BeforeOrEqualThan(other);

	// Result
	act.ShouldThrow<ArgumentException>()
		.Where(exception => exception.Message.Contains($"Parameter 'dateTime' should not be before or equal than '{other}'."));
}
```

### BeforeThanNow ###

Assert if DateTime is before than current time.

```
#!c#
[TestCase]
public void ThrowIfBeforeThanNow_DateTimeIsBeforeThanNow_ThrowArgumentException()
{
	// Arrange
	var dateTime = System.DateTime.Now.AddDays(-1);

	// Act
	Action act = () => dateTime.ThrowIf(() => dateTime).BeforeThanNow();

	// Result
	act.ShouldThrow<ArgumentException>()
		.Where(exception => exception.Message.Contains($"Parameter 'dateTime' should not be before than '{System.DateTime.Now}'."));
}
```

### BeforeThan ###

Assert if DateTime is before than specified time.

```
#!c#
[TestCase]
public void ThrowIfBeforeThan_DateTimeIsBeforeThanOther_ThrowArgumentException()
{
	// Arrange
	var dateTime = System.DateTime.Now.AddDays(-1);
	var other = System.DateTime.Now;

	// Act
	Action act = () => dateTime.ThrowIf(() => dateTime).BeforeThan(other);

	// Result
	act.ShouldThrow<ArgumentException>()
		.Where(exception => exception.Message.Contains($"Parameter 'dateTime' should not be before than '{other}'."));
}
```

### InMiddleOf ###

Assert if DateTime is in middle of a specified range.

```
#!c#
[TestCase]
public void ThrowIfNotInMiddleOf_DateTimeIsNotInMiddleOfStartAndEnd_ThrowArgumentException()
{
	// Arrange
	var dateTime = System.DateTime.Now.AddDays(-2);
	var start = System.DateTime.Now.AddDays(-1);
	var end = System.DateTime.Now.AddDays(1);

	// Act
	Action act = () => dateTime.ThrowIfNot(() => dateTime).InMiddleOf(start, end);

	// Result
	act.ShouldThrow<ArgumentException>()
		.Where(exception => exception.Message.Contains($"Parameter 'dateTime' should be in middle of '{start}' and '{end}'."));
}
```

# Creating custom assertions #

Both **ThrowIf()** and **ThrowIfNot()** returns an object which is extending Assertion or NullableAssertion type. To create a custom assertion, create an extension on the assertion base class.

Below is an example where a numerical value is checked if it is negative:

```
#!c#
namespace ArgumentAssertions.Numeric
{
    public static class NumericNegative
    {
        public static OrConstraint<T> Negative<T>(this IAssertion<T> assertion)
            where T : struct, IComparable, IComparable<T>, IConvertible, IEquatable<T>, IFormattable
        {
            var context = (AssertionContext) assertion;

            assertion.Assert(assertion.Value.CompareTo(0) < 0,
                context.ExceptionExpression ??
                (() => new ArgumentException(context.NegativeMessage(), context.ParameterName)));

            return new OrConstraint<T>(assertion);
        }

        [SuppressMessage("ReSharper", "UseNullPropagation")]
        public static NullableOrConstraint<T> Negative<T>(this INullableAssertion<T> assertion)
            where T : struct, IComparable, IComparable<T>, IConvertible, IEquatable<T>, IFormattable
        {
            var context = (AssertionContext) assertion;

            assertion.Assert(() =>
            {
                var nullableType = assertion.Value;
                if (!nullableType.HasValue)
                {
                    return false;
                }
                return nullableType.Value.CompareTo(0) < 0;
            }, context.ExceptionExpression ??
                (() => new ArgumentException(context.NegativeMessage(), context.ParameterName)));

            return new NullableOrConstraint<T>(assertion);
        }
    }
}
```

The error message is normally bind with an extension method to the condition:

```
#!c#
namespace ArgumentAssertions.Numeric
{
    public static class NumericExceptionMessages
    {
        public static string NegativeMessage(this AssertionContext context)
        {
            var condition = context.Condition;
            var paramName = context.ParameterName;
            switch (condition)
            {
                case Condition.If:
                    return $"Parameter '{paramName}' should not be negative.";
                case Condition.IfNot:
                    return $"Parameter '{paramName}' should be negative.";
                default:
                    throw new ArgumentOutOfRangeException(nameof(condition), condition, null);
            }
        }
    }
}
```

# Contributors #

Mika Kälkäinen

# License #

Copyright (c) 2016 Mika Kälkäinen

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.